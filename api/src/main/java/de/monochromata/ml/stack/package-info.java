/**
 * A stack of machine-learning models. The layers of the stack of models are
 * trained and evaluated one after another so that different kinds of error can
 * be removed by different models.
 */
package de.monochromata.ml.stack;