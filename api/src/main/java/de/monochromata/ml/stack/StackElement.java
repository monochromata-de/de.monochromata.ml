package de.monochromata.ml.stack;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.modelselection.Configuration;
import de.monochromata.ml.modelselection.ModelSelection.Result;

/**
 * An element on the model stack.
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public class StackElement<M, C, Ie, Ote, Oc, T> {

	private final Result<M, C, Ie, Ote, T> result;
	private final Configuration<M, C, Ie, Ote, Oc, T> configuration;

	public StackElement(final Result<M, C, Ie, Ote, T> result) {
		this(result, null);
	}

	public StackElement(final Result<M, C, Ie, Ote, T> result,
			final Configuration<M, C, Ie, Ote, Oc, T> configuration) {
		this.result = result;
		this.configuration = configuration;
	}

	public Result<M, C, Ie, Ote, T> getResult() {
		return result;
	}

	/**
	 * @return the configuration used for the stack element, or {@literal null},
	 *         if no configuration has been saved to reduce resource
	 *         consumption.
	 */
	public Configuration<M, C, Ie, Ote, Oc, T> getConfiguration() {
		return configuration;
	}

}
