package de.monochromata.ml.stack;

import java.util.List;

public class MinimumErrorReduction implements StackingStrategy {

	private final double minimumErrorReduction;

	public MinimumErrorReduction(double minimumErrorReduction) {
		this.minimumErrorReduction = minimumErrorReduction;
	}

	public double getMinimumFitnessImprovement() {
		return minimumErrorReduction;
	}

	/**
	 * Always returns true.
	 */
	@Override
	public <M, C, Ie, Ote, Oc, T> boolean createNewLayer(List<StackElement<M, C, Ie, Ote, Oc, T>> stack) {
		return true;
	}

	/**
	 * Add another layer to the stack, if the error of the last stack element is
	 * at least {@link #getMinimumFitnessImprovement()} lower than the fitness
	 * of the second to last stack element.
	 * <p>
	 * {@code true} is also returned if the stack does not yet have 2 elements.
	 */
	@Override
	public <M, C, Ie, Ote, Oc, T> boolean addLayerToStack(final StackElement<M, C, Ie, Ote, Oc, T> potentialNewLayer,
			final List<StackElement<M, C, Ie, Ote, Oc, T>> stack) {
		if (stack.size() < 1)
			return true;
		double lastErrorOnStack = stack.get(stack.size() - 1).getResult().getAverageErrorValue();
		double newError = potentialNewLayer.getResult().getAverageErrorValue();
		return (lastErrorOnStack - newError) >= minimumErrorReduction;
	}

}
