package de.monochromata.ml.stack;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import de.monochromata.ml.lowlevel.MLSpi;
import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.DefaultTrainingData;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;
import de.monochromata.ml.modelselection.Configuration;
import de.monochromata.ml.modelselection.ModelSelection;
import de.monochromata.ml.modelselection.ModelSelection.Result;

/**
 * A stack of models to be applied to a given problem. The stack will be
 * constructed by repeatedly training models chosen by a given
 * {@link ModelSelection}. The out of the earlier models serves as input for
 * later models, e.g in a stack of 2 models, the output data from the first
 * model will be input into the second model.
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public class Stacking<M, C, Ie, Ote, Oc, T> {

	/**
	 * Select a number of models that are added to a stack. The returned stack
	 * elements do not contain a configuration. Use
	 * {@link #assembleStack(StackingStrategy, ModelSelection, Configuration, BiFunction)}
	 * with a custom stack element factory function to create stack elements
	 * that include the configuration.
	 * 
	 * @param stackingStrategy
	 *            the stacking strategy to apply
	 * @param modelSelection
	 *            the model selection strategy to apply
	 * @param initialConfiguration
	 *            the configuration to use for model selection
	 * @return the stack of model selection results
	 */
	public List<StackElement<M, C, Ie, Ote, Oc, T>> assembleStack(final StackingStrategy stackingStrategy,
			final ModelSelection<M, C, Ie, Ote, Oc, T> modelSelection,
			final Configuration<M, C, Ie, Ote, Oc, T> initialConfiguration) {
		return assembleStack(stackingStrategy, modelSelection, initialConfiguration,
				(result, configuration) -> new StackElement<>(result));
	}

	/**
	 * Select a number of models that are added to a stack.
	 * 
	 * @param stackingStrategy
	 *            the stacking strategy to apply
	 * @param modelSelection
	 *            the model selection strategy to apply
	 * @param initialConfiguration
	 *            the configuration to use for model selection
	 * @param stackElementFactory
	 *            the factory function to use to create stack elements (e.g. to
	 *            include or omit the configuration of the stack element)
	 * @return the stack of model selection results
	 */
	public List<StackElement<M, C, Ie, Ote, Oc, T>> assembleStack(final StackingStrategy stackingStrategy,
			final ModelSelection<M, C, Ie, Ote, Oc, T> modelSelection,
			final Configuration<M, C, Ie, Ote, Oc, T> initialConfiguration,
			final BiFunction<Result<M, C, Ie, Ote, T>, Configuration<M, C, Ie, Ote, Oc, T>, StackElement<M, C, Ie, Ote, Oc, T>> stackElementFactory) {
		Configuration<M, C, Ie, Ote, Oc, T> configuration = initialConfiguration;
		final List<StackElement<M, C, Ie, Ote, Oc, T>> stack = new ArrayList<>();
		while (stackingStrategy.createNewLayer(stack)) {
			final Result<M, C, Ie, Ote, T> result = modelSelection.chooseValues(configuration);
			final StackElement<M, C, Ie, Ote, Oc, T> potentialNewLayer = stackElementFactory.apply(result,
					configuration);
			if (!stackingStrategy.addLayerToStack(potentialNewLayer, stack)) {
				break;
			}
			stack.add(potentialNewLayer);
			configuration = getConfigurationReflectingPartialCorrectionInCurrentResult(initialConfiguration,
					potentialNewLayer.getResult());
		}
		return stack;
	}

	public Configuration<M, C, Ie, Ote, Oc, T> getConfigurationReflectingPartialCorrectionInCurrentResult(
			final Configuration<M, C, Ie, Ote, Oc, T> configuration, final Result<M, C, Ie, Ote, T> currentResult) {
		final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors = configuration.getMlConfiguration()
				.createPreprocessors();
		final CorrectionModel<Ie, Ote, T> correctionModel = getCorrectionModelForCurrentResult(configuration,
				currentResult, preprocessors);
		return getConfigurationReflectingPartialCorrectionInCurrentResult(configuration, correctionModel,
				preprocessors);
	}

	public CorrectionModel<Ie, Ote, T> getCorrectionModelForCurrentResult(
			final Configuration<M, C, Ie, Ote, Oc, T> configuration, final Result<M, C, Ie, Ote, T> currentResult,
			final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors) {
		final C modelConfiguration = currentResult.getModelConfiguration();
		final MLSpi<M, C, Ie, Ote, Oc, T> spi = configuration.getMl().getSpi();
		final TrainingData<Ie, Ote> trainingData = createTrainingDataWithTruthValues(configuration);
		return configuration.getMl()
				.trainingData(configuration.getMlConfiguration().getInputFeatures(),
						configuration.getMlConfiguration().getOutputFeatures())
				.populate(trainingData).preprocess(preprocessors, configuration.getMlConfiguration().getExecutor())
				.thenCompose(training -> training.train(modelConfiguration, spi::read, spi::predictingCorrector,
						spi::write, configuration.getMlConfiguration().getExecutor()))
				.toCompletableFuture().join();
	}

	public TrainingData<Ie, Ote> createTrainingDataWithTruthValues(
			final Configuration<M, C, Ie, Ote, Oc, T> configuration) {
		final MLSpi<M, C, Ie, Ote, Oc, T> spi = configuration.getMl().getSpi();
		return new DefaultTrainingData<M, C, Ie, Ote, Oc, T>(spi, configuration.getMlConfiguration().getInputFeatures(),
				configuration.getMlConfiguration().getOutputFeatures(),
				getInputFeatureValues(spi, configuration.getData()), getOutputFeatureValuesByDimension(configuration));
	}

	protected List<Ie> getInputFeatureValues(final MLSpi<M, C, Ie, Ote, Oc, T> spi, final List<T> data) {
		return data.stream().map(datum -> spi.read(datum).getInputFeatureValues()).collect(toList());
	}

	protected List<List<Ote>> getOutputFeatureValuesByDimension(
			final Configuration<M, C, Ie, Ote, Oc, T> configuration) {
		final List<List<Ote>> truthValues = configuration.getTruthValues();
		final int numberOfDimensions = configuration.getMlConfiguration().getOutputFeatures().size();
		final List<List<Ote>> outputFeatureValuesByDimension = initializeNestedList(numberOfDimensions);
		for (final List<Ote> truthValue : truthValues) {
			addOutputFeatureValuesByDimension(outputFeatureValuesByDimension, truthValue);
		}
		return outputFeatureValuesByDimension;
	}

	private List<List<Ote>> initializeNestedList(final int sizeOfOuterList) {
		return initializeList(sizeOfOuterList, () -> new LinkedList<Ote>());
	}

	private <E> List<E> initializeList(final int size, final Supplier<E> initializer) {
		final ArrayList<E> list = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			list.add(initializer.get());
		}
		return list;
	}

	protected void addOutputFeatureValuesByDimension(final List<List<Ote>> allOutputFeatureValues,
			final List<Ote> newOutputFeatureValues) {
		final int numberOfDimensions = allOutputFeatureValues.size();
		for (int i = 0; i < numberOfDimensions; i++) {
			allOutputFeatureValues.get(i).add(newOutputFeatureValues.get(i));
		}
	}

	public Configuration<M, C, Ie, Ote, Oc, T> getConfigurationReflectingPartialCorrectionInCurrentResult(
			final Configuration<M, C, Ie, Ote, Oc, T> configuration, final CorrectionModel<Ie, Ote, T> correctionModel,
			final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors) {
		final MLSpi<M, C, Ie, Ote, Oc, T> spi = configuration.getMl().getSpi();
		final List<Transformer<Ie, Ote>> transformers = configuration.getMlConfiguration()
				.createTransformers(preprocessors);
		final int numberOfOutputFeatures = configuration.getMlConfiguration().getOutputFeatures().size();
		final List<T> newData = configuration.getCopyFunction().apply(configuration.getData());
		final List<List<Ote>> newTruthValues = new ArrayList<>(numberOfOutputFeatures);
		final int numberOfData = configuration.getData().size();
		for (int i = 0; i < numberOfData; i++) {
			final T newObj = newData.get(i);
			final List<Ote> truthValue = configuration.getTruthValues().get(i);
			final CorrectionDatum<Ie, Ote> originalDatum = spi.read(configuration.getData().get(i));
			final CorrectionDatum<Ie, Ote> correctedDatum = spi.read(correctionModel.apply(newObj, transformers));

			final List<Ote> outputFeatureValuesByDimension = correctedDatum.getOutputFeatureValues();
			final List<Ote> newOutputValues = spi.substractOutputFeatureValues(originalDatum.getOutputFeatureValues(),
					outputFeatureValuesByDimension);
			newData.set(i,
					spi.write(CorrectionDatum.of(correctedDatum.getInputFeatureValues(), newOutputValues), newObj));
			final List<Ote> newTruthValue = spi.substractOutputFeatureValues(truthValue,
					outputFeatureValuesByDimension);
			newTruthValues.add(newTruthValue);
		}
		return new Configuration<>(configuration, newData, newTruthValues);
	}
}
