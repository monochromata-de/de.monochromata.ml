package de.monochromata.ml.stack;

import java.util.List;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.modelselection.ModelSelection.Result;

/**
 * Decide whether to grow an ML stack.
 * <p>
 * Stacking comprises a cycle of two decisions:
 * <ol>
 * <li>whether a new layer should be created
 * <li>whether the new layer should be added to the stack
 * </ol>
 */
public interface StackingStrategy {

	/**
	 * Decide whether a new layer shall be created, which might optionally be
	 * added to the stack later. If a new layer is created, it will be added to
	 * the stack only if {@link #addLayerToStack(StackElement, List)} returned true.
	 * 
	 * @param stack
	 *            the stack of existing model selection results
	 * @param <M>
	 *            Model type that determines the hyper-parameters that apply.
	 *            E.g. linear, polynomial and RBF kernels are model types.
	 * @param <C>
	 *            the type of the implementation-specific configuration of the
	 *            ML
	 * @param <Ie>
	 *            the type of elements in the collection of input feature values
	 *            in the implementation-specific model of the ML
	 * @param <Ote>
	 *            the type of the elements in the collection of output feature
	 *            values in the implementation-specific training model of the ML
	 * @param <Oc>
	 *            the type of the collection of output feature values in the
	 *            implementation-specific correction model of the ML
	 * @param <T>
	 *            the type of objects to be corrected by the eventually-obtained
	 *            {@link CorrectionModel}
	 * @return {@code true}, if a new layer shall be created, {@code false} if
	 *         no new layer shall be created.
	 */
	public <M, C, Ie, Ote, Oc, T> boolean createNewLayer(final List<StackElement<M, C, Ie, Ote, Oc, T>> stack);

	/**
	 * Decide whether the stack shall be extended by the given potential new
	 * layer. If this method returns false, no further layer will be created and
	 * the stack will not grow further.
	 * 
	 * @param potentialNewLayer
	 *            a new model selection result that might be added to the stack
	 *            or be discarded.
	 * @param stack
	 *            the stack of existing model selection results
	 * @param <M>
	 *            Model type that determines the hyper-parameters that apply.
	 *            E.g. linear, polynomial and RBF kernels are model types.
	 * @param <C>
	 *            the type of the implementation-specific configuration of the
	 *            ML
	 * @param <Ie>
	 *            the type of elements in the collection of input feature values
	 *            in the implementation-specific model of the ML
	 * @param <Ote>
	 *            the type of the elements in the collection of output feature
	 *            values in the implementation-specific training model of the ML
	 * @param <Oc>
	 *            the type of the collection of output feature values in the
	 *            implementation-specific correction model of the ML
	 * @param <T>
	 *            the type of objects to be corrected by the eventually-obtained
	 *            {@link CorrectionModel}
	 * @return {@code true}, if the new layer shall be added to the stack and
	 *         the stacking cycle shall continue, {@code false} if the stack
	 *         should not be modified and stacking should be finished.
	 */
	public <M, C, Ie, Ote, Oc, T> boolean addLayerToStack(final StackElement<M, C, Ie, Ote, Oc, T> potentialNewLayer,
			final List<StackElement<M, C, Ie, Ote, Oc, T>> stack);

}
