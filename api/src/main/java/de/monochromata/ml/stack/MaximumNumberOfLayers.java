package de.monochromata.ml.stack;

import java.util.List;

public class MaximumNumberOfLayers implements StackingStrategy {

	private final int maximumNumberOfLayers;

	public MaximumNumberOfLayers(final int maximumNumberOfLayers) {
		this.maximumNumberOfLayers = maximumNumberOfLayers;
	}

	public int getMaximumNumberOfLayers() {
		return maximumNumberOfLayers;
	}

	/**
	 * Create another layer if {@link #getMaximumNumberOfLayers()} has not yet
	 * been reached.
	 */
	@Override
	public <M, C, Ie, Ote, Oc, T> boolean createNewLayer(List<StackElement<M, C, Ie, Ote, Oc, T>> stack) {
		return stack.size() < maximumNumberOfLayers;
	}

	/**
	 * Add the given layer to the stack, if {@link #getMaximumNumberOfLayers()}
	 * has not yet been reached for the given stack.
	 */
	@Override
	public <M, C, Ie, Ote, Oc, T> boolean addLayerToStack(final StackElement<M, C, Ie, Ote, Oc, T> potentialNewLayer,
			final List<StackElement<M, C, Ie, Ote, Oc, T>> stack) {
		return createNewLayer(stack);
	}

}
