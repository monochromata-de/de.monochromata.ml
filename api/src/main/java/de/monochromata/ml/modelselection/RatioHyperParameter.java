package de.monochromata.ml.modelselection;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * A hyper-parameter for a ratio scale of measurement, i.e. a parameter with an
 * infinite set of ordered double-precision floating-point values.
 */
public class RatioHyperParameter extends AbstractHyperParameter {

	private final double minimum;
	private final double maximum;

	public RatioHyperParameter(final String name, final double minimum, final double maximum) {
		super(name);
		requireDifferenceBetweenMinimumAndMaximum(minimum, maximum);
		requireMaximumGreaterThanMinimum(minimum, maximum);
		this.minimum = minimum;
		this.maximum = maximum;
	}

	private void requireDifferenceBetweenMinimumAndMaximum(final double minimum, final double maximum) {
		if (minimum == maximum) {
			throw new IllegalArgumentException("Minimum and maximum must differ");
		}
	}

	private void requireMaximumGreaterThanMinimum(final double minimum, final double maximum) {
		if (minimum > maximum) {
			throw new IllegalArgumentException("Maximum must be greater than minimum");
		}
	}

	protected double getMinimum() {
		return minimum;
	}

	protected double getMaximum() {
		return maximum;
	}

	/**
	 * A simple generator function for ratio hyper parameter values that generates a
	 * fixed number of equidistant values for each ratio hyper parameter. There
	 * generated values include the maximum and minimum of the passed ratio
	 * hyper-parameter.
	 * 
	 * @param numberOfSteps the number of values to generate
	 * @return the list of equidistant values
	 * @see NonIterativeModelSelection#NonIterativeModelSelection(Function)
	 */
	public static Function<RatioHyperParameter, List<HyperParameterValue<? extends Object>>> gridGenerator(
			final int numberOfSteps) {
		requireAtLeast2Steps(numberOfSteps);
		return hyperParameter -> {
			final List<HyperParameterValue<? extends Object>> values = new ArrayList<>();
			values.add(new HyperParameterValue<>(hyperParameter, hyperParameter.minimum));
			generateIntermediateSteps(values, numberOfSteps, hyperParameter);
			values.add(new HyperParameterValue<>(hyperParameter, hyperParameter.maximum));
			return values;
		};
	}

	private static void generateIntermediateSteps(final List<HyperParameterValue<? extends Object>> values,
			final int numberOfSteps, final RatioHyperParameter hyperParameter) {
		final int stepsToGenerate = numberOfSteps - 2;
		if (stepsToGenerate > 0) {
			double nextValue = hyperParameter.minimum;
			final double width = hyperParameter.maximum - hyperParameter.minimum;
			final double stepSize = width / (stepsToGenerate + 1);
			for (int i = 0; i < stepsToGenerate; i++) {
				nextValue += stepSize;
				values.add(new HyperParameterValue<>(hyperParameter, nextValue));
			}
		}
	}

	private static void requireAtLeast2Steps(final int numberOfSteps) {
		if (numberOfSteps < 2) {
			throw new IllegalArgumentException("Step size must be at least 2 so minimum and maximum are covered. "
					+ "Use a NominalHyperParameter if you want to set a single value.");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(maximum);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minimum);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RatioHyperParameter other = (RatioHyperParameter) obj;
		if (Double.doubleToLongBits(maximum) != Double.doubleToLongBits(other.maximum))
			return false;
		if (Double.doubleToLongBits(minimum) != Double.doubleToLongBits(other.minimum))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RatioHyperParameter [getName()=" + getName() + ", getMinimum()=" + getMinimum() + ", getMaximum()="
				+ getMaximum() + "]";
	}

}
