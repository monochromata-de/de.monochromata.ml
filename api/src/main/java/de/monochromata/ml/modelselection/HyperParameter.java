package de.monochromata.ml.modelselection;

/**
 * A hyper parameter, i.e. a parameter of the ML (that specifies the ML) as
 * opposed to a model parameter (that specifies the problem).
 */
public interface HyperParameter {

	public String getName();

}
