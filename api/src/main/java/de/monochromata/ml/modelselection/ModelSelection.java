package de.monochromata.ml.modelselection;

import java.util.List;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;

/**
 * Provides a way of choosing the best values for a given set of
 * hyper-parameters.
 * <p>
 * If more than one model type is provided, hyper-parameters values will be
 * chosen and validated for all model types. How hyper-parameter values are
 * chosen is specific to the implementation of this interface.
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public interface ModelSelection<M, C, Ie, Ote, Oc, T> {

	// TODO: Maybe use nested cross-validation for model selection and model
	// evaluation? See
	// http://jmlr.csail.mit.edu/papers/volume11/cawley10a/cawley10a.pdf

	/**
	 * Choose optimal hyper-parameter values.
	 * 
	 * @param configuration
	 *            TODO
	 * 
	 * @return the optimal configuration of the machine-learning framework among
	 *         the given hyper-parameters.
	 */
	public Result<M, C, Ie, Ote, T> chooseValues(Configuration<M, C, Ie, Ote, Oc, T> configuration);

	/**
	 * The result of the choice of hyper-parameters.
	 * 
	 * @param <M>
	 *            Model type that determines the hyper-parameters that apply.
	 *            E.g. linear, polynomial and RBF kernels are model types.
	 * @param <C>
	 *            the type of the implementation-specific configuration of the
	 *            ML
	 * @param <Ie>
	 *            the type of elements in the collection of input feature values
	 *            in the implementation-specific model of the ML
	 * @param <Ote>
	 *            the type of the elements in the collection of output feature
	 *            values in the implementation-specific training model of the ML
	 * @param <T>
	 *            the type of objects to be corrected by the eventually-obtained
	 *            {@link CorrectionModel}
	 */
	public static class Result<M, C, Ie, Ote, T> {

		private final M modelType;
		private final List<HyperParameterValue<?>> hyperParameterValues;
		private final C modelConfiguration;
		private final List<CorrectionModel<Ie, Ote, T>> correctionModels;
		private final List<List<Transformer<Ie, Ote>>> transformers;
		private final List<Double> errorValuesByDimension;
		private final double averageErrorValue;

		public Result(final M modelType, final List<HyperParameterValue<?>> hyperParameterValues,
				final C modelConfiguration, final List<CorrectionModel<Ie, Ote, T>> correctionModels,
				final List<List<Transformer<Ie, Ote>>> transformers, final List<Double> errorValuesByDimension) {
			this.modelType = modelType;
			this.hyperParameterValues = hyperParameterValues;
			this.modelConfiguration = modelConfiguration;
			this.correctionModels = correctionModels;
			this.transformers = transformers;
			this.errorValuesByDimension = errorValuesByDimension;
			this.averageErrorValue = errorValuesByDimension.stream().mapToDouble(value -> value).average().getAsDouble();
		}

		public M getModelType() {
			return modelType;
		}

		public List<HyperParameterValue<?>> getHyperParameterValues() {
			return hyperParameterValues;
		}

		public C getModelConfiguration() {
			return modelConfiguration;
		}

		public List<CorrectionModel<Ie, Ote, T>> getCorrectionModels() {
			return correctionModels;
		}

		public List<List<Transformer<Ie, Ote>>> getTransformers() {
			return transformers;
		}

		public List<Double> getErrorValuesByDimension() {
			return errorValuesByDimension;
		}

		public double getAverageErrorValue() {
			return averageErrorValue;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(averageErrorValue);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + ((modelConfiguration == null) ? 0 : modelConfiguration.hashCode());
			result = prime * result + ((errorValuesByDimension == null) ? 0 : errorValuesByDimension.hashCode());
			result = prime * result + ((hyperParameterValues == null) ? 0 : hyperParameterValues.hashCode());
			result = prime * result + ((modelType == null) ? 0 : modelType.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Result other = (Result) obj;
			if (Double.doubleToLongBits(averageErrorValue) != Double.doubleToLongBits(other.averageErrorValue))
				return false;
			if (modelConfiguration == null) {
				if (other.modelConfiguration != null)
					return false;
			} else if (!modelConfiguration.equals(other.modelConfiguration))
				return false;
			if (errorValuesByDimension == null) {
				if (other.errorValuesByDimension != null)
					return false;
			} else if (!errorValuesByDimension.equals(other.errorValuesByDimension))
				return false;
			if (hyperParameterValues == null) {
				if (other.hyperParameterValues != null)
					return false;
			} else if (!hyperParameterValues.equals(other.hyperParameterValues))
				return false;
			if (modelType == null) {
				if (other.modelType != null)
					return false;
			} else if (!modelType.equals(other.modelType))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "Result [getModelType()=" + getModelType() + ", getHyperParameterValues()="
					+ getHyperParameterValues() + ", getConfiguration()=" + getModelConfiguration()
					+ ", getErrorValues()=" + getErrorValuesByDimension() + ", getAverageErrorValue()=" + getAverageErrorValue()
					+ "]";
		}

	}

}
