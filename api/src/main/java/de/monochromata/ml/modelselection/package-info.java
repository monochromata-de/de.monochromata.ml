/**
 * Means of searching the space of values of
 * {@link de.monochromata.ml.modelselection.HyperParameter}s.
 * <p>
 * A {@link de.monochromata.ml.modelselection.ModelSelection} is used
 * to find a configuration for a machine learning framework
 */
package de.monochromata.ml.modelselection;