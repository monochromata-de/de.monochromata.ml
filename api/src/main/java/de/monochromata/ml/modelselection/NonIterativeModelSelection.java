package de.monochromata.ml.modelselection;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.monochromata.ml.crossvalidation.CrossValidation;
import de.monochromata.ml.lowlevel.ML;
import de.monochromata.ml.lowlevel.MLConfiguration;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;

/**
 * An abstract base class for choices of hyper-parameter values.
 * <p>
 * The ML API supports two kinds of hyper-parameters:
 * {@link NominalHyperParameter}s and {@link RatioHyperParameter}s.
 * {@link NominalHyperParameter}s have a list of values and each value should be
 * considered when choosing hyper-parameters, which is what this abstract class
 * does. {@link RatioHyperParameter}s have an infinite number of values,
 * sub-classes of this class need to implement their own way of choosing a
 * sub-set of these values.
 * <p>
 * Instances of this class create the Cartesian product of all values of all
 * hyper parameters. The values of the ratio hyper parameters are sampled using
 * a function supplied to the constructor of this class.
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public class NonIterativeModelSelection<M, C, Ie, Ote, Oc, T> implements ModelSelection<M, C, Ie, Ote, Oc, T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(NonIterativeModelSelection.class);

	private final Function<RatioHyperParameter, List<HyperParameterValue<? extends Object>>> ratioHyperParameterValueGenerator;

	public NonIterativeModelSelection(
			final Function<RatioHyperParameter, List<HyperParameterValue<? extends Object>>> ratioHyperParameterValueGenerator) {
		this.ratioHyperParameterValueGenerator = ratioHyperParameterValueGenerator;
	}

	@Override
	public Result<M, C, Ie, Ote, T> chooseValues(Configuration<M, C, Ie, Ote, Oc, T> configuration) {
		Result<M, C, Ie, Ote, T> bestResult = null;
		for (final Entry<M, List<HyperParameter>> modelTypeEntry : configuration.getHyperParameters().entrySet()) {
			final Result<M, C, Ie, Ote, T> newResult = chooseValues(configuration.getMl(),
					configuration.getMlConfiguration(), configuration.getCrossValidation(), modelTypeEntry.getKey(),
					modelTypeEntry.getValue(), configuration.getErrorFunction(), configuration.getData(),
					configuration.getTruthValues());
			bestResult = chooseBestResult(bestResult, newResult);
		}
		return bestResult;
	}

	private Result<M, C, Ie, Ote, T> chooseBestResult(final Result<M, C, Ie, Ote, T> oldResult,
			final Result<M, C, Ie, Ote, T> newResult) {
		return (oldResult == null || newResult.getAverageErrorValue() < oldResult.getAverageErrorValue()) ? newResult
				: oldResult;
	}

	public Result<M, C, Ie, Ote, T> chooseValues(final ML<M, C, Ie, Ote, Oc, T> ml,
			final MLConfiguration<Ie, Ote, Oc, T> configuration,
			final CrossValidation<M, C, Ie, Ote, Oc, T> crossValidation, final M modelType,
			final List<HyperParameter> hyperParameters, final BiFunction<List<Ote>, List<Ote>, Double> errorFunction,
			final List<T> data, final List<List<Ote>> truthValues) {
		final Map<HyperParameter, List<HyperParameterValue<?>>> hyperParameterValues = getHyperParameterValues(
				hyperParameters);
		final List<List<HyperParameterValue<?>>> cartesianProduct = cartesianProduct(hyperParameters,
				hyperParameterValues);
		ensureCartesianProductIsNotEmpty(cartesianProduct);
		LOGGER.debug("There are " + cartesianProduct.size() + " combinations to submit to cross-validation");
		Result<M, C, Ie, Ote, T> bestResult = null;
		for (final List<HyperParameterValue<?>> values : cartesianProduct) {
			final Result<M, C, Ie, Ote, T> newResult = performCrossValidationFor(ml, configuration, crossValidation,
					modelType, values, errorFunction, data, truthValues);
			bestResult = chooseBestResult(bestResult, newResult);
		}
		return bestResult;
	}

	private void ensureCartesianProductIsNotEmpty(final List<List<HyperParameterValue<?>>> cartesianProduct) {
		if (cartesianProduct.isEmpty()) {
			throw new IllegalStateException("Cartesian product is empty");
		}
	}

	public Result<M, C, Ie, Ote, T> performCrossValidationFor(final ML<M, C, Ie, Ote, Oc, T> ml,
			final MLConfiguration<Ie, Ote, Oc, T> problemConfiguration,
			final CrossValidation<M, C, Ie, Ote, Oc, T> crossValidation, final M modelType,
			final List<HyperParameterValue<?>> hyperParameterValues,
			final BiFunction<List<Ote>, List<Ote>, Double> errorFunction, final List<T> data,
			final List<List<Ote>> truthValues) {
		final C modelConfiguration = ml.getSpi().createConfiguration(modelType, hyperParameterValues);
		final CrossValidation.Result<Ie, Ote, T> validationResult = crossValidation.validate(ml, problemConfiguration,
				modelConfiguration, errorFunction, data, truthValues);
		return new Result<M, C, Ie, Ote, T>(modelType, hyperParameterValues, modelConfiguration,
				validationResult.getCorrectionModels(), validationResult.getTransformers(),
				validationResult.getAverageErrorByDimension());
	}

	// TODO: Add a test
	public List<List<HyperParameterValue<?>>> cartesianProduct(final List<HyperParameter> hyperParameters,
			final Map<HyperParameter, List<HyperParameterValue<?>>> hyperParameterValues) {
		final List<List<HyperParameterValue<?>>> values = hyperParameters.stream()
				.map(hyperParameter -> hyperParameterValues.get(hyperParameter)).collect(toList());
		return cartesianProduct(values);
	}

	// TODO: Add a test
	public List<List<HyperParameterValue<?>>> cartesianProduct(final List<List<HyperParameterValue<?>>> values) {
		final List<List<HyperParameterValue<?>>> resultLists = new ArrayList<>();
		if (values.size() == 0) {
			resultLists.add(new ArrayList<>());
			return resultLists;
		} else {
			final List<HyperParameterValue<?>> valuesForFirstParameter = values.get(0);
			final List<List<HyperParameterValue<?>>> valuesForRemainingParameters = cartesianProduct(
					values.subList(1, values.size()));
			for (final HyperParameterValue<?> valueForFirstParameter : valuesForFirstParameter) {
				for (final List<HyperParameterValue<?>> valuesForAnotherParameter : valuesForRemainingParameters) {
					final ArrayList<HyperParameterValue<?>> resultList = new ArrayList<>();
					resultList.add(valueForFirstParameter);
					resultList.addAll(valuesForAnotherParameter);
					resultLists.add(resultList);
				}
			}
		}
		return resultLists;
	}

	protected Map<HyperParameter, List<HyperParameterValue<?>>> getHyperParameterValues(
			final List<HyperParameter> hyperParameters) {
		final Map<HyperParameter, List<HyperParameterValue<?>>> values = new HashMap<>();
		for (final HyperParameter hyperParameter : hyperParameters) {
			values.put(hyperParameter, getHyperParameterValues(hyperParameter));
		}
		return values;
	}

	protected List<HyperParameterValue<?>> getHyperParameterValues(final HyperParameter hyperParameter) {
		if (hyperParameter instanceof NominalHyperParameter) {
			return getHyperParameterValues((NominalHyperParameter<?>) hyperParameter);
		} else if (hyperParameter instanceof RatioHyperParameter) {
			return getHyperParameterValues((RatioHyperParameter) hyperParameter);
		} else {
			throw new IllegalArgumentException("Unknown sub-class of " + HyperParameter.class.getSimpleName() + ": "
					+ hyperParameter.getClass().getName());
		}
	}

	protected List<HyperParameterValue<? extends Object>> getHyperParameterValues(
			final NominalHyperParameter<? extends Object> hyperParameter) {
		return hyperParameter.getDiscreteValues().stream()
				.map(value -> new HyperParameterValue<>(hyperParameter, value)).collect(toList());
	}

	protected List<HyperParameterValue<? extends Object>> getHyperParameterValues(
			final RatioHyperParameter hyperParameter) {
		return ratioHyperParameterValueGenerator.apply(hyperParameter);
	}

}
