package de.monochromata.ml.modelselection;

/**
 * A value for a hyper-parameter.
 * 
 * @param <T>
 *            the type of the value
 */
public class HyperParameterValue<T> {

	private final HyperParameter hyperParameter;
	private final T value;

	public HyperParameterValue(HyperParameter hyperParameter, T value) {
		this.hyperParameter = hyperParameter;
		this.value = value;
	}

	public HyperParameter getHyperParameter() {
		return hyperParameter;
	}

	public T getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hyperParameter == null) ? 0 : hyperParameter.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HyperParameterValue<?> other = (HyperParameterValue<?>) obj;
		if (hyperParameter == null) {
			if (other.hyperParameter != null)
				return false;
		} else if (!hyperParameter.equals(other.hyperParameter))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HyperParameterValue [hyperParameter=" + hyperParameter + ", value=" + value + "]";
	}

}
