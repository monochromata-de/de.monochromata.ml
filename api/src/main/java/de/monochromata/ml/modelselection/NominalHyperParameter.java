package de.monochromata.ml.modelselection;

import java.util.List;

/**
 * A hyper-parameter for a nominal scale of measurement, i.e. a parameter with a
 * fixed set of unordered values.
 * 
 * @param <T>
 *            the type of the discrete values
 */
public class NominalHyperParameter<T> extends AbstractHyperParameter {
	private final List<T> discreteValues;

	public NominalHyperParameter(final String name, final List<T> discreteValues) {
		super(name);
		this.discreteValues = discreteValues;
	}

	protected List<T> getDiscreteValues() {
		return discreteValues;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((discreteValues == null) ? 0 : discreteValues.hashCode());
		return result;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NominalHyperParameter other = (NominalHyperParameter) obj;
		if (discreteValues == null) {
			if (other.discreteValues != null)
				return false;
		} else if (!discreteValues.equals(other.discreteValues))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NominalHyperParameter [getName()=" + getName() + ", getDiscreteValues()=" + getDiscreteValues() + "]";
	}
}
