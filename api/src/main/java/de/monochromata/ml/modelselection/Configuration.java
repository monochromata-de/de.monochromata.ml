package de.monochromata.ml.modelselection;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import de.monochromata.ml.crossvalidation.CrossValidation;
import de.monochromata.ml.lowlevel.ML;
import de.monochromata.ml.lowlevel.MLConfiguration;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;

/**
 * A parameter object for {@link ModelSelection}.
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public class Configuration<M, C, Ie, Ote, Oc, T> {

	private final ML<M, C, Ie, Ote, Oc, T> ml;
	private final MLConfiguration<Ie, Ote, Oc, T> mlConfiguration;
	private final CrossValidation<M, C, Ie, Ote, Oc, T> crossValidation;
	private final Map<M, List<HyperParameter>> hyperParameters;
	private final BiFunction<List<Ote>, List<Ote>, Double> errorFunction;
	private final Function<List<T>, List<T>> copyFunction;
	private final List<T> data;
	private final List<List<Ote>> truthValues;

	public Configuration(final ML<M, C, Ie, Ote, Oc, T> ml, final MLConfiguration<Ie, Ote, Oc, T> mlConfiguration,
			final CrossValidation<M, C, Ie, Ote, Oc, T> crossValidation,
			final Map<M, List<HyperParameter>> hyperParameters,
			final BiFunction<List<Ote>, List<Ote>, Double> errorFunction, final Function<List<T>, List<T>> copyFunction,
			final List<T> data, final List<List<Ote>> truthValues) {
		this.ml = ml;
		this.mlConfiguration = mlConfiguration;
		this.crossValidation = crossValidation;
		this.hyperParameters = hyperParameters;
		this.errorFunction = errorFunction;
		this.copyFunction = copyFunction;
		this.data = data;
		this.truthValues = truthValues;
	}

	/**
	 * Create a new configuration using all data from the given prototyp, except
	 * for {@code data} and {@code truthValues} which are taken from the given
	 * arguments.
	 */
	public Configuration(final Configuration<M, C, Ie, Ote, Oc, T> prototype, final List<T> data,
			final List<List<Ote>> truthValues) {
		this(prototype.ml, prototype.mlConfiguration, prototype.crossValidation, prototype.hyperParameters,
				prototype.errorFunction, prototype.copyFunction, data, truthValues);
	}

	public ML<M, C, Ie, Ote, Oc, T> getMl() {
		return ml;
	}

	public MLConfiguration<Ie, Ote, Oc, T> getMlConfiguration() {
		return mlConfiguration;
	}

	public CrossValidation<M, C, Ie, Ote, Oc, T> getCrossValidation() {
		return crossValidation;
	}

	public Map<M, List<HyperParameter>> getHyperParameters() {
		return hyperParameters;
	}

	public BiFunction<List<Ote>, List<Ote>, Double> getErrorFunction() {
		return errorFunction;
	}

	public Function<List<T>, List<T>> getCopyFunction() {
		return copyFunction;
	}

	public List<T> getData() {
		return data;
	}

	public List<List<Ote>> getTruthValues() {
		return truthValues;
	}
}