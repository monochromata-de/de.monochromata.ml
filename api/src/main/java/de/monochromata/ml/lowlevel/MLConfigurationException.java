package de.monochromata.ml.lowlevel;

/**
 * A non-checked exception that signals a mis-configuration of a
 * Machine-Learning framework.
 */
public class MLConfigurationException extends RuntimeException {

	private static final long serialVersionUID = 1974832987164392353L;

	public MLConfigurationException() {
		super();
	}

	public MLConfigurationException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MLConfigurationException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public MLConfigurationException(final String message) {
		super(message);
	}

	public MLConfigurationException(final Throwable cause) {
		super(cause);
	}

}
