package de.monochromata.ml.lowlevel;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.function.Supplier;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;

/**
 * A bean that can be used as {@link MLConfiguration}.
 *
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public class DefaultMLConfiguration<Ie, Ote, Oc, T> implements MLConfiguration<Ie, Ote, Oc, T> {

	private final Executor executor;
	private final List<Feature> inputFeatures;
	private final List<Feature> outputFeatures;
	private final Supplier<List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>>> preprocessorsSupplier;
	private final Function<List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>>, List<Transformer<Ie, Ote>>> transformersSupplier;

	public DefaultMLConfiguration(final Executor executor, final List<Feature> inputFeatures,
			final List<Feature> outputFeatures,
			final Supplier<List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>>> preprocessorsSupplier,
			final Function<List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>>, List<Transformer<Ie, Ote>>> transformersSupplier) {
		this.executor = executor;
		this.inputFeatures = inputFeatures;
		this.outputFeatures = outputFeatures;
		this.preprocessorsSupplier = preprocessorsSupplier;
		this.transformersSupplier = transformersSupplier;
	}

	@Override
	public Executor getExecutor() {
		return executor;
	}

	@Override
	public List<Feature> getInputFeatures() {
		return inputFeatures;
	}

	@Override
	public List<Feature> getOutputFeatures() {
		return outputFeatures;
	}

	@Override
	public List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> createPreprocessors() {
		return preprocessorsSupplier.get();
	}

	@Override
	public List<Transformer<Ie, Ote>> createTransformers(
			final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors) {
		return transformersSupplier.apply(preprocessors);
	}

}
