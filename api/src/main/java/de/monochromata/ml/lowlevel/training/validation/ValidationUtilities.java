package de.monochromata.ml.lowlevel.training.validation;

import java.util.List;

public class ValidationUtilities {

	public static void requireSameSize(final List<?> list1, final List<?> list2, final String errorMessage) {
		requireSameSize(list1.size(), list2.size(), errorMessage);
	}

	public static void requireSameSize(final double[] array1, final double[] array2, final String errorMessage) {
		requireSameSize(array1.length, array2.length, errorMessage);
	}

	private static void requireSameSize(final int size1, final int size2, final String errorMessage) {
		if (size1 != size2) {
			throw new IllegalArgumentException(errorMessage + " (" + size1 + " != " + size2 + ")");
		}
	}

}
