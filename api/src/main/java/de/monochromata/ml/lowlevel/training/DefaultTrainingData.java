package de.monochromata.ml.lowlevel.training;

import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.MLSpi;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;

/**
 * A default implementation of a model data container.
 * 
 * @param <M>   Model type that determines the hyper-parameters that apply. E.g.
 *              linear, polynomial and RBF kernels are model types.
 * @param <C>   the type of the implementation-specific configuration of the ML
 * @param <Ie>  the type of elements in the collection of input feature values
 *              in the implementation-specific model of the ML
 * @param <Ote> the type of the elements in the collection of output feature
 *              values in the implementation-specific training model of the ML
 * @param <Oc>  the type of the collection of output feature values in the
 *              implementation-specific correction model of the ML
 * @param <T>   the type of objects to be corrected by the eventually-obtained
 *              {@link CorrectionModel}
 */
public class DefaultTrainingData<M, C, Ie, Ote, Oc, T> implements TrainingData<Ie, Ote> {

	private final MLSpi<M, C, Ie, Ote, Oc, T> spi;
	private final List<Feature> inputFeatures;
	private final List<Feature> outputFeatures;
	private final List<Ie> inputFeatureValues;
	private final List<List<Ote>> outputFeatureValues;

	/**
	 * Creates new training data.
	 * 
	 * <p>
	 * Note that the provided {@code outputFeatureValues} need to be initialized
	 * with a (potentially empty) list for each output feature.
	 * 
	 * <p>
	 * This constructor shall be used when it is necessary to keep a reference to
	 * the lists of values that the created instance operates on.
	 * 
	 * <p>
	 * Feature values provided to this constructor are organized in the following
	 * way.
	 * 
	 * <pre>
	 * {@code
	 * datum1 = [ <value of input feature 1>, <value of input feature 2>, ... ]
	 * datum2 = [ <value of input feature 1>, <value of input feature 2>, ... ]
	 * inputFeatureValues = { datum1, datum2 }
	 * 
	 * valuesOfOutputFeature1 = [ <value of outputFeature1 for datum 1>, <value of outputFeature1 for datum 2>, ... ]
	 * valuesOfOutputFeature2 = [ <value of outputFeature2 for datum 1>, <value of outputFeature2 for datum 2>, ... ]
	 * outputFeatureValues = { valuesOfOutputFeature1,
	 * 						   valuesOfOutputFeature2 }
	 * }
	 * </pre>
	 * 
	 * @param spi                 the service-provider interface to use, which
	 *                            adapts the ML API to the machine-learning
	 *                            framework used
	 * @param inputFeatures       the input features
	 * @param outputFeatures      defines the order of output features
	 * @param inputFeatureValues  the input feature values
	 * @param outputFeatureValues the output feature values
	 * @throws IllegalArgumentException If
	 *                                  {@code trainingData.getOutputFeatureValues().size()}
	 *                                  does not match
	 *                                  {@code outputFeatures.size()}.
	 * @throws NullPointerException     If any of the arguments is {@code null}, or
	 *                                  if
	 *                                  {@code trainingData.getOutputFeatureValues()}
	 *                                  contains {@code null} values
	 */
	public DefaultTrainingData(final MLSpi<M, C, Ie, Ote, Oc, T> spi, final List<Feature> inputFeatures,
			final List<Feature> outputFeatures, final List<Ie> inputFeatureValues,
			final List<List<Ote>> outputFeatureValues) {
		requireNonNull(spi, "spi must not be null");
		requireNonNull(inputFeatures, "inputFeatures must not be null");
		requireNonNull(outputFeatures, "outputFeatures must not be null");
		requireNonNull(inputFeatureValues, "inputFeatureValues must not be null");
		requireNonNull(outputFeatureValues, "outputFeatureValues must not be null");
		ensureThereAreValuesForEachOutputFeature(outputFeatures, outputFeatureValues);
		ensureThatNoOutputFeatureIsNull(outputFeatureValues);
		this.spi = spi;
		this.inputFeatures = inputFeatures;
		this.outputFeatures = outputFeatures;
		this.inputFeatureValues = inputFeatureValues;
		this.outputFeatureValues = outputFeatureValues;
	}

	/**
	 * Compares the sizes of the list of output features and the list of output
	 * feature values.
	 * 
	 * @param outputFeatures      the list of output features
	 * @param outputFeatureValues a list that contains a sub-list each output
	 *                            feature to a list of its data. The size of this
	 *                            list must match the size of outputFeatures list.
	 * @throws IllegalArgumentException If the size of {@code outputFeatureValues}
	 *                                  does not match the size of
	 *                                  {@code outputFeatures}.
	 */
	protected void ensureThereAreValuesForEachOutputFeature(final List<Feature> outputFeatures,
			final List<List<Ote>> outputFeatureValues) {
		final int numberOfOutputFeatures = outputFeatures.size();
		final int sizeOfOutputFeatureValues = outputFeatureValues.size();
		if (numberOfOutputFeatures != sizeOfOutputFeatureValues) {
			throw new IllegalArgumentException(
					"Number of output features does not match the size of outputFeatureValues ("
							+ numberOfOutputFeatures + "!=" + sizeOfOutputFeatureValues + ")");
		}
	}

	/**
	 * Ensures that there a no {@code null} values in the given list.
	 * 
	 * @param list the list to check
	 * @throws NullPointerException if the list contains {@code null} values
	 */
	protected void ensureThatNoOutputFeatureIsNull(final List<List<Ote>> list) {
		IntStream.range(0, list.size()).forEach(i -> {
			if (list.get(i) == null)
				throw new NullPointerException(i + ". output feature has a null list of values");
		});
	}

	@Override
	public List<Feature> getInputFeatures() {
		return inputFeatures;
	}

	@Override
	public List<Feature> getOutputFeatures() {
		return outputFeatures;
	}

	@Override
	public List<Ie> getInputFeatureValues() {
		return inputFeatureValues;
	}

	@Override
	public List<List<Ote>> getOutputFeatureValues() {
		return outputFeatureValues;
	}

	@Override
	public TrainingData<Ie, Ote> clone() {
		return new DefaultTrainingData<>(spi, copyFeatures(inputFeatures), copyFeatures(outputFeatures),
				copyInputFeatureValues(), copyOutputFeatureValues());
	}

	protected List<Feature> copyFeatures(final List<Feature> features) {
		return features.stream().map(oldFeature -> new Feature(oldFeature.getName())).collect(Collectors.toList());
	}

	protected List<Ie> copyInputFeatureValues() {
		return spi.copyInputFeatureValues(inputFeatureValues);
	}

	protected List<List<Ote>> copyOutputFeatureValues() {
		return spi.copyOutputFeatureValues(outputFeatureValues);
	}
}
