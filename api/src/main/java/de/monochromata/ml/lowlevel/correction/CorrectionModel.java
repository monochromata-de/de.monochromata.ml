package de.monochromata.ml.lowlevel.correction;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.io.Serializable;
import java.util.List;
import java.util.function.UnaryOperator;

import de.monochromata.ml.lowlevel.training.Training;

/**
 * A correction model wraps a trained ML that may be used to perform
 * cross-validation or to correct real data.
 * 
 * <p>
 * Instances of classes implementing this interface are created by
 * implementations of {@link Training}. Implementations of this interface have
 * Therefore, {@link CorrectionModel}s have access to the correctors passed to
 * {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction)}
 * or
 * {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction, java.util.concurrent.Executor)}
 * .
 * 
 * @param <Ie>  the type of elements in the collection of input feature values
 *              in the implementation-specific model of the ML
 * @param <Ote> the type of the elements in the collection of output feature
 *              values in the implementation-specific training model of the ML
 * @param <T>   the type of objects to be corrected
 */
public interface CorrectionModel<Ie, Ote, T> extends UnaryOperator<T> {

	/**
	 * Obtain a memento that can be save to disk and can be used to reconstruct the
	 * model at a later time.
	 * 
	 * @return the memento
	 */
	public Serializable getMemento();

	/**
	 * Correct the given object. No transformers will be applied.
	 * 
	 * @param input the object to correct
	 * @return the modified input instance, or a new instance that has been created
	 *         by the corrector function passed to the preceding invocation of
	 *         {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction)}
	 *         or
	 *         {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction, java.util.concurrent.Executor)}
	 *         . Whether the modified input instance or a new instance is returned,
	 *         depends on the corrector implementation.
	 */
	@Override
	default T apply(final T input) {
		return apply(input, emptyList());
	}

	/**
	 * Apply the given transformer to the input feature values extracted from the
	 * given object, correct the given object, and apply the given transformer to
	 * the output feature values predicted during correction.
	 * 
	 * @param input       the object to correct
	 * @param transformer the transformer to apply
	 * @return the modified input instance, or a new instance that has been created
	 *         by the corrector function passed to the preceding invocation of
	 *         {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction)}
	 *         or
	 *         {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction, java.util.concurrent.Executor)}
	 *         . Whether the modified input instance or a new instance is returned,
	 *         depends on the corrector implementation.
	 */
	default T apply(final T input, final Transformer<Ie, Ote> transformer) {
		return apply(input, singletonList(transformer));
	}

	/**
	 * Apply the given transformers to the input feature values extracted from the
	 * given object, correct the given object, and apply the given transformers to
	 * the output feature values predicted during correction.
	 * 
	 * @param input        the object to correct
	 * @param transformers the transformers to apply
	 * @return the modified input instance, or a new instance that has been created
	 *         by the corrector function passed to the preceding invocation of
	 *         {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction)}
	 *         or
	 *         {@link Training#train(Object, java.util.function.Function, java.util.function.BiFunction, java.util.function.BiFunction, java.util.concurrent.Executor)}
	 *         . Whether the modified input instance or a new instance is returned,
	 *         depends on the corrector implementation.
	 */
	T apply(final T input, final List<Transformer<Ie, Ote>> transformers);

	/**
	 * Apply the given transformers to the input feature values in the given
	 * correction datum, correct the given datum, and apply the given transformers
	 * to the output feature values predicted during correction.
	 * 
	 * @param correctionDatum the object holding the input feature values and
	 *                        supposed to consume the corrected output feature
	 *                        values
	 * @param transformers    the transformers to apply
	 * @return the modified correction datum.
	 */
	CorrectionDatum<Ie, Ote> applyToDatum(final CorrectionDatum<Ie, Ote> correctionDatum,
			final List<Transformer<Ie, Ote>> transformers);
}
