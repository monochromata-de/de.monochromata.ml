package de.monochromata.ml.lowlevel.training;

/**
 * A populator is used to add data to a {@link TrainingDataCollection} instance.
 * 
 * <p>
 * Implementations of this method might not be thread-safe.
 */
public interface TrainingDataPopulator {

	/**
	 * Adds a single datum to the training data.
	 * 
	 * @throws IllegalArgumentException
	 *             if the number of feature values does not match the number of
	 *             features configured for the {@link TrainingDataCollection}
	 *             that this {@link TrainingDataPopulator} populates.
	 * @throws IllegalStateException
	 *             If this method is invoked after {@link #finish()} has been
	 *             invoked on the same {@link TrainingDataPopulator} instance.
	 * @see #finish()
	 */
	public void addDatum(double[] inputFeatureValues, double[] outputFeatureValues);

	/**
	 * Invoked when no further data will be added.
	 *
	 * @throws IllegalStateException
	 *             If this method is invoked twice on the same
	 *             {@link TrainingDataPopulator} instance.
	 * @see #addDatum(double[], double[])
	 */
	public void finish();
}