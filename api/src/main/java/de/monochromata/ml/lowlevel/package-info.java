/**
 * Finding optimal parameters via support-vector machines (
 * <a href="https://en.wikipedia.org/wiki/Support_vector_machine">ML</a>s).
 * <p>
 * There is a basic distinction between hyper parameters and model parameters.
 * Hyper parameters configure the ML. Model parameters are features that
 * specify the problem to be solved.
 * <p>
 * There are different ways of applying this framework.
 * <ol>
 * <li><b>train + correct</b>: train a model with a given set of values for
 * hyper-parameters and thereafter correct data with it</li>
 * <li><b>choose_hyper_parameters(train+validate)+correct</b>: automatically
 * choose those values for a given set of hyper-parameters that yield the best
 * validation results and correct data thereafter.</li>
 * <li><b>configure_stack(choose_hyper_parameters(train+validate))+correct</b>
 * </li>
 * </ol>
 */
package de.monochromata.ml.lowlevel;