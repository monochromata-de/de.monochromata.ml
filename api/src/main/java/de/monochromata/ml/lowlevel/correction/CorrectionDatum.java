package de.monochromata.ml.lowlevel.correction;

import java.util.List;

/**
 * A single datum that is subject to correction.
 * 
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 */
public interface CorrectionDatum<Ie, Ote> {

	public Ie getInputFeatureValues();

	/**
	 * The output feature values ordered by dimension.
	 */
	public List<Ote> getOutputFeatureValues();

	public static <Ie, Ote> CorrectionDatum<Ie, Ote> of(final Ie inputFeatureValues,
			final List<Ote> outputFeatureValues) {
		return new CorrectionDatum<Ie, Ote>() {

			@Override
			public Ie getInputFeatureValues() {
				return inputFeatureValues;
			}

			@Override
			public List<Ote> getOutputFeatureValues() {
				return outputFeatureValues;
			}

		};
	}

}
