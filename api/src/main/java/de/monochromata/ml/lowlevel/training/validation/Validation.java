package de.monochromata.ml.lowlevel.training.validation;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;

/**
 * A interface for validating a {@link CorrectionModel}.
 * 
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <T>
 *            the type of objects to be corrected
 */
public interface Validation<Ie, Ote, T> {

	/**
	 * Validates the given (potentially multi-dimensional) correction model.
	 * 
	 * @param model
	 *            the correction model to validate. The model will be invoked
	 *            for each test datum.
	 * @param reader
	 *            transforms the problem-specific data structure into a
	 *            framework-specific data-structure before correction
	 * @param transformers
	 *            the transformers to apply when computing estimates
	 * @param testDataSupplier
	 *            a supplier for test data. The supplier is invoked until it
	 *            returns {@code null}.
	 * @param truthSupplier
	 *            a supplier for true values. The supplier will be invoked for
	 *            each test datum provided by the {@code testDataSupplier}.
	 * @param errorFunction
	 *            to evaluate the correction model
	 * @return Error values, as returned by e.g.
	 *         {@link ErrorFunction#mse(java.util.List, java.util.List)} Each
	 *         dimension of the return value is a dimension of a
	 *         multi-dimensional problem. An error value of {@code 0d}
	 *         represents a perfect fit.
	 * @throws IllegalStateException
	 *             If no truth can be obtained for a test datum, i.e. if
	 *             {@code truthSupplier} supplies fewer values than
	 *             {@code testDataSupplier}.
	 */
	List<Double> validate(final CorrectionModel<Ie, Ote, T> model, final Function<T, CorrectionDatum<Ie, Ote>> reader,
			final List<Transformer<Ie, Ote>> transformers, final Supplier<T> testDataSupplier,
			final Supplier<List<Ote>> truthSupplier, final BiFunction<List<Ote>, List<Ote>, Double> errorFunction);
}
