package de.monochromata.ml.lowlevel.training;

import java.util.List;

import de.monochromata.ml.lowlevel.Feature;

/**
 * A combination of input feature values and output feature values that are
 * supplied to an ML for training, validation or data correction.
 * 
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 */
public interface TrainingData<Ie, Ote> extends Cloneable {

	/**
	 * @return the input features
	 */
	public List<Feature> getInputFeatures();

	/**
	 * @return the ordered output features
	 */
	public List<Feature> getOutputFeatures();

	/**
	 * @return the input feature values
	 */
	public List<Ie> getInputFeatureValues();

	/**
	 * @return the output feature values
	 */
	public List<List<Ote>> getOutputFeatureValues();

	/**
	 * Creates a deep copy of the training data.
	 * 
	 * @return a new, deep copy of the training data.
	 */
	public TrainingData<Ie, Ote> clone();

}
