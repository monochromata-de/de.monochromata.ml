package de.monochromata.ml.lowlevel;

import java.util.List;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.TrainingDataCollection;

/**
 * A functional interface to a machine-learning framework like e.g. an
 * implementation of a support-vector machine (
 * <a href="https://en.wikipedia.org/wiki/Support_vector_machine">ML</a>).
 * 
 * <p>
 * The interface may be used fluently in the following way
 * 
 * <pre>
 * {@code
 * svm.trainingData()
 * 	  .populate(dataSource)
 *    .train()
 *    .join()
 *    .correct(uncorrectedEyeContext);
 * }
 * </pre>
 * 
 * TODO: Add the snippet to a test - as it is already outdated
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 * 
 */
public interface ML<M, C, Ie, Ote, Oc, T> {

	/**
	 * Start to construct an ML by creating a set of training data.
	 * 
	 * <p>
	 * TODO: Permit the kernel etc. to be chosen
	 * 
	 * @param inputFeatures
	 *            The list of features that are known for both training and
	 *            correction data.
	 * @param outputFeatures
	 *            A list of additional features that are known for training data
	 *            only and that are estimated by the ML during data correction.
	 *            A separate ML is trained and used for each output feature.
	 * @return the training data to be filled
	 * @throws IllegalStateException
	 *             If this method is invoked more than once on the same
	 *             {@link ML} instance.
	 */
	public TrainingDataCollection<C, Ie, Ote, Oc, T> trainingData(List<Feature> inputFeatures,
			List<Feature> outputFeatures);

	/**
	 * Returns the implementation of the service-provider interface underlying
	 * this ML instance.
	 * 
	 * @return the SPI
	 */
	public MLSpi<M, C, Ie, Ote, Oc, T> getSpi();

	public static <M, C, Ie, Ote, Oc, T> ML<M, C, Ie, Ote, Oc, T> getInstance(final MLSpi<M, C, Ie, Ote, Oc, T> spi) {
		return new ML<M, C, Ie, Ote, Oc, T>() {

			@Override
			public TrainingDataCollection<C, Ie, Ote, Oc, T> trainingData(List<Feature> inputFeatures,
					List<Feature> outputFeatures) {
				return spi.trainingData(inputFeatures, outputFeatures);
			}

			@Override
			public MLSpi<M, C, Ie, Ote, Oc, T> getSpi() {
				return spi;
			}

		};
	}

}
