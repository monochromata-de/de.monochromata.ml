package de.monochromata.ml.lowlevel.training.preprocessing;

import de.monochromata.ml.lowlevel.training.TrainingData;

/**
 * Scaler normalizes input and output values so that the ML parameters obtained
 * during training can be compared between different SVMs.
 *
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 */
public interface Scaler<Ie, Ote> extends Preprocessor<Ie, Ote, TrainingData<Ie, Ote>> {

}
