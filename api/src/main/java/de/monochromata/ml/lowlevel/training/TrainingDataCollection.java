package de.monochromata.ml.lowlevel.training;

import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessing;

/**
 * A container for ML training data that is populated asynchronously and yields
 * a {@link Preprocessing}.
 * 
 * <p>
 * Implementations of this class might not be thread-safe.
 * 
 * <p>
 * TODO: Define the terminology: data, datum, feature, parameter,
 * hyper-parameter, parameter value
 * 
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public interface TrainingDataCollection<C, Ie, Ote, Oc, T> {

	/**
	 * This method is invoked by framework users to fill a training model
	 * synchronously with the provided training data.
	 * 
	 * @param trainingData
	 *            A list of training data.
	 * @return the preprocessing interface
	 * @throws IllegalStateException
	 *             If any of the methods of this interface has already been
	 *             invoked on the current {@link TrainingDataCollection}
	 *             instance
	 */
	public Preprocessing<C, Ie, Ote, Oc, T> populate(final TrainingData<Ie, Ote> trainingData);

	/**
	 * This method is invoked by framework users to fill a training model
	 * asynchronously with training data provided by a populator.
	 * 
	 * <p>
	 * This method does not wait until the first invocation of the populator but
	 * returns immediately.
	 * 
	 * @param populatorUser
	 *            a consumer of the populator used to add data to this instance.
	 * @return a completion stage that will be done when all training data has
	 *         been added by the populator - i.e. when
	 *         {@link TrainingDataPopulator#finish()} has been invoked.
	 * @throws IllegalStateException
	 *             If any of the methods of this interface has already been
	 *             invoked on the current {@link TrainingDataCollection}
	 *             instance.
	 */
	public CompletionStage<Preprocessing<C, Ie, Ote, Oc, T>> populate(
			final Consumer<TrainingDataPopulator> populatorUser);
}
