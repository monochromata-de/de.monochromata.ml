package de.monochromata.ml.lowlevel.training.preprocessing;

import java.util.function.UnaryOperator;

import de.monochromata.ml.lowlevel.training.TrainingData;

/**
 * Preprocessor is applied to training data when all training data has been
 * collected and before the ML is trained. It may modify or eliminate data.
 * (While it is theoretically possible that a preprocessor adds data, I cannot
 * think of such a case right now.)
 * 
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <T>
 *            a type that combines input and output feature values, i.e.
 *            {@link TrainingData} or a sub-type of it
 */
public interface Preprocessor<Ie, Ote, T extends TrainingData<Ie, Ote>> extends UnaryOperator<T> {

	/**
	 * Apply the preprocessor to the given model data.
	 * 
	 * <p>
	 * Implementations of this method operate on the provided training data and
	 * return the provided instance after it has been modified.
	 * 
	 * @param d
	 *            the input training data
	 * @return the preprocessed model data
	 */
	@Override
	T apply(final T d);

}
