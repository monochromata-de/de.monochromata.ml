package de.monochromata.ml.lowlevel;

import java.util.List;
import java.util.concurrent.Executor;

import de.monochromata.ml.crossvalidation.CrossValidation;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;

/**
 * The problem-specific aspects of a configuration of a machine-learning
 * framework.
 * <p>
 * This interface is used to configure complex ML setups, like
 * {@link CrossValidation}.
 * 
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public interface MLConfiguration<Ie, Ote, Oc, T> {

	/**
	 * The executor to use to parallelize execution.
	 * 
	 * @return the executor
	 */
	public Executor getExecutor();

	/**
	 * A description of the input features that are part of each datum.
	 * 
	 * @return the input features
	 */
	public List<Feature> getInputFeatures();

	/**
	 * A description of the output features that are part of each datum and are
	 * estimated by the ML algorithm.
	 * 
	 * @return the outout features
	 */
	public List<Feature> getOutputFeatures();

	/**
	 * Create a new list of new instances of the preprocessors to be applied
	 * during pre-processing.
	 * 
	 * @return the preprocessor
	 */
	public List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> createPreprocessors();

	/**
	 * Create a new list of new instances of the transformers to use during
	 * correction.
	 * 
	 * @param preprocessors
	 *            the pre-processors to base the transformers on
	 * @return the transformers
	 */
	public List<Transformer<Ie, Ote>> createTransformers(
			final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors);

}
