package de.monochromata.ml.lowlevel;

import java.util.List;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.TrainingDataCollection;
import de.monochromata.ml.lowlevel.training.validation.Validation;
import de.monochromata.ml.modelselection.HyperParameterValue;

/**
 * A service-provider interface implemented by machine-learning frameworks in
 * order to implement the ML API.
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public interface MLSpi<M, C, Ie, Ote, Oc, T> {

	public TrainingDataCollection<C, Ie, Ote, Oc, T> trainingData(List<Feature> inputFeatures,
			List<Feature> outputFeatures);

	public List<Ie> copyInputFeatureValues(final List<Ie> inputFeatureValues);

	public List<List<Ote>> copyOutputFeatureValues(final List<List<Ote>> outputFeatureValues);

	public List<Ote> substractOutputFeatureValues(final List<Ote> minuend, final List<Ote> subtrahend);

	public CorrectionDatum<Ie, Ote> read(final T inputToBeCorrected);

	public CorrectionDatum<Ie, Ote> predictingCorrector(final CorrectionDatum<Ie, Ote> input, final Oc models);

	public T write(final CorrectionDatum<Ie, Ote> correctedDatum, final T inputToBeCorrected);

	public Validation<Ie, Ote, T> createValidation();

	// Methods to support choice of hyper-parameters

	public C createConfiguration(final M modelType, final List<HyperParameterValue<?>> hyperParameterValues);

}
