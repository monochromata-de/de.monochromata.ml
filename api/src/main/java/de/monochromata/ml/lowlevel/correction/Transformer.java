package de.monochromata.ml.lowlevel.correction;

import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;

/**
 * A transformer is used during data correction. It is able to apply a
 * transformation and to reverse it.
 *
 * <p>
 * Implementations of this interfaces are initialized with an implementation of
 * {@link Preprocessor}. The {@link #transformInput(CorrectionDatum)} and
 * {@link #invertPreprocessing(CorrectionDatum)} methods are based on the state
 * of a {@link Preprocessor} that has been trained with the training data.
 * 
 * @param <Ie>  the type of elements in the collection of input feature values
 *              in the implementation-specific model of the ML
 * @param <Ote> the type of the elements in the collection of output feature
 *              values in the implementation-specific training model of the ML
 */
public interface Transformer<Ie, Ote> {

	/**
	 * Transforms the input feature values of the given object.
	 * 
	 * <p>
	 * This method is invoked with for a given object after the object has been
	 * passed to {@link CorrectionModel#apply(Object)}.
	 * 
	 * @param datum the object to be transformed
	 * @return the object with transformed input feature values.
	 * @throws IllegalStateException If the transformer is not yet ready, i.e. if
	 *                               the {@link Preprocessor} underlying the
	 *                               transformer has not been trained yet when this
	 *                               method is invoked.
	 * @see #invertPreprocessing(CorrectionDatum)
	 */
	public CorrectionDatum<Ie, Ote> transformInput(final CorrectionDatum<Ie, Ote> datum);

	/**
	 * Inverts the following modifications:
	 * 
	 * <ul>
	 * <li>the transformation that {@link #transformInput(CorrectionDatum)} applied
	 * to the input feature values.
	 * <li>the transformation during training that are performed by the
	 * corresponding {@link Preprocessor} that are reflected by the predicted output
	 * feature values.
	 * </ul>
	 * 
	 * <p>
	 * This method is invoked for objects before they are returned by
	 * {@link CorrectionModel#apply(Object)}.
	 * 
	 * @param datum the transformed datum
	 * @return the object to which the inverse transformation has been applied.
	 * @throws IllegalStateException If the transformer is not yet ready, i.e. if
	 *                               the {@link Preprocessor} underlying the
	 *                               transformer has not been trained yet when this
	 *                               method is invoked.
	 * @see #transformInput(CorrectionDatum)
	 */
	public CorrectionDatum<Ie, Ote> invertPreprocessing(final CorrectionDatum<Ie, Ote> datum);
}
