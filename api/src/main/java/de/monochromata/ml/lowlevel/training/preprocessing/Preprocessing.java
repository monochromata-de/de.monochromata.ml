package de.monochromata.ml.lowlevel.training.preprocessing;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.Training;
import de.monochromata.ml.lowlevel.training.TrainingData;

/**
 * A preprocessing model has been filled with training data and can be used to
 * alter or eliminate values (i.e. to pre-process the training data). A
 * {@link Training} is obtained after preprocessing.
 * 
 * TODO: Das correction model muss dann aber auch das preprocessing auf die
 * echten Daten anwenden und auch wieder rückgängig machen.
 * 
 * @param <C>   the type of the implementation-specific configuration of the ML
 * @param <Ie>  the type of elements in the collection of input feature values
 *              in the implementation-specific model of the ML
 * @param <Ote> the type of the elements in the collection of output feature
 *              values in the implementation-specific training model of the ML
 * @param <Oc>  the type of the collection of output feature values in the
 *              implementation-specific correction model of the ML
 * @param <T>   the type of objects to be corrected by the eventually-obtained
 *              {@link CorrectionModel}
 */
@FunctionalInterface
public interface Preprocessing<C, Ie, Ote, Oc, T> {

	/**
	 * Preprocess the data in the underlying model, yielding a training model.
	 * 
	 * <p>
	 * This method may be invoked multiple times in order to preprocess the model
	 * data with differently. On each invocation, a deep copy of the underlying
	 * {@link TrainingData} is created and supplied to the given preprocessors.
	 * 
	 * <p>
	 * This method uses {@link ForkJoinPool#commonPool()} to perform the
	 * preprocessing asynchronously.
	 * 
	 * @param preprocessors the preprocessors to apply before training
	 * @return a completion stage that eventually contains the training model
	 * @see #preprocess(List, Executor)
	 */
	default CompletionStage<Training<C, Ie, Ote, Oc, T>> preprocess(
			final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors) {
		return preprocess(preprocessors, ForkJoinPool.commonPool());
	}

	/**
	 * Use the given {@link Executor} to preprocess the model data, yielding a
	 * training model.
	 * 
	 * <p>
	 * This method may be invoked multiple times in order to preprocess the data
	 * differently. On each invocation, a deep copy of the underlying
	 * {@link TrainingData} is created and supplied to the given preprocessors.
	 * 
	 * @param preprocessors the pre-processors to apply before training
	 * @param executor      the executor used to preprocess the model data
	 * @return a completion stage that eventually contains the training model
	 */
	public CompletionStage<Training<C, Ie, Ote, Oc, T>> preprocess(
			final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors, final Executor executor);

}
