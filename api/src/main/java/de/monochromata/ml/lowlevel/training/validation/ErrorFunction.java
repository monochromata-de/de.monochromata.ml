package de.monochromata.ml.lowlevel.training.validation;

import static de.monochromata.ml.lowlevel.training.validation.ValidationUtilities.requireSameSize;
import static java.util.Objects.requireNonNull;

import java.util.List;

public interface ErrorFunction {

	/**
	 * Calculate the <a href="https://en.wikipedia.org/wiki/Mean_squared_error">mean
	 * squared error (MSE)</a>.
	 * 
	 * @param truth     the correct values
	 * @param estimates the estimates e.g. produced by a regression model
	 * @return the MSE for the given values
	 */
	static double mse(final List<Double> truth, final List<Double> estimates) {
		requireNonNull(truth, "truth must not be null");
		requireNonNull(estimates, "estimates must not be null");
		requireSameSize(truth, estimates, "truth and estimates must have the same size");
		final int numberOfData = truth.size();
		double total = 0d;
		for (int i = 0; i < numberOfData; i++) {
			total += Math.pow(truth.get(i) - estimates.get(i), 2);
		}
		return total / numberOfData;
	}

	/**
	 * Calculate the
	 * <a href="https://en.wikipedia.org/wiki/Root-mean-square_deviation">root-
	 * mean-square error (RMSE)</a>.
	 * 
	 * @param truth     the correct values
	 * @param estimates the estimates e.g. produced by a regression model
	 * @return the RMSE for the given values
	 */
	static double rmse(final List<Double> truth, final List<Double> estimates) {
		return Math.sqrt(mse(truth, estimates));
	}

}
