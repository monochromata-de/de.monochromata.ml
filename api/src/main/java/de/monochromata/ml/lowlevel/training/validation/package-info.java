/**
 * Different strategies for validating training results.
 */
package de.monochromata.ml.lowlevel.training.validation;