package de.monochromata.ml.lowlevel.training;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.function.BiFunction;
import java.util.function.Function;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;

/**
 * A training model contains pre-processed training data. Via this interface, an
 * ML can be trained in order to obtain a {@link CorrectionModel}.
 * 
 * @param <C>   the type of the implementation-specific configuration of the ML
 * @param <Ie>  the type of elements in the collection of input feature values
 *              in the implementation-specific model of the ML
 * @param <Ote> the type of the elements in the collection of output feature
 *              values in the implementation-specific training model of the ML
 * @param <Oc>  the type of the collection of output feature values in the
 *              implementation-specific correction model of the ML
 * @param <T>   the type of objects to be corrected by the
 *              {@link CorrectionModel} to-be-obtained from this
 *              {@link Training}
 */
@FunctionalInterface
public interface Training<C, Ie, Ote, Oc, T> {

	/**
	 * Train the ML underlying this model, yielding a trained ML that can be used to
	 * correct data or to perform cross-validation.
	 * 
	 * <p>
	 * This method may be invoked multiple times in order to train different SVMs
	 * with different hyper-parameters, e.g. during cross-validation.
	 * 
	 * <p>
	 * This method uses {@link ForkJoinPool#commonPool()} to perform the training
	 * asynchronously.
	 * 
	 * @param configuration the configuration (including hyper-parameters) to be
	 *                      used during training.
	 * @param reader        creates a {@link CorrectionDatum} from an input object
	 *                      of type {@code T}
	 * @param corrector     a function that takes the trained SVMs and a
	 *                      to-be-corrected instance and returns a new corrected
	 *                      instance
	 * @param writer        writes the corrected data from the
	 *                      {@link CorrectionDatum} into the given input object of
	 *                      type {@code T} or creates a new instance of that type.
	 * @return a completion stage that eventually contains the trained model that
	 *         can be used for correction.
	 * @see #train(Object, Function, BiFunction, BiFunction, Executor)
	 */
	default CompletionStage<CorrectionModel<Ie, Ote, T>> train(final C configuration,
			final Function<T, CorrectionDatum<Ie, Ote>> reader,
			final BiFunction<CorrectionDatum<Ie, Ote>, Oc, CorrectionDatum<Ie, Ote>> corrector,
			final BiFunction<CorrectionDatum<Ie, Ote>, T, T> writer) {
		return train(configuration, reader, corrector, writer, ForkJoinPool.commonPool());
	}

	/**
	 * Use the given {@link Executor} to train an ML underlying this model, yielding
	 * a trained ML that can be used to correct data or to perform cross-validation.
	 * 
	 * <p>
	 * This method may be invoked multiple times in order to train different SVMs
	 * with different hyper-parameters, e.g. during cross-validation.
	 * 
	 * @param configuration the configuration (including hyper-parameters) to be
	 *                      used during training.
	 * @param reader        creates a {@link CorrectionDatum} from an input object
	 *                      of type {@code T}
	 * @param corrector     a function that takes the trained SVMs and a
	 *                      to-be-corrected instance and returns a new corrected
	 *                      instance
	 * @param writer        writes the corrected data from the
	 *                      {@link CorrectionDatum} into the given input object of
	 *                      type {@code T} or creates a new instance of that type.
	 * @param executor      the executor used to train the model
	 * @return a completion stage that eventually contains the trained model that
	 *         can be used for correction.
	 */
	public CompletionStage<CorrectionModel<Ie, Ote, T>> train(final C configuration,
			final Function<T, CorrectionDatum<Ie, Ote>> reader,
			final BiFunction<CorrectionDatum<Ie, Ote>, Oc, CorrectionDatum<Ie, Ote>> corrector,
			final BiFunction<CorrectionDatum<Ie, Ote>, T, T> writer, final Executor executor);

}
