package de.monochromata.ml.lowlevel;

import de.monochromata.ml.lowlevel.training.TrainingDataCollection;

/**
 * A double-valued feature in a {@link TrainingDataCollection} instance.
 */
public class Feature {

	private final String name;

	/**
	 * Creates a new feature.
	 * 
	 * @param name
	 *            the name of the feature
	 */
	public Feature(final String name) {
		this.name = name;
	}

	/**
	 * @return the name of the feature
	 */
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Feature other = (Feature) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Feature [name=" + name + "]";
	}

}
