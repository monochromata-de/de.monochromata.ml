package de.monochromata.ml.crossvalidation;

import static de.monochromata.ml.lowlevel.training.validation.ValidationUtilities.requireSameSize;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.ML;
import de.monochromata.ml.lowlevel.MLConfiguration;
import de.monochromata.ml.lowlevel.MLSpi;
import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.DefaultTrainingData;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;
import de.monochromata.ml.lowlevel.training.validation.Validation;

/**
 * Split the data into {@code N} folds, perform {@code N} iterations in which
 * {@code N-1} folds are used for training, while the {@code N}'th fold is used
 * for validation.
 * <p>
 * TODO: If there is a feature-selection step, nested CV needs to be used: outer
 * CV for feature selection and inner CV for hyper-parameter selection.
 * 
 * @param <M>   Model type that determines the hyper-parameters that apply. E.g.
 *              linear, polynomial and RBF kernels are model types.
 * @param <C>   the type of the implementation-specific configuration of the ML
 * @param <Ie>  the type of elements in the collection of input feature values
 *              in the implementation-specific model of the ML
 * @param <Ote> the type of the elements in the collection of output feature
 *              values in the implementation-specific training model of the ML
 * @param <Oc>  the type of the collection of output feature values in the
 *              implementation-specific correction model of the ML
 * @param <T>   the type of objects to be corrected by the eventually-obtained
 *              {@link CorrectionModel}
 */
public class NFoldCrossValidation<M, C, Ie, Ote, Oc, T> implements CrossValidation<M, C, Ie, Ote, Oc, T> {

	private final int numberOfFolds;

	public NFoldCrossValidation(int numberOfFolds) {
		this.numberOfFolds = numberOfFolds;
	}

	public int getNumberOfFolds() {
		return numberOfFolds;
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * Cross-validation will be performed using the number of folds given to the
	 * constructor.
	 * 
	 * @param ml                   {@inheritDoc}
	 * @param problemConfiguration {@inheritDoc}
	 * @param modelConfiguration   {@inheritDoc}
	 * @param errorFunction        {@inheritDoc}
	 * @param data                 The data to use for training and validation
	 *                             during cross-validation.
	 * @param truthValues          The truth values to use for training and
	 *                             validation during cross-validation. A list of
	 *                             lists: the outer list contains the values, the
	 *                             inner list contains the dimensions of the
	 *                             multi-dimensional problem.
	 * @return the error for each dimension (output feature) averaged over the folds
	 */
	@Override
	public Result<Ie, Ote, T> validate(final ML<M, C, Ie, Ote, Oc, T> ml,
			final MLConfiguration<Ie, Ote, Oc, T> problemConfiguration, final C modelConfiguration,
			final BiFunction<List<Ote>, List<Ote>, Double> errorFunction, final List<T> data,
			final List<List<Ote>> truthValues) {
		final List<Fold<Ote, T>> folds = createFolds(data, truthValues);
		final List<CorrectionModel<Ie, Ote, T>> foldModels = new ArrayList<>();
		final List<List<Transformer<Ie, Ote>>> foldTransformers = new ArrayList<>();
		final List<Double> error = createErrorList(problemConfiguration.getOutputFeatures().size());
		final Validation<Ie, Ote, T> validation = ml.getSpi().createValidation();
		for (int i = 0; i < numberOfFolds; i++) {
			final FoldResult<Ie, Ote, T> foldResult = validateWithFold(folds, i, ml, problemConfiguration,
					modelConfiguration, validation, errorFunction);
			foldModels.add(foldResult.getCorrectionModel());
			foldTransformers.add(foldResult.getTransformers());
			updateError(error, foldResult.getErrors());
		}
		final List<Double> averageErrorForEachFold = getAverageErrorForEachFold(error, numberOfFolds);
		return new Result<>(foldModels, foldTransformers, averageErrorForEachFold);
	}

	private List<Double> createErrorList(final int numberOfDimensions) {
		return initializeList(numberOfDimensions, () -> 0d);
	}

	private <E> List<E> initializeList(final int size, final Supplier<E> initializer) {
		final ArrayList<E> list = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			list.add(initializer.get());
		}
		return list;
	}

	private void updateError(final List<Double> totalError, final List<Double> foldError) {
		final int numberOfDimensions = totalError.size();
		for (int i = 0; i < numberOfDimensions; i++) {
			totalError.set(i, totalError.get(i) + foldError.get(i));
		}
	}

	private List<Double> getAverageErrorForEachFold(final List<Double> totalError, final int numberOfFolds) {
		final int numberOfDimensions = totalError.size();
		final List<Double> averageError = new ArrayList<>(numberOfDimensions);
		for (int i = 0; i < numberOfDimensions; i++) {
			averageError.add(totalError.get(i) / numberOfFolds);
		}
		return averageError;
	}

	public FoldResult<Ie, Ote, T> validateWithFold(final List<Fold<Ote, T>> folds, final int indexOfValidationFold,
			final ML<M, C, Ie, Ote, Oc, T> ml, final MLConfiguration<Ie, Ote, Oc, T> problemConfiguration,
			final C modelConfiguration, final Validation<Ie, Ote, T> validation,
			final BiFunction<List<Ote>, List<Ote>, Double> errorFunction) {
		final MLSpi<M, C, Ie, Ote, Oc, T> spi = ml.getSpi();
		final Fold<Ote, T> validationFold = folds.get(indexOfValidationFold);
		final TrainingData<Ie, Ote> trainingData = createTrainingData(spi, problemConfiguration, folds,
				indexOfValidationFold, problemConfiguration.getInputFeatures(),
				problemConfiguration.getOutputFeatures());
		final List<Preprocessor<Ie, Ote, TrainingData<Ie, Ote>>> preprocessors = problemConfiguration
				.createPreprocessors();

		final CorrectionModel<Ie, Ote, T> model = ml
				.trainingData(problemConfiguration.getInputFeatures(), problemConfiguration.getOutputFeatures())
				.populate(trainingData).preprocess(preprocessors, problemConfiguration.getExecutor())
				.thenCompose(training -> training.train(modelConfiguration, spi::read, spi::predictingCorrector,
						spi::write, problemConfiguration.getExecutor()))
				.toCompletableFuture().join();

		final ListSupplier<T> dataSupplier = new ListSupplier<>(validationFold.getData());
		final ListSupplier<List<Ote>> truthSupplier = new ListSupplier<>(validationFold.getTruthValues());
		final List<Transformer<Ie, Ote>> transformers = problemConfiguration.createTransformers(preprocessors);
		final List<Double> validationResult = validation.validate(model, spi::read, transformers, dataSupplier,
				truthSupplier, errorFunction);
		return new FoldResult<>(model, transformers, validationResult);
	}

	public TrainingData<Ie, Ote> createTrainingData(final MLSpi<M, C, Ie, Ote, Oc, T> spi,
			final MLConfiguration<Ie, Ote, Oc, T> configuration, final List<Fold<Ote, T>> folds,
			final int indexOfValidationFold, final List<Feature> inputFeatures, final List<Feature> outputFeatures) {
		final Fold<Ote, T> combinedTrainingFolds = getCombinedTrainingFold(folds, indexOfValidationFold);
		final FeatureValues featureValues = getFeatureValues(spi, configuration, combinedTrainingFolds);
		return new DefaultTrainingData<>(spi, inputFeatures, outputFeatures, featureValues.getInputFeatureValues(),
				featureValues.getOutputFeatureValues());
	}

	protected Fold<Ote, T> getCombinedTrainingFold(final List<Fold<Ote, T>> folds, final int indexOfValidationFold) {
		final List<T> data = new LinkedList<>();
		final List<List<Ote>> truthValues = new LinkedList<>();
		final int numberOfFolds = folds.size();
		for (int i = 0; i < numberOfFolds; i++) {
			if (i != indexOfValidationFold) {
				final Fold<Ote, T> fold = folds.get(i);
				data.addAll(fold.getData());
				truthValues.addAll(fold.getTruthValues());
			}
		}
		return new Fold<>(data, truthValues);
	}

	protected FeatureValues getFeatureValues(final MLSpi<M, C, Ie, Ote, Oc, T> spi,
			final MLConfiguration<Ie, Ote, Oc, T> configuration, final Fold<Ote, T> combinedTrainingFolds) {
		final List<Ie> inputFeatureValues = new LinkedList<>();
		final int numberOfOutputFeatures = configuration.getOutputFeatures().size();
		final List<List<Ote>> outputFeatureValues = initializeList(numberOfOutputFeatures, () -> new LinkedList<>());
		for (final T datum : combinedTrainingFolds.data) {
			final CorrectionDatum<Ie, Ote> correctionDatum = spi.read(datum);
			inputFeatureValues.add(correctionDatum.getInputFeatureValues());
			addOutputFeatureValuesByDimension(outputFeatureValues, correctionDatum.getOutputFeatureValues());
		}
		return new FeatureValues(inputFeatureValues, outputFeatureValues);
	}

	protected void addOutputFeatureValuesByDimension(final List<List<Ote>> allOutputFeatureValues,
			final List<Ote> newOutputFeatureValues) {
		final int numberOfDimensions = allOutputFeatureValues.size();
		for (int i = 0; i < numberOfDimensions; i++) {
			allOutputFeatureValues.get(i).add(newOutputFeatureValues.get(i));
		}
	}

	/**
	 * Split the data into a given number of folds.
	 * <p>
	 * If the number of data is not divisible by the number of folds, some data will
	 * be ignored.
	 */
	public List<Fold<Ote, T>> createFolds(final List<T> data, final List<List<Ote>> truthValues) {
		requireSameSize(data, truthValues, "data.size() != truthValues.size()");
		final ArrayList<Fold<Ote, T>> folds = new ArrayList<>(numberOfFolds);
		final int foldSize = data.size() / numberOfFolds;
		for (int i = 0; i < numberOfFolds; i++) {
			int fromIndexInclusive = i * foldSize;
			int toIndexExclusive = (i + 1) * foldSize;
			final List<T> foldData = unmodifiableList(data.subList(fromIndexInclusive, toIndexExclusive));
			final List<List<Ote>> foldTruthValues = unmodifiableList(
					truthValues.subList(fromIndexInclusive, toIndexExclusive));
			folds.add(new Fold<>(foldData, foldTruthValues));
		}
		return folds;
	}

	public static class Fold<Ote, T> {

		private final List<T> data;

		/**
		 * A list of lists: the outer list contains the values, the inner list contains
		 * the dimensions of the multi-dimensional problem.
		 */
		private final List<List<Ote>> truthValues;

		public Fold(final List<T> data, final List<List<Ote>> truthValues) {
			this.data = data;
			this.truthValues = truthValues;
		}

		public List<T> getData() {
			return data;
		}

		public List<List<Ote>> getTruthValues() {
			return truthValues;
		}

	}

	public static class FoldResult<Ie, Ote, T> {

		private final CorrectionModel<Ie, Ote, T> correctionModel;
		private final List<Transformer<Ie, Ote>> transformers;
		private final List<Double> errors;

		public FoldResult(final CorrectionModel<Ie, Ote, T> correctionModel,
				final List<Transformer<Ie, Ote>> transformers, final List<Double> errors) {
			this.correctionModel = correctionModel;
			this.transformers = transformers;
			this.errors = errors;
		}

		public CorrectionModel<Ie, Ote, T> getCorrectionModel() {
			return correctionModel;
		}

		public List<Transformer<Ie, Ote>> getTransformers() {
			return transformers;
		}

		public List<Double> getErrors() {
			return errors;
		}

	}

	private class FeatureValues {

		private final List<Ie> inputFeatureValues;

		/**
		 * A list of lists: the outer list contains the dimensions of the
		 * multi-dimensional problem, the inner list contains the values.
		 */
		private final List<List<Ote>> outputFeatureValues;

		public FeatureValues(final List<Ie> inputFeatureValues, final List<List<Ote>> outputFeatureValues) {
			this.inputFeatureValues = inputFeatureValues;
			this.outputFeatureValues = outputFeatureValues;
		}

		public List<Ie> getInputFeatureValues() {
			return inputFeatureValues;
		}

		public List<List<Ote>> getOutputFeatureValues() {
			return outputFeatureValues;
		}

	}

	private static class ListSupplier<A> implements Supplier<A> {

		private final Iterator<A> values;

		public ListSupplier(final List<A> values) {
			this.values = values.iterator();
		}

		@Override
		public A get() {
			if (values.hasNext()) {
				return values.next();
			}
			return null;
		}

	}

}
