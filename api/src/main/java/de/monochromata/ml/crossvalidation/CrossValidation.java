package de.monochromata.ml.crossvalidation;

import java.util.List;
import java.util.function.BiFunction;

import de.monochromata.ml.lowlevel.ML;
import de.monochromata.ml.lowlevel.MLConfiguration;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;

/**
 * A interface for
 * <a href="https://en.wikipedia.org/wiki/Cross-validation_(statistics)">cross-
 * validation</a>.
 * 
 * @param <M>
 *            Model type that determines the hyper-parameters that apply. E.g.
 *            linear, polynomial and RBF kernels are model types.
 * @param <C>
 *            the type of the implementation-specific configuration of the ML
 * @param <Ie>
 *            the type of elements in the collection of input feature values in
 *            the implementation-specific model of the ML
 * @param <Ote>
 *            the type of the elements in the collection of output feature
 *            values in the implementation-specific training model of the ML
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public interface CrossValidation<M, C, Ie, Ote, Oc, T> {

	/**
	 * Perform cross-validation.
	 * 
	 * @param ml
	 *            The ML framework to use
	 * @param problemConfiguration
	 *            The problem-specific configuration of the ML framework
	 * @param modelConfiguration
	 *            The framework-specific model configuration
	 * @param errorFunction
	 *            The error function to compute error values
	 * @param data
	 *            The data to use for training and validation during
	 *            cross-validation. The data will be split in an
	 *            implementation-specific way.
	 * @param truthValues
	 *            The truth values to use for training and validation during
	 *            cross-validation. The data will be split in an
	 *            implementation-specific way.
	 * @return the validation result
	 */
	public Result<Ie, Ote, T> validate(final ML<M, C, Ie, Ote, Oc, T> ml,
			final MLConfiguration<Ie, Ote, Oc, T> problemConfiguration, final C modelConfiguration,
			final BiFunction<List<Ote>, List<Ote>, Double> errorFunction, final List<T> data,
			final List<List<Ote>> truthValues);

	class Result<Ie, Ote, T> {

		private final List<CorrectionModel<Ie, Ote, T>> correctionModels;
		private final List<List<Transformer<Ie, Ote>>> transformers;
		private final List<Double> averageErrorByDimension;

		/**
		 * Create a new cross-validation result.
		 * 
		 * @param correctionModels
		 *            all correction models created during cross-validation
		 * @param transformers
		 *            the transformers to use in conjunction with all correction
		 *            models created during cross-validation
		 * @param averageErrorByDimension
		 *            the error for each dimension (output feature) averaged in
		 *            a way that is specific to the cross-validation
		 *            implementation.
		 */
		public Result(final List<CorrectionModel<Ie, Ote, T>> correctionModels,
				final List<List<Transformer<Ie, Ote>>> transformers, final List<Double> averageErrorByDimension) {
			this.correctionModels = correctionModels;
			this.transformers = transformers;
			this.averageErrorByDimension = averageErrorByDimension;
		}

		public List<CorrectionModel<Ie, Ote, T>> getCorrectionModels() {
			return correctionModels;
		}

		public List<List<Transformer<Ie, Ote>>> getTransformers() {
			return transformers;
		}

		/**
		 * @return the error for each dimension (output feature) averaged in a
		 *         way that is specific to the cross-validation implementation.
		 */
		public List<Double> getAverageErrorByDimension() {
			return averageErrorByDimension;
		}

	}
}
