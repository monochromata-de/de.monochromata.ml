package de.monochromata.ml.modelselection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;

import org.junit.Test;

public class RatioHyperParameterTest {

	@Test
	public void aMaximumThatIsLowerThanTheMinimumYieldsAnException() {
		assertThatThrownBy(() -> {
			new RatioHyperParameter("maximum is lower than minimum", 10, 5);
		}).isInstanceOf(IllegalArgumentException.class).hasMessage("Maximum must be greater than minimum");
	}

	@Test
	public void anExceptionIsThrownIfMinimumAndMaximumAreEqual() {
		assertThatThrownBy(() -> {
			new RatioHyperParameter("minimum and maximum are equal", 3, 3);
		}).isInstanceOf(IllegalArgumentException.class).hasMessage("Minimum and maximum must differ");
	}

	@Test
	public void anExceptionIsThrownIfGeneratorStepSizeIsLessThanTwo() {
		assertThatThrownBy(() -> {
			RatioHyperParameter.gridGenerator(1);
		}).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Step size must be at least 2 so minimum and maximum are covered. "
						+ "Use a NominalHyperParameter if you want to set a single value.");
	}

	@Test
	@SuppressWarnings("unchecked")
	public void generatorFunctionCreatesEquidistantValuesIncludingMinAndMax() {
		final RatioHyperParameter hyperParameter = new RatioHyperParameter("test", -3, 10);
		final List<HyperParameterValue<? extends Object>> values = RatioHyperParameter.gridGenerator(4)
				.apply(hyperParameter);
		assertThat(values).containsExactly(new HyperParameterValue<Double>(hyperParameter, -3.0d),
				new HyperParameterValue<>(hyperParameter, -3 + (13 / 3d)),
				new HyperParameterValue<>(hyperParameter, -3 + (2 * (13 / 3d))),
				new HyperParameterValue<>(hyperParameter, 10.0d));
	}

}
