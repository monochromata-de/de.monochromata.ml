package de.monochromata.ml.training.validation.error;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.monochromata.ml.lowlevel.training.validation.ErrorFunction;

public class ErrorFunctionTest {

	@Test
	public void mseIsCalculatedCorrectly() {
		final List<Double> truth = Arrays.asList(0d, 0d, 2d, 3d);
		final List<Double> estimates = Arrays.asList(1d, 2d, 3d, 4d);
		final double mse = ErrorFunction.mse(truth, estimates);
		assertThat(mse).isEqualTo(7 / 4d);
	}

	@Test
	public void rmseIsCalculatedCorrectly() {
		final List<Double> truth = Arrays.asList(0d, 0d, 2d, 3d);
		final List<Double> estimates = Arrays.asList(2d, 2d, 4d, 5d);
		final double rmse = ErrorFunction.rmse(truth, estimates);
		assertThat(rmse).isEqualTo(2d);
	}

}
