package de.monochromata.ml.training.validation;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;

import org.junit.Test;

import de.monochromata.ml.crossvalidation.NFoldCrossValidation;
import de.monochromata.ml.crossvalidation.NFoldCrossValidation.Fold;

public class NFoldCrossValidationTest {

	@Test
	public void foldsCannotBeCreatedIfDataAndTruthValuesHaveDifferentSize() {
		assertThatThrownBy(() -> {
			final List<Double> data = asList(0d);
			final List<List<Double>> truthValues = asList(singletonList(0d), singletonList(1d));
			final int numberOfFolds = 10;
			new NFoldCrossValidation<Object, Object, Object, Double, Object, Double>(numberOfFolds).createFolds(data,
					truthValues);
		}).isInstanceOf(IllegalArgumentException.class).hasMessage("data.size() != truthValues.size() (1 != 2)");
	}

	@Test
	public void create2FoldsFrom6Data() {
		final List<Double> data = asList(0d, 1d, 2d, 3d, 4d, 5d);
		final List<List<Double>> truthValues = asList(singletonList(0d), singletonList(1d), singletonList(2d),
				singletonList(3d), singletonList(4d), singletonList(5d));
		final int numberOfFolds = 2;
		final List<Fold<Double, Double>> folds = new NFoldCrossValidation<Object, Object, Object, Double, Object, Double>(
				numberOfFolds).createFolds(data, truthValues);

		assertThat(folds).hasSize(2);
		assertFoldSize(folds.get(0), 3);
		assertFoldSize(folds.get(1), 3);
	}

	@Test
	public void ifTheDataIsNotDivisibleByTheNumberOfFoldsSomeDataIsIgnored() {
		final List<Double> data = asList(0d, 1d, 2d, 3d, 4d);
		final List<List<Double>> truthValues = asList(singletonList(0d), singletonList(1d), singletonList(2d),
				singletonList(3d), singletonList(4d));
		final int numberOfFolds = 2;
		final List<Fold<Double, Double>> folds = new NFoldCrossValidation<Object, Object, Object, Double, Object, Double>(
				numberOfFolds).createFolds(data, truthValues);

		assertThat(folds).hasSize(2);
		assertFoldSize(folds.get(0), 2);
		assertFoldSize(folds.get(1), 2);
	}

	private void assertFoldSize(final Fold<Double, Double> fold, final int expectedSize) {
		assertThat(fold.getData()).hasSize(expectedSize);
		assertThat(fold.getTruthValues()).hasSize(expectedSize);
	}

}
