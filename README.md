# A Basic Machine-Learning Framework for Java

[![pipeline status](https://gitlab.com/monochromata-de/de.monochromata.ml/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/de.monochromata.ml/commits/master)

The library provide a basic API and an implementation of the API based on LIBSVM. The [Mavenized version of the LIBSVM library provided by Datumbox](https://github.com/datumbox/libsvm) is used.  

* It is available from a Maven repository at https://monochromata-de.gitlab.io/de.monochromata.ml/m2/ .
* The Maven site is available at https://monochromata-de.gitlab.io/de.monochromata.ml/ .
* An Eclipse p2 repository is available at https://monochromata-de.gitlab.io/de.monochromata.ml/p2repo .

## Links

The project is used by
[de.monochromata.eyetracking](https://gitlab.com/monochromata-de/de.monochromata.eyetracking/).

## License

LGPL