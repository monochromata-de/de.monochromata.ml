# Release 1.0.122

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.9

# Release 1.0.121

* Update org.slf4j.version to v1.7.35

# Release 1.0.120

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.3

# Release 1.0.119

* Update dependency de.monochromata.plot:plot to v1.0.55

# Release 1.0.118

* Update dependency org.codehaus.mojo:versions-maven-plugin to v2.9.0

# Release 1.0.117

* Update org.slf4j.version to v1.7.33

# Release 1.0.116

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.2

# Release 1.0.115

* Update dependency de.monochromata.plot:plot to v1.0.54

# Release 1.0.114

* Update dependency org.reficio:p2-maven-plugin to v2

# Release 1.0.113

* Update dependency org.assertj:assertj-core to v3.22.0

# Release 1.0.112

* Update dependency de.monochromata.plot:plot to v1.0.52

# Release 1.0.111

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.10.0

# Release 1.0.110

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.1

# Release 1.0.109

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8

# Release 1.0.108

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7.2
* Merge version numbers

# Release 1.0.107

* Update dependency org.assertj:assertj-core to v3.21.0

# Release 1.0.106

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.1

# Release 1.0.105

* Update dependency de.monochromata.plot:aggregator to 1.0.50

# Release 1.0.104

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7.1

# Release 1.0.103

* Update org.slf4j.version to v1.7.32

# Release 1.0.102

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7

# Release 1.0.101

* Update dependency de.monochromata.plot:aggregator to 1.0.49

# Release 1.0.100

* Update dependency org.assertj:assertj-core to v3.20.2

# Release 1.0.99

* Update org.slf4j.version to v1.7.31
* Merge version numbers

# Release 1.0.98

* Update dependency org.assertj:assertj-core to v3.20.1

# Release 1.0.97

* Update dependency org.reficio:p2-maven-plugin to v1.7.0

# Release 1.0.96

* Update dependency de.monochromata.plot:aggregator to 1.0.47

# Release 1.0.95

* Update dependency de.monochromata.plot:plot to v1.0.46

# Release 1.0.94

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.0

# Release 1.0.93

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.6.1

# Release 1.0.92

* Update dependency org.jacoco:jacoco-maven-plugin to v0.8.7

# Release 1.0.91

* Update dependency de.monochromata.plot:aggregator to 1.0.45

# Release 1.0.90

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.2

# Release 1.0.89

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.6

# Release 1.0.88

* Update dependency org.reficio:p2-maven-plugin to v1.6.0

# Release 1.0.87

* Update dependency de.monochromata.plot:aggregator to 1.0.44

# Release 1.0.86

* fix bundling instructions for libsvm

# Release 1.0.85

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.5

# Release 1.0.84

* Update dependency de.monochromata.plot:aggregator to 1.0.43

# Release 1.0.83

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.4
* Merge version numbers

# Release 1.0.82

* Update dependency de.monochromata.plot:aggregator to 1.0.42
* Update dependency org.assertj:assertj-core to v3.19.0

# Release 1.0.81

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.2
* Merge version numbers

# Release 1.0.80

* Update dependency de.monochromata.plot:aggregator to 1.0.41
* Update dependency org.assertj:assertj-core to v3.18.1
* Merge version numbers

# Release 1.0.79

* Update dependency de.monochromata.plot:aggregator to 1.0.40
* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.1

# Release 1.0.78

* Merge remote-tracking branch 'origin/master' into renovate/nl.jqno.equalsverifier-equalsverifier-3.x

# Release 1.0.77

* Merge remote-tracking branch 'origin/master' into renovate/org.assertj-assertj-core-3.x

# Release 1.0.76

* Merge remote-tracking branch 'origin/master' into renovate/org.codehaus.mojo-versions-maven-plugin-2.x

# Release 1.0.75

* Update dependency de.monochromata.plot:aggregator to 1.0.39
* Update dependency de.monochromata.plot:aggregator to 1.0.38
* Merge version numbers

# Release 1.0.74

* Update dependency de.monochromata.plot:aggregator to 1.0.37
* Merge remote-tracking branch 'origin/master' into renovate/org.apache.maven.plugins-maven-project-info-reports-plugin-3.x

# Release 1.0.73

* Specify repository for parent POM
* Correct use of baseUri
* Update dependency de.monochromata.plot:aggregator to 1.0.36
* Update dependency de.monochromata.plot:aggregator to 1.0.35
* Update dependency de.monochromata.plot:aggregator to 1.0.34
* Also release the parent POM
* Update JaCoCo 0.8.5 -> 0.8.6
* Add plot m2 repository
* Publish m2 and p2 repos to GitLab Pages
* Update dependency de.monochromata.plot:aggregator to 1.0.33
* Revert "Upload to Gitlab repository"
* Revert "Correct repository URL for gitlab.com"
* Revert "Use project ID instead of group name in distribution URL"
* Use project ID instead of group name in distribution URL
* Correct repository URL for gitlab.com
* Upload to Gitlab repository

# Release 1.0.72

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.1

# Release 1.0.71

* Update dependency de.monochromata.plot:aggregator to 1.0.32

# Release 1.0.70

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4.1

# Release 1.0.69

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.0

# Release 1.0.68

* Update dependency de.monochromata.plot:aggregator to 1.0.31

# Release 1.0.67

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.3

# Release 1.0.66

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.2

# Release 1.0.65

* Update dependency de.monochromata.plot:aggregator to 1.0.30

# Release 1.0.64

* Update dependency org.assertj:assertj-core to v3.16.1

# Release 1.0.63

* Update dependency org.assertj:assertj-core to v3.16.0

# Release 1.0.62

* Update dependency de.monochromata.plot:aggregator to 1.0.29

# Release 1.0.61

* Update dependency org.apache.maven.wagon:wagon-ssh to v3.4.0

# Release 1.0.60

* Update dependency de.monochromata.plot:aggregator to 1.0.28

# Release 1.0.59

* Update dependency de.monochromata.plot:aggregator to 1.0.27

# Release 1.0.58

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.2.0

# Release 1.0.57

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.1.13

# Release 1.0.56

* Update dependency de.monochromata.plot:aggregator to 1.0.26

# Release 1.0.55

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.0

# Release 1.0.54

* Update dependency de.monochromata.plot:aggregator to 1.0.25

# Release 1.0.53

* Update dependency org.assertj:assertj-core to v3.15.0

# Release 1.0.52

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.1.12

# Release 1.0.51

* No changes

# Release 1.0.50

* No changes

# Release 1.0.49

* No changes

# Release 1.0.48

* No changes

# Release 1.0.47

* No changes

# Release 1.0.46

* No changes

# Release 1.0.45

* No changes

# Release 1.0.44

* No changes

# Release 1.0.43

* No changes

# Release 1.0.42

* No changes

# Release 1.0.41

* No changes

# Release 1.0.40

* No changes

# Release 1.0.39

* No changes

# Release 1.0.38

* No changes

# Release 1.0.37

* No changes

# Release 1.0.36

* No changes

