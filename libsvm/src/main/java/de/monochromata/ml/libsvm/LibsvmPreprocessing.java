package de.monochromata.ml.libsvm;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;

import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.Training;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessing;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

/**
 * A preprocessing model for LIBSVM.
 * 
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public class LibsvmPreprocessing<T> implements Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T> {

	private final TrainingData<svm_node[], Double> trainingData;

	public LibsvmPreprocessing(final TrainingData<svm_node[], Double> trainingData) {
		this.trainingData = trainingData;
	}

	@Override
	public CompletionStage<Training<svm_parameter, svm_node[], Double, List<svm_model>, T>> preprocess(
			final List<Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>>> preprocessors,
			final Executor executor) {
		TrainingData<svm_node[], Double> deepTrainingDataCopy = trainingData.clone();
		for (final Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>> preprocessor : preprocessors) {
			deepTrainingDataCopy = preprocessor.apply(deepTrainingDataCopy);
		}
		return CompletableFuture.completedFuture(new LibsvmTraining<>(deepTrainingDataCopy));
	}

}
