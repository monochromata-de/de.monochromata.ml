package de.monochromata.ml.libsvm;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.Transformer;
import libsvm.svm_node;

/**
 * A transformer that scales correction data input feature values in the way
 * that {@link LibsvmScaler} scales training data. This transformer is also able
 * to invert the scaling of output feature values after correction.
 */
public class LibsvmTransformer implements Transformer<svm_node[], Double> {

	private final LibsvmScaler scaler;

	/**
	 * Create a new instance.
	 * 
	 * @param scaler
	 *            the underlying scaler that is also used during preprocessing
	 */
	public LibsvmTransformer(final LibsvmScaler scaler) {
		this.scaler = scaler;
	}

	public static LibsvmTransformer fromMemento(final Serializable memento) {
		return new LibsvmTransformer((LibsvmScaler) memento);
	}

	public Serializable getMemento() {
		return scaler;
	}

	@Override
	public CorrectionDatum<svm_node[], Double> transformInput(final CorrectionDatum<svm_node[], Double> datum) {
		applyScaler(datum, this::readInput, scaler::scaleInputFeatureValues, this::writeInput);
		return datum;
	}

	@Override
	public CorrectionDatum<svm_node[], Double> invertPreprocessing(final CorrectionDatum<svm_node[], Double> datum) {
		CorrectionDatum<svm_node[], Double> tmp = datum;
		tmp = applyScaler(tmp, this::readInput, scaler::invertScalingOfInputFeatureValues, this::writeInput);
		tmp = applyScaler(tmp, this::readOutput, scaler::invertScalingOfOutputFeatureValues, this::writeOutput);
		return tmp;
	}

	private double[] readInput(final CorrectionDatum<svm_node[], Double> datum) {
		return Arrays.stream(datum.getInputFeatureValues()).map(node -> node.value).mapToDouble(dbl -> dbl).toArray();
	}

	private CorrectionDatum<svm_node[], Double> writeInput(final CorrectionDatum<svm_node[], Double> datum,
			final double[] scaledData) {
		for (int i = 0; i < scaledData.length; i++) {
			datum.getInputFeatureValues()[i].value = scaledData[i];
		}
		return datum;
	}

	private double[] readOutput(final CorrectionDatum<svm_node[], Double> datum) {
		return datum.getOutputFeatureValues().stream().mapToDouble(dbl -> dbl).toArray();
	}

	private CorrectionDatum<svm_node[], Double> writeOutput(final CorrectionDatum<svm_node[], Double> datum,
			final double[] scaledData) {
		final List<Double> newOutputFeatureValues = stream(scaledData).mapToObj(dbl -> (Double) dbl).collect(toList());
		return LibsvmCorrectionDatum.of(datum.getInputFeatureValues(), newOutputFeatureValues);
	}

	private CorrectionDatum<svm_node[], Double> applyScaler(final CorrectionDatum<svm_node[], Double> datum,
			final Function<CorrectionDatum<svm_node[], Double>, double[]> getter,
			final UnaryOperator<double[]> scalerCallback,
			final BiFunction<CorrectionDatum<svm_node[], Double>, double[], CorrectionDatum<svm_node[], Double>> setter) {
		return setter.apply(datum, scalerCallback.apply(getter.apply(datum)));
	}
}
