package de.monochromata.ml.libsvm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import libsvm.svm_problem;

/**
 * A multi-dimensional problem statement i.e. a problem statement with a
 * multi-dimensional label that is sought.
 */
public class Problems {

	private final List<svm_problem> problems;

	/**
	 * Creates a new multi-dimensional problem.
	 * 
	 * @param problems
	 *            the 1-dimensional sub-problems for each dimension of the
	 *            multi-dimensional problem
	 */
	public Problems(final svm_problem... problems) {
		this(Arrays.asList(problems));
	}

	/**
	 * Creates a new multi-dimensional problem.
	 * 
	 * @param problems
	 *            the 1-dimensional sub-problems for each dimension of the
	 *            multi-dimensional problem
	 */
	public Problems(final List<svm_problem> problems) {
		this.problems = problems;
	}

	protected List<svm_problem> getProblems() {
		return Collections.unmodifiableList(problems);
	}

}
