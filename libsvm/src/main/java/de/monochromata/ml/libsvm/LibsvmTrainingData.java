package de.monochromata.ml.libsvm;

import static java.util.stream.Collectors.toList;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.MLSpi;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.DefaultTrainingData;
import libsvm.svm_node;
import libsvm.svm_parameter;

/**
 * A default implementation of a model data container.
 * 
 * @param <Oc>
 *            the type of the collection of output feature values in the
 *            implementation-specific correction model of the ML
 * @param <T>
 *            the type of objects to be corrected by the eventually-obtained
 *            {@link CorrectionModel}
 */
public class LibsvmTrainingData<Oc, T>
		extends DefaultTrainingData<ModelType, svm_parameter, svm_node[], Double, Oc, T> {

	/**
	 * Creates new empty training data.
	 * 
	 * @param inputFeatures
	 *            the input features
	 * @param outputFeatures
	 *            the output features
	 * @see #LibsvmTrainingData(MLSpi, List, List, List, List)
	 * @throws NullPointerException
	 *             If any of the arguments is {@code null}, or if
	 *             {@code outputFeatureValues} contains {@code null} values
	 */
	public LibsvmTrainingData(final MLSpi<ModelType, svm_parameter, svm_node[], Double, Oc, T> spi,
			final List<Feature> inputFeatures, final List<Feature> outputFeatures) {
		super(spi, inputFeatures, outputFeatures, new LinkedList<>(),
				createOutputFeatureValuesWithSubLists(outputFeatures));
	}

	private static List<List<Double>> createOutputFeatureValuesWithSubLists(final List<Feature> outputFeatures) {
		return outputFeatures.stream().map(feature -> new LinkedList<Double>()).collect(toList());
	}

	public LibsvmTrainingData(final MLSpi<ModelType, svm_parameter, svm_node[], Double, Oc, T> spi,
			final List<Feature> inputFeatures, final List<Feature> outputFeatures,
			final List<svm_node[]> inputFeatureValues, final List<List<Double>> outputFeatureValues) {
		super(spi, inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);
	}

}
