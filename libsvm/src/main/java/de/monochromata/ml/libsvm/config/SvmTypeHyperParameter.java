package de.monochromata.ml.libsvm.config;

import libsvm.svm_parameter;

public enum SvmTypeHyperParameter {

	/**
	 * The cost parameter of C-SVC, epsilon-SVR, and nu-SVR ({@code double},
	 * default 1).
	 */
	Cost,

	/**
	 * The parameter nu of nu-SVC, one-class SVM, and nu-SVR ({@code double},
	 * default 0.5).
	 */
	Nu,

	/**
	 * The parameter epsilon in loss function of epsilon-SVR ({@code double},
	 * default 0.1).
	 */
	Epsilon,

	/**
	 * The parameter C of the ith class shall be weight*C, for C-SVC (
	 * {@code double[]}, default 1)
	 */
	Weights;

	public void setIn(final svm_parameter parameters, final Object value) {
		switch (this) {
		case Cost:
			parameters.C = (double) value;
			break;
		case Nu:
			parameters.nu = (double) value;
			break;
		case Epsilon:
			parameters.p = (double) value;
			break;
		case Weights:
			final double[] weights = (double[]) value;
			parameters.nr_weight = weights.length;
			parameters.weight = weights;
			break;
		default:
			throw new IllegalStateException("Unknown svm-type hyper-parameter: " + name());
		}
	}

}