package de.monochromata.ml.libsvm;

import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.stream.IntStream;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.TrainingDataPopulator;
import libsvm.svm_node;

/**
 * A {@link TrainingDataPopulator} for LIBSVM.
 */
public class LibsvmTrainingDataPopulator implements TrainingDataPopulator {

	private final TrainingData<svm_node[], Double> trainingData;
	private final FinishListener finishListener;
	private State state = new ReadyToPopulate();

	/**
	 * Creates a new training populator.
	 * 
	 * @param trainingData
	 *            the training data to be populated by this instance
	 * @see #LibsvmTrainingDataPopulator(TrainingData, FinishListener)
	 * @throws NullPointerException
	 *             If the given trainingData is {@code null}
	 */
	public LibsvmTrainingDataPopulator(final TrainingData<svm_node[], Double> trainingData) {
		this(trainingData, null);
	}

	/**
	 * Creates a new training populator.
	 * 
	 * @param trainingData
	 *            the training data to be populated by this instance
	 * @param finishListener
	 *            a listener that is informed when the populator's
	 *            {@link #finish()} has been invoked.
	 * @throws NullPointerException
	 *             If the given trainingData is {@code null}
	 */
	public LibsvmTrainingDataPopulator(final TrainingData<svm_node[], Double> trainingData,
			final FinishListener finishListener) {
		requireNonNull(trainingData, "trainingData must not be null");
		this.trainingData = trainingData;
		this.finishListener = finishListener;
	}

	@Override
	public void addDatum(final double[] inputFeatureValues, final double[] outputFeatureValues) {
		state.addDatum(inputFeatureValues, outputFeatureValues);
	}

	@Override
	public void finish() {
		state.finish();
		if (finishListener != null) {
			finishListener.finished(this);
		}
	}

	TrainingData<svm_node[], Double> getTrainingData() {
		return trainingData;
	}

	private interface State extends TrainingDataPopulator {

	}

	private class ReadyToPopulate implements State {

		@Override
		public void addDatum(final double[] inputFeatureValues, final double[] outputFeatureValues) {
			ensureThatFeatureNumberMatchesNumberOfValues(inputFeatureValues, outputFeatureValues);
			addInputValues(inputFeatureValues);
			addOutputValues(outputFeatureValues);
		}

		private void ensureThatFeatureNumberMatchesNumberOfValues(final double[] inputFeatureValues,
				final double[] outputFeatureValues) {
			ensureThatFeatureNumberMatchesNumberOfValues(trainingData.getInputFeatures(), inputFeatureValues);
			ensureThatFeatureNumberMatchesNumberOfValues(trainingData.getOutputFeatures(), outputFeatureValues);
		}

		private void ensureThatFeatureNumberMatchesNumberOfValues(final Collection<Feature> features,
				final double[] featureValues) {
			if (features.size() != featureValues.length) {
				throw new IllegalArgumentException("The number of provided feature values (" + featureValues.length
						+ ")" + " does not match required number of feature values (" + features.size() + ")");
			}
		}

		private void addInputValues(final double[] inputFeatureValues) {
			final svm_node[] nodes = new svm_node[trainingData.getInputFeatures().size()];
			for (int i = 0; i < inputFeatureValues.length; i++) {
				nodes[i] = new svm_node();
				nodes[i].index = i + 1;
				nodes[i].value = inputFeatureValues[i];
			}
			trainingData.getInputFeatureValues().add(nodes);
		}

		private void addOutputValues(final double[] outputFeatureValues) {
			IntStream.range(0, outputFeatureValues.length).forEach(i -> {
				trainingData.getOutputFeatureValues().get(i).add(outputFeatureValues[i]);
			});
		}

		@Override
		public void finish() {
			state = new Finished();
		}

	}

	private class Finished implements State {

		@Override
		public void addDatum(final double[] inputFeatureValues, final double[] outputFeatureValues) {
			throw new IllegalStateException("No more data may be added to a finished populator.");
		}

		@Override
		public void finish() {
			throw new IllegalStateException("A finished populator may not be finished a second time.");
		}

	}

	/**
	 * A listener that will be informed when the populator has been finished.
	 */
	public interface FinishListener {

		/**
		 * Invoked once before {@link LibsvmTrainingDataPopulator#finish()}
		 * returns.
		 * 
		 * @param populator
		 *            the populator that has been finished.
		 */
		public void finished(final LibsvmTrainingDataPopulator populator);

	}

}
