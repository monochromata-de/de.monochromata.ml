package de.monochromata.ml.libsvm;

import libsvm.svm_node;

/**
 * Utility methods for using Libsvm.
 */
public interface LibsvmSupport {

	/**
	 * Create an array of {@link svm_node}s from the given double values.
	 * 
	 * @param values
	 *            the input
	 * @return the output
	 */
	default svm_node[] svm_nodes(final double... values) {
		final svm_node[] nodes = new svm_node[values.length];
		for (int i = 0; i < values.length; i++) {
			nodes[i] = svm_node(i, values[i]);
		}
		return nodes;
	}

	/**
	 * Create an {@link svm_node}.
	 * 
	 * @param index
	 *            the index of the node
	 * @param value
	 *            the value of the node
	 * @return the node
	 */
	default svm_node svm_node(final int index, final double value) {
		final svm_node node = new svm_node();
		node.index = index;
		node.value = value;
		return node;
	}

}
