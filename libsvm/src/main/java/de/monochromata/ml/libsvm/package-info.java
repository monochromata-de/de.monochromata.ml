/**
 * An implementation of the ML interfaces based on
 * <a href="https://www.csie.ntu.edu.tw/~cjlin/libsvm/">LIBSVM</a>. See also the
 * <a href="http://www.csie.ntu.edu.tw/~cjlin/papers/libsvm.pdf">LIBSVM
 * paper</a>.
 */
package de.monochromata.ml.libsvm;