package de.monochromata.ml.libsvm.config;

import static java.util.Collections.singletonList;

import java.util.Arrays;
import java.util.List;

import de.monochromata.ml.modelselection.HyperParameter;
import de.monochromata.ml.modelselection.NominalHyperParameter;
import libsvm.svm_parameter;

public enum TechnicalHyperParameter {

	/**
	 * The size of the internal cache used by LIBSVM, in megabytes (
	 * {@code double}, defaults to 100).
	 */
	CacheSizeMB,

	/**
	 * The tolerance of the termination criterion ({@code double}, default
	 * 0.001).
	 */
	EpsilonTerminationCriterion,

	/**
	 * Whether or not to use shrinking heuristics ({@code int}, 0 for false, 1
	 * for true, default 1).
	 */
	UseShrinkingHeuristics,

	/**
	 * Whether or not to train the SVM for probability estimates, ({@code int},
	 * 0 for false, 1 for true, default 0).
	 */
	TrainForProbabilityEstimates;

	public void setIn(final svm_parameter parameters, final Object value) {
		switch (this) {
		case CacheSizeMB:
			parameters.cache_size = (int) value;
			break;
		case EpsilonTerminationCriterion:
			parameters.eps = (double) value;
			break;
		case UseShrinkingHeuristics:
			parameters.shrinking = (int) value;
			break;
		case TrainForProbabilityEstimates:
			parameters.probability = (int) value;
			break;
		default:
			throw new IllegalStateException("Unknown technical hyper-parameter: " + name());
		}
	}

	public static List<HyperParameter> getDefaults() {
		return Arrays.asList(new NominalHyperParameter<>(CacheSizeMB.name(), singletonList(100)),
				new NominalHyperParameter<>(EpsilonTerminationCriterion.name(), singletonList(0.001d)),
				new NominalHyperParameter<>(UseShrinkingHeuristics.name(), singletonList(1)),
				new NominalHyperParameter<>(TrainForProbabilityEstimates.name(), singletonList(0)));
	}

}
