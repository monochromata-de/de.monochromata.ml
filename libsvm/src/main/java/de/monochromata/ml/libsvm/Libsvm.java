package de.monochromata.ml.libsvm;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.monochromata.ml.libsvm.config.KernelTypeHyperParameter;
import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.libsvm.config.SvmTypeHyperParameter;
import de.monochromata.ml.libsvm.config.TechnicalHyperParameter;
import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.MLSpi;
import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.training.TrainingDataCollection;
import de.monochromata.ml.lowlevel.training.validation.Validation;
import de.monochromata.ml.modelselection.HyperParameterValue;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

/**
 * An implementation of the machine-learning service-provider interface based on
 * LIBSVM.
 *
 * @param <T>
 *            the type of objects to be corrected
 */
public class Libsvm<T> implements MLSpi<ModelType, svm_parameter, svm_node[], Double, List<svm_model>, T> {

	private final Function<T, CorrectionDatum<svm_node[], Double>> reader;
	private final BiFunction<CorrectionDatum<svm_node[], Double>, T, T> writer;

	public Libsvm(Function<T, CorrectionDatum<svm_node[], Double>> reader,
			BiFunction<CorrectionDatum<svm_node[], Double>, T, T> writer) {
		this.reader = reader;
		this.writer = writer;
	}

	@Override
	public TrainingDataCollection<svm_parameter, svm_node[], Double, List<svm_model>, T> trainingData(
			List<Feature> inputFeatures, List<Feature> outputFeatures) {
		ensureListIsNonNullAndNonEmpty("inputFeatures", inputFeatures);
		ensureListIsNonNullAndNonEmpty("outputFeatures", outputFeatures);
		return new LibsvmTrainingDataCollection<>(this, inputFeatures, outputFeatures);
	}

	private void ensureListIsNonNullAndNonEmpty(final String nameOfFeatureList, final List<Feature> features) {
		if (features == null) {
			throw new IllegalArgumentException(nameOfFeatureList + " cannot be null");
		}
		if (features.isEmpty()) {
			throw new IllegalArgumentException(nameOfFeatureList + " must not be empty");
		}
	}

	@Override
	public List<svm_node[]> copyInputFeatureValues(List<svm_node[]> inputFeatureValues) {
		return inputFeatureValues.stream().map(this::copy).collect(Collectors.toList());
	}

	protected svm_node[] copy(final svm_node[] oldNodes) {
		final int numberOfNodes = oldNodes.length;
		final svm_node[] newNodes = new svm_node[numberOfNodes];
		for (int i = 0; i < numberOfNodes; i++) {
			newNodes[i] = copy(oldNodes[i]);
		}
		return newNodes;
	}

	protected svm_node copy(final svm_node oldNode) {
		final svm_node newNode = new svm_node();
		newNode.index = oldNode.index;
		newNode.value = oldNode.value;
		return newNode;
	}

	@Override
	public List<List<Double>> copyOutputFeatureValues(List<List<Double>> outputFeatureValues) {
		return outputFeatureValues.stream().map(listOfValues -> copyDoubles(listOfValues)).collect(toList());
	}

	protected List<Double> copyDoubles(final List<Double> oldList) {
		return oldList.stream().map(value -> new Double(value.doubleValue())).collect(Collectors.toList());
	}

	@Override
	public List<Double> substractOutputFeatureValues(List<Double> minuend, List<Double> subtrahend) {
		requireSameSize(minuend, subtrahend);
		final List<Double> differences = new ArrayList<Double>(minuend.size());
		final int numberOfDimensions = minuend.size();
		for (int i = 0; i < numberOfDimensions; i++) {
			differences.add(minuend.get(i) - subtrahend.get(i));
		}
		return differences;
	}

	protected void requireSameSize(final List<Double> list1, final List<Double> list2) {
		if (list1.size() != list2.size()) {
			throw new IllegalArgumentException(
					"The two lists do not have the same size: " + list1.size() + " != " + list2.size());
		}
	}

	@Override
	public CorrectionDatum<svm_node[], Double> read(final T inputToBeCorrected) {
		return reader.apply(inputToBeCorrected);
	}

	@Override
	public T write(final CorrectionDatum<svm_node[], Double> correctedDatum, final T inputToBeCorrected) {
		return writer.apply(correctedDatum, inputToBeCorrected);
	}

	@Override
	public CorrectionDatum<svm_node[], Double> predictingCorrector(final CorrectionDatum<svm_node[], Double> input,
			final List<svm_model> models) {
		final List<Double> predictedOutputs = models.stream()
				.map(model -> svm.svm_predict(model, input.getInputFeatureValues())).collect(Collectors.toList());
		return LibsvmCorrectionDatum.of(input.getInputFeatureValues(), predictedOutputs);
	}

	@Override
	public Validation<svm_node[], Double, T> createValidation() {
		return new LibsvmValidation<>();
	}

	@Override
	public svm_parameter createConfiguration(ModelType modelType, List<HyperParameterValue<?>> hyperParameterValues) {
		final svm_parameter parameters = new svm_parameter();
		modelType.getSvmType().setIn(parameters);
		modelType.getKernelType().setIn(parameters);
		setHyperParameters(hyperParameterValues, parameters);
		return parameters;
	}

	protected void setHyperParameters(List<HyperParameterValue<?>> hyperParameterValues,
			final svm_parameter parameters) {
		for (final HyperParameterValue<?> parameterValue : hyperParameterValues) {
			setHyperParameter(parameters, parameterValue);
		}
	}

	protected void setHyperParameter(final svm_parameter parameters, final HyperParameterValue<?> parameterValue) {
		final String nameOfHyperParameter = parameterValue.getHyperParameter().getName();
		final List<KernelTypeHyperParameter> kernelTypeHyperParameters = stream(KernelTypeHyperParameter.values())
				.filter(hyperParameter -> hyperParameter.name().equals(nameOfHyperParameter)).collect(toList());
		if (!kernelTypeHyperParameters.isEmpty()) {
			kernelTypeHyperParameters.get(0).setIn(parameters, parameterValue.getValue());
		} else {
			setSvmOrTechnicalHyperParameterOrThrowException(parameters, parameterValue, nameOfHyperParameter);
		}
	}

	private void setSvmOrTechnicalHyperParameterOrThrowException(final svm_parameter parameters,
			final HyperParameterValue<?> parameterValue, final String nameOfHyperParameter) {
		final List<SvmTypeHyperParameter> svmTypeHyperParameters = stream(SvmTypeHyperParameter.values())
				.filter(hyperParameter -> hyperParameter.name().equals(nameOfHyperParameter)).collect(toList());
		if (!svmTypeHyperParameters.isEmpty()) {
			svmTypeHyperParameters.get(0).setIn(parameters, parameterValue.getValue());
		} else {
			setTechnicalHyperParameterOrThrowException(parameters, parameterValue, nameOfHyperParameter);
		}
	}

	private void setTechnicalHyperParameterOrThrowException(final svm_parameter parameters,
			final HyperParameterValue<?> parameterValue, final String nameOfHyperParameter) {
		final List<TechnicalHyperParameter> technicalHyperParameters = stream(TechnicalHyperParameter.values())
				.filter(hyperParameter -> hyperParameter.name().equals(nameOfHyperParameter)).collect(toList());
		if (!technicalHyperParameters.isEmpty()) {
			technicalHyperParameters.get(0).setIn(parameters, parameterValue.getValue());
		} else {
			throw new IllegalArgumentException("Unknown hyper-parameter: " + nameOfHyperParameter);
		}
	}

}
