package de.monochromata.ml.libsvm;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.MLConfigurationException;
import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.Training;
import de.monochromata.ml.lowlevel.training.TrainingData;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

/**
 * A LIBSVM training model trains an ML.
 * 
 * @param <T>
 *            the type of objects to be used for training and to be corrected
 */
public class LibsvmTraining<T> implements Training<svm_parameter, svm_node[], Double, List<svm_model>, T> {

	private final TrainingData<svm_node[], Double> trainingData;

	/**
	 * Creates a new training model without feature values.
	 * 
	 * @param inputFeatures
	 *            the input features
	 * @param outputFeatures
	 *            the output features
	 */
	public LibsvmTraining(final Libsvm spi, final List<Feature> inputFeatures, final List<Feature> outputFeatures) {
		this(new LibsvmTrainingData(spi, inputFeatures, outputFeatures));
	}

	/**
	 * Create a new training model from the given model data.
	 * 
	 * @param trainingData
	 *            the model data to use for training
	 */
	public LibsvmTraining(final TrainingData<svm_node[], Double> trainingData) {
		this.trainingData = trainingData;
	}

	@Override
	public CompletionStage<CorrectionModel<svm_node[], Double, T>> train(final svm_parameter hyperParameters,
			final Function<T, CorrectionDatum<svm_node[], Double>> reader,
			final BiFunction<CorrectionDatum<svm_node[], Double>, List<svm_model>, CorrectionDatum<svm_node[], Double>> corrector,
			final BiFunction<CorrectionDatum<svm_node[], Double>, T, T> writer, final Executor executor) {
		final List<svm_problem> problems = createProblems();
		// TODO: scale the input features
		checkHyperParameters(problems, hyperParameters);
		return trainModelsInExecutor(hyperParameters, reader, corrector, writer, executor, problems);
	}

	protected List<svm_problem> createProblems() {
		final svm_node[][] inputData = trainingData.getInputFeatureValues().toArray(new svm_node[0][]);
		return trainingData.getOutputFeatureValues().stream().map(values -> {
			final svm_problem problem = new svm_problem();
			problem.x = inputData;
			problem.l = inputData.length;
			problem.y = toPrimitiveArray(values);
			return problem;
		}).collect(toList());
	}

	protected void checkHyperParameters(final List<svm_problem> problems, final svm_parameter hyperParameters) {
		IntStream.range(0, problems.size()).forEach(i -> {
			final Feature outputFeature = trainingData.getOutputFeatures().get(i);
			final svm_problem problem = problems.get(i);
			checkHyperParameters(outputFeature, problem, hyperParameters);
		});
	}

	protected void checkHyperParameters(final Feature outputFeature, final svm_problem problem,
			final svm_parameter hyperParameters) {
		final String error = svm.svm_check_parameter(problem, hyperParameters);
		if (error != null) {
			throw new MLConfigurationException("Invalid hyper-parameters for the ML for solving output feature "
					+ outputFeature.getName() + ": " + error);
		}
	}

	private CompletionStage<CorrectionModel<svm_node[], Double, T>> trainModelsInExecutor(
			final svm_parameter hyperParameters, final Function<T, CorrectionDatum<svm_node[], Double>> reader,
			final BiFunction<CorrectionDatum<svm_node[], Double>, List<svm_model>, CorrectionDatum<svm_node[], Double>> corrector,
			final BiFunction<CorrectionDatum<svm_node[], Double>, T, T> writer, final Executor executor,
			final List<svm_problem> problems) {
		final CompletableFuture<CorrectionModel<svm_node[], Double, T>> result = new CompletableFuture<>();
		executor.execute(() -> {
			try {
				final List<svm_model> models = trainModels(problems, hyperParameters);
				result.complete(new LibsvmCorrectionModel<>(models, reader, corrector, writer));
			} catch (final Throwable t) {
				result.completeExceptionally(t);
			}
		});
		return result;
	}

	protected List<svm_model> trainModels(final List<svm_problem> problems, final svm_parameter hyperParameters) {
		return problems.stream().map(problem -> svm.svm_train(problem, hyperParameters)).collect(Collectors.toList());
	}

	protected double[] toPrimitiveArray(final List<Double> doubles) {
		return doubles.stream().mapToDouble(value -> value).toArray();
	}

}
