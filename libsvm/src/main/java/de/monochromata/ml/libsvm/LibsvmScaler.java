package de.monochromata.ml.libsvm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Scaler;
import libsvm.svm_node;

/**
 * A scaler that scales both input and output to the range.
 */
public class LibsvmScaler implements Scaler<svm_node[], Double>, Serializable {

	private static final long serialVersionUID = -3678899276920634197L;
	private final double min;
	private final double max;

	private boolean parametersHaveBeenObtainedFromTrainingData = false;
	private final List<Parameters> inputFeatureParameters = new ArrayList<>();
	private final List<Parameters> outputFeatureParameters = new ArrayList<>();

	private LibsvmScaler(final double min, final double max) {
		this.min = min;
		this.max = max;
	}

	public static LibsvmScaler fromMemento(final Serializable memento) {
		return (LibsvmScaler) memento;
	}

	public Serializable getMemento() {
		return this;
	}

	public static LibsvmScaler from0to1() {
		return new LibsvmScaler(0, 1);
	}

	public static LibsvmScaler fromMinus1ToPlus1() {
		return new LibsvmScaler(-1, 1);
	}

	public static LibsvmScaler fromMintoMax(final double min, final double max) {
		return new LibsvmScaler(min, max);
	}

	@Override
	public TrainingData<svm_node[], Double> apply(final TrainingData<svm_node[], Double> trainingData) {
		ensureThatThisScalerHasNotBeenTrainedYet();
		scaleInputFeatures(trainingData);
		scaleOutputFeatures(trainingData);
		parametersHaveBeenObtainedFromTrainingData = true;
		return trainingData;
	}

	private void ensureThatThisScalerHasNotBeenTrainedYet() {
		if (parametersHaveBeenObtainedFromTrainingData) {
			throw new IllegalStateException("LibsvmScaler must not be re-trained. Use a new instance.");
		}
	}

	private void scaleInputFeatures(final TrainingData<svm_node[], Double> trainingData) {
		// TODO: In chunks/iterativ verarbeiten?
		final List<List<svm_node>> inputFeatureValuesByFeature = getInputFeatureValuesByFeature(trainingData);
		inputFeatureValuesByFeature.stream().forEach(values -> {
			final DoubleSummaryStatistics summary = values.stream().mapToDouble(node -> node.value).summaryStatistics();
			final Parameters parameters = new Parameters(summary, min, max);
			inputFeatureParameters.add(parameters);
			values.stream().forEach(node -> {
				node.value = scaleMinMax(parameters, node.value);
			});
		});
	}

	protected static List<List<svm_node>> getInputFeatureValuesByFeature(
			final TrainingData<svm_node[], Double> trainingData) {
		final List<List<svm_node>> inputFeatureValuesByFeature = createListOfLists(
				trainingData.getInputFeatures().size());
		trainingData.getInputFeatureValues().stream().forEach(nodes -> {
			for (int i = 0; i < nodes.length; i++) {
				inputFeatureValuesByFeature.get(i).add(nodes[i]);
			}
		});
		return inputFeatureValuesByFeature;
	}

	protected static List<List<svm_node>> createListOfLists(final int numberOfLists) {
		final List<List<svm_node>> listOfLists = new ArrayList<>(numberOfLists);
		for (int i = 0; i < numberOfLists; i++) {
			listOfLists.add(new LinkedList<>());
		}
		return listOfLists;
	}

	private void scaleOutputFeatures(final TrainingData<svm_node[], Double> trainingData) {
		trainingData.getOutputFeatureValues().stream().forEach(values -> {
			final DoubleSummaryStatistics summary = values.stream().mapToDouble(value -> value).summaryStatistics();
			final Parameters parameters = new Parameters(summary, min, max);
			outputFeatureParameters.add(parameters);
			final List<Double> newValues = values.stream().map(value -> scaleMinMax(parameters, value))
					.collect(Collectors.toList());
			values.clear();
			values.addAll(newValues);
		});
	}

	private Double scaleMinMax(final Parameters parameters, final double value) {
		/*
		 * final double std = (value - summary.getMin()) / (summary.getMax() -
		 * summary.getMin()); final double scaled = (std * (max - min)) + min;
		 * return scaled;
		 */

		// TODO: Add a test to ensure that the data range is not 0, otherwise
		// there will be a division by zero

		return (value * parameters.scale) + parameters.min;
	}

	private Double invertScaleMinMax(final Parameters parameters, final double scaledValue) {
		return (scaledValue - parameters.min) / parameters.scale;
	}

	public double[] scaleInputFeatureValues(final double[] values) {
		ensureThatScalingParametersAreAvailable();
		ensureNumberOfValuesMatchesInputFeatureParameters(values);
		return doScaleInputFeatureValues(values);
	}

	private void ensureThatScalingParametersAreAvailable() {
		// TODO: Add a test for this exception - in all calling methods
		if (!parametersHaveBeenObtainedFromTrainingData) {
			throw new IllegalStateException(
					"Cannot scale additional data before the scaling parameters have been obtained from the training data");
		}
	}

	private void ensureNumberOfValuesMatchesInputFeatureParameters(final double[] values) {
		// TODO: Add a test for this exception
		if (values.length != inputFeatureParameters.size()) {
			throw new IllegalArgumentException("values.length != inputFeatureParameters.size() (" + values.length
					+ " != " + inputFeatureParameters.size() + ")");
		}
	}

	private double[] doScaleInputFeatureValues(final double[] values) {
		final double[] scaledValues = new double[values.length];
		for (int i = 0; i < values.length; i++) {
			scaledValues[i] = scaleMinMax(inputFeatureParameters.get(i), values[i]);
		}
		return scaledValues;
	}

	public double[] invertScalingOfInputFeatureValues(final double[] scaledValues) {
		return invertScalingOfFeatureValues(inputFeatureParameters, scaledValues);
	}

	public double[] invertScalingOfOutputFeatureValues(final double[] scaledValues) {
		return invertScalingOfFeatureValues(outputFeatureParameters, scaledValues);
	}

	private double[] invertScalingOfFeatureValues(final List<Parameters> featureParameters,
			final double[] scaledValues) {
		ensureThatScalingParametersAreAvailable();
		ensureThatNumberOfScaledValuesMatchesFeatureParameters(featureParameters, scaledValues);
		return doInvertScalingOfFeatureValues(featureParameters, scaledValues);
	}

	private void ensureThatNumberOfScaledValuesMatchesFeatureParameters(final List<Parameters> featureParameters,
			final double[] scaledValues) {
		// TODO: Add a test for this exception
		if (scaledValues.length != featureParameters.size()) {
			throw new IllegalArgumentException("scaledValues.length != featureParameters.size() (" + scaledValues.length
					+ " != " + featureParameters.size() + ")");
		}
	}

	private double[] doInvertScalingOfFeatureValues(final List<Parameters> featureParameters,
			final double[] scaledValues) {
		final double[] values = new double[scaledValues.length];
		for (int i = 0; i < scaledValues.length; i++) {
			values[i] = invertScaleMinMax(featureParameters.get(i), scaledValues[i]);
		}
		return values;
	}

	private static class Parameters implements Serializable {

		private static final long serialVersionUID = 7502652807525035118L;
		private final double scale;
		private final double min;

		private Parameters(final DoubleSummaryStatistics summary, final double min, final double max) {
			throwExceptionIfRangeIsEmptyAndDataMinEqualsMin(min, summary.getMin(), summary.getMax());
			final double dataMin = correctIfRangeIsEmpty(summary, summary.getMin(), min);
			final double dataMax = summary.getMax();
			final double dataRange = correctIfRangeIsEmpty(summary, dataMax - dataMin, dataMax - min);
			this.scale = ((max - min) / dataRange);
			this.min = min - dataMin * scale;
		}

		private double correctIfRangeIsEmpty(final DoubleSummaryStatistics summary, final double valueIfRangeIsNonEmpty,
				final double valueIfRangeIsEmpty) {
			if (dataRangeIsEmpty(summary.getMin(), summary.getMax())) {
				return valueIfRangeIsEmpty;
			}
			return valueIfRangeIsNonEmpty;
		}

		private boolean dataRangeIsEmpty(final double dataMin, final double dataMax) {
			return dataMin == dataMax;
		}

		private void throwExceptionIfRangeIsEmptyAndDataMinEqualsMin(final double min, double dataMin,
				final double dataMax) {
			if (dataRangeIsEmpty(dataMin, dataMax) && dataMin == min) {
				throw new IllegalArgumentException("TrainingData range is empty and identical to target minimum: min="
						+ dataMin + " max=" + dataMax);
			}
		}

	}
}
