package de.monochromata.ml.libsvm.config;

public class ModelType {

	private final SvmType svmType;
	private final KernelType kernelType;

	public ModelType(final SvmType svmType, final KernelType kernelType) {
		this.svmType = svmType;
		this.kernelType = kernelType;
	}

	public SvmType getSvmType() {
		return svmType;
	}

	public KernelType getKernelType() {
		return kernelType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kernelType == null) ? 0 : kernelType.hashCode());
		result = prime * result + ((svmType == null) ? 0 : svmType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ModelType other = (ModelType) obj;
		if (kernelType != other.kernelType)
			return false;
		if (svmType != other.svmType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ModelType [svmType=" + svmType + ", kernelType=" + kernelType + "]";
	}

}