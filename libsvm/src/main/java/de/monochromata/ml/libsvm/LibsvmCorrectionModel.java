package de.monochromata.ml.libsvm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Function;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import libsvm.svm_model;
import libsvm.svm_node;

/**
 * A Libsvm correction model wraps a trained ML that may be used to perform
 * cross-validation or to correct real data.
 * 
 * @param <T>
 *            the type of objects to be corrected
 */
public class LibsvmCorrectionModel<T> implements CorrectionModel<svm_node[], Double, T> {

	private final ArrayList<svm_model> models;
	private final Function<T, CorrectionDatum<svm_node[], Double>> reader;
	private final BiFunction<CorrectionDatum<svm_node[], Double>, List<svm_model>, CorrectionDatum<svm_node[], Double>> corrector;
	private final BiFunction<CorrectionDatum<svm_node[], Double>, T, T> writer;

	/**
	 * Create a new instance
	 * 
	 * @param models
	 *            the trained SVMs to be supplied to the corrector
	 * @param reader
	 *            transforms the problem-specific data structure into a LIBSVM
	 *            data-structure before correction
	 * @param corrector
	 *            a function that takes the trained SVMs and a to-be-corrected
	 *            instance and returns a new corrected instance
	 * @param writer
	 *            writes the corrected data the LIBSVM data-structure into the
	 *            existing problem-specific data structure after correction. The
	 *            writer may return the existing (mutable) instance of the
	 *            problem-specific data structure, or return a new (immutable)
	 *            instance.
	 */
	public LibsvmCorrectionModel(final List<svm_model> models,
			final Function<T, CorrectionDatum<svm_node[], Double>> reader,
			final BiFunction<CorrectionDatum<svm_node[], Double>, List<svm_model>, CorrectionDatum<svm_node[], Double>> corrector,
			final BiFunction<CorrectionDatum<svm_node[], Double>, T, T> writer) {
		this.models = new ArrayList<>(models);
		this.reader = reader;
		this.corrector = corrector;
		this.writer = writer;
	}

	/**
	 * Create a new instance from a memento
	 * 
	 * @param memento
	 *            the persisted state of a correction model
	 * @param reader
	 *            transforms the problem-specific data structure into a LIBSVM
	 *            data-structure before correction
	 * @param corrector
	 *            a function that takes the trained SVMs and a to-be-corrected
	 *            instance and returns a new corrected instance
	 * @param writer
	 *            writes the corrected data the LIBSVM data-structure into the
	 *            existing problem-specific data structure after correction. The
	 *            writer may return the existing (mutable) instance of the
	 *            problem-specific data structure, or return a new (immutable)
	 *            instance.
	 */
	@SuppressWarnings("unchecked")
	public static <T> LibsvmCorrectionModel<T> of(final Serializable memento,
			final Function<T, CorrectionDatum<svm_node[], Double>> reader,
			final BiFunction<CorrectionDatum<svm_node[], Double>, List<svm_model>, CorrectionDatum<svm_node[], Double>> corrector,
			final BiFunction<CorrectionDatum<svm_node[], Double>, T, T> writer) {
		return new LibsvmCorrectionModel<>((ArrayList<svm_model>) memento, reader, corrector, writer);
	}

	@Override
	public Serializable getMemento() {
		return models;
	}

	@Override
	public T apply(final T input, final List<Transformer<svm_node[], Double>> transformers) {
		final CorrectionDatum<svm_node[], Double> datum = reader.apply(input);
		return writer.apply(applyToDatum(datum, transformers), input);
	}

	@Override
	public CorrectionDatum<svm_node[], Double> applyToDatum(CorrectionDatum<svm_node[], Double> correctionDatum,
			List<Transformer<svm_node[], Double>> transformers) {
		final AtomicReference<CorrectionDatum<svm_node[], Double>> atomicDatum = new AtomicReference<>(correctionDatum);
		transformInput(atomicDatum, transformers);
		correctOutput(atomicDatum);
		invertOutputPreprocessing(atomicDatum, transformers);
		return atomicDatum.get();
	}

	private void transformInput(final AtomicReference<CorrectionDatum<svm_node[], Double>> datum,
			final List<Transformer<svm_node[], Double>> transformers) {
		transformers.forEach(transformer -> {
			datum.set(transformer.transformInput(datum.get()));
		});
	}

	private void correctOutput(final AtomicReference<CorrectionDatum<svm_node[], Double>> datum) {
		datum.set(corrector.apply(datum.get(), models));
	}

	private void invertOutputPreprocessing(final AtomicReference<CorrectionDatum<svm_node[], Double>> datum,
			final List<Transformer<svm_node[], Double>> transformers) {
		transformers.forEach(transformer -> {
			datum.set(transformer.invertPreprocessing(datum.get()));
		});
	}

}
