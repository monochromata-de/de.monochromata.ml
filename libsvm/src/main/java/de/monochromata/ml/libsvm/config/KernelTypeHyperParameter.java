package de.monochromata.ml.libsvm.config;

import libsvm.svm_parameter;

public enum KernelTypeHyperParameter {

	/**
	 * The degree of a polynomial kernel function ({@code int}, default 3).
	 */
	Degree,

	/**
	 * The gamma parameter in a polynomial, RBF or sigmoid kernel function (
	 * {@code double}, default 1/numberOfInputFeatures).
	 */
	Gamma,

	/**
	 * The coefficient in a polynomial or sigmoid kernel function (
	 * {@code double}, default 0).
	 */
	Coefficient;

	public void setIn(final svm_parameter parameters, final Object value) {
		switch (this) {
		case Degree:
			parameters.degree = (int) (double) value;
			break;
		case Gamma:
			parameters.gamma = (double) value;
			break;
		case Coefficient:
			parameters.coef0 = (double) value;
			break;
		default:
			throw new IllegalStateException("Unknown kernel-type hyper-parameter: " + name());
		}
	}
}