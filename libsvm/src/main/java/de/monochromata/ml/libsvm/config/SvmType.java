package de.monochromata.ml.libsvm.config;

import static de.monochromata.ml.libsvm.config.SvmTypeHyperParameter.Cost;
import static de.monochromata.ml.libsvm.config.SvmTypeHyperParameter.Epsilon;
import static de.monochromata.ml.libsvm.config.SvmTypeHyperParameter.Nu;

import java.util.ArrayList;
import java.util.List;

import de.monochromata.ml.modelselection.HyperParameter;
import de.monochromata.ml.modelselection.RatioHyperParameter;
import libsvm.svm_parameter;

public enum SvmType {
	/** for classification */
	C_SVC,
	/** for classification */
	NuSVC,
	/**
	 * to decide whether a datum belongs to a singular known class, or not
	 */
	OneClassSVC,
	/** for regression */
	EpsilonSVR,
	/** for regression */
	NuSVR;

	public <T> void setIn(final svm_parameter parameters) {
		switch (this) {
		case C_SVC:
			parameters.svm_type = svm_parameter.C_SVC;
			break;
		case NuSVC:
			parameters.svm_type = svm_parameter.NU_SVC;
			break;
		case OneClassSVC:
			parameters.svm_type = svm_parameter.ONE_CLASS;
			break;
		case EpsilonSVR:
			parameters.svm_type = svm_parameter.EPSILON_SVR;
			break;
		case NuSVR:
			parameters.svm_type = svm_parameter.NU_SVR;
			break;
		default:
			throw new IllegalStateException("Unknown kernel-type: " + name());
		}
	}

	// TODO: Add factory methods for remaining SVM types

	/**
	 * @see SvmTypeHyperParameter#Cost
	 * @see SvmTypeHyperParameter#Epsilon
	 */
	public static List<HyperParameter> hyperParametersForEpsilonSVR(final double costMin, final double costMax,
			final double epsilonMin, final double epsilonMax) {
		final ArrayList<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.add(new RatioHyperParameter(Cost.name(), costMin, costMax));
		hyperParameters.add(new RatioHyperParameter(Epsilon.name(), epsilonMin, epsilonMax));
		return hyperParameters;
	}

	/**
	 * @see SvmTypeHyperParameter#Cost
	 * @see SvmTypeHyperParameter#Nu
	 */
	public static List<HyperParameter> hyperParametersForNuSVR(final double costMin, final double costMax,
			final double nuMin, final double nuMax) {
		final ArrayList<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.add(new RatioHyperParameter(Cost.name(), costMin, costMax));
		hyperParameters.add(new RatioHyperParameter(Nu.name(), nuMin, nuMax));
		return hyperParameters;
	}
}