package de.monochromata.ml.libsvm;

import java.util.List;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import libsvm.svm_node;

public interface LibsvmCorrectionDatum {

	public static CorrectionDatum<svm_node[], Double> of(final svm_node[] inputFeatureValues,
			final List<Double> outputFeatureValues) {
		return CorrectionDatum.of(inputFeatureValues, outputFeatureValues);
	}

}