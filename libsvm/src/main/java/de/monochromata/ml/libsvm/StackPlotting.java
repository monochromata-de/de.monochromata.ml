package de.monochromata.ml.libsvm;

import static java.lang.String.format;

import java.awt.Color;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;

import org.math.plot.PlotPanel;

import de.monochromata.ml.crossvalidation.NFoldCrossValidation;
import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.stack.StackElement;
import de.monochromata.plot.MultiColumnPlotFrame;
import de.monochromata.plot.ScatterPlot3D;
import de.monochromata.plot.MultiColumnPlotFrame.Column;
import de.monochromata.plot.MultiColumnPlotFrame.LabeledPlotPanel;
import libsvm.svm_model;
import libsvm.svm_parameter;

/**
 * A trait for plotting the results of a stack.
 * 
 * @param <T>
 *            the type of objects to be corrected
 */
public interface StackPlotting<T> {

	/**
	 * Plots a stack element from the given stack. Each output dimension has one
	 * column containing all folds of that dimension.
	 * 
	 * @param stack
	 *            the stack to plot
	 * @param stackElementIndex
	 *            index of the stack element to plot
	 */
	default void plotData(
			final List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T>> stack,
			final int stackElementIndex) {
		final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement = stack
				.get(stackElementIndex);
		final List<Feature> outputFeatures = stackElement.getConfiguration().getMlConfiguration().getOutputFeatures();
		final int numberOfFolds = ((NFoldCrossValidation<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T>) stackElement
				.getConfiguration().getCrossValidation()).getNumberOfFolds();

		final List<Column> columns = createColumns(stackElement, outputFeatures, numberOfFolds);
		new MultiColumnPlotFrame(columns).showPlotAndWaitUntilJFrameIsClosed("Stack element " + stackElementIndex);
		// TODO: Maybe also interpolate the data from correction model of the
		// chosen best result - in a diagram that is not fold-wise
	}

	default List<Column> createColumns(
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final List<Feature> outputFeatures, final int numberOfFolds) {
		final ArrayList<Column> columns = new ArrayList<>();
		for (int i = 0; i < outputFeatures.size(); i++) {
			final Feature outputFeature = outputFeatures.get(i);
			columns.add(createColumn(stackElement, outputFeature, i, numberOfFolds));
		}
		return columns;
	}

	default Column createColumn(
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final Feature outputFeature, final int outputFeatureIndex, final int numberOfFolds) {
		final List<LabeledPlotPanel> plotPanels = new ArrayList<>();
		for (int i = 0; i < numberOfFolds; i++) {
			final PlotPanel plotPanel = getFoldPlot(stackElement, outputFeatureIndex, i);
			final LabeledPlotPanel labeledPlotPanel = new LabeledPlotPanel("Fold " + i, plotPanel);
			plotPanels.add(labeledPlotPanel);
		}
		final Double averageError = stackElement.getResult().getErrorValuesByDimension().get(outputFeatureIndex);
		final String title = format("Output feature %1$s (avg. fold error: %2$.3f)", outputFeature.getName(),
				averageError);
		return new Column(title, plotPanels);
	}

	default PlotPanel getFoldPlot(
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final int outputFeatureIndex, final int foldIndex) {
		return getFoldPlot(getDataForFold(stackElement, foldIndex), getTruthValuesForFold(stackElement, foldIndex),
				stackElement, outputFeatureIndex, foldIndex);
	}

	default PlotPanel getFoldPlot(final List<T> testData, final List<List<Double>> truthValues,
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final int outputFeatureIndex, final int foldIndex) {
		final ScatterPlot3D scatterPlot = new ScatterPlot3D();
		addScatterPlot(scatterPlot, "Input", Color.RED, testData,
				getDataSupplier(testData, outputFeatureIndex, this::zAccessor));
		// TODO: Permit a (an interactive?) choice between addScatterPlot and
		// addScatterPlotForSurface
		addScatterPlotForSurface(scatterPlot, "Corrected", Color.ORANGE, testData, stackElement, outputFeatureIndex,
				foldIndex);
		addScatterPlot(scatterPlot, "Truth", Color.GREEN, testData, getTruthSupplier(truthValues, outputFeatureIndex));
		return scatterPlot.getPanel();
	}

	default void addScatterPlot(final ScatterPlot3D scatterPlot, final String legendEntry, final Color color,
			final List<T> testData, final Supplier<double[]> zSupplier) {
		scatterPlot.addScatterPlot(legendEntry, color, getDataSupplier(testData, this::xAccessor),
				getDataSupplier(testData, this::yAccessor), zSupplier);
	}

	default void addScatterPlot(final ScatterPlot3D scatterPlot, final String legendEntry, final Color color,
			final List<T> testData,
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final int outputFeatureIndex, final int foldIndex) {
		final Supplier<double[]> dataX = getDataSupplier(testData, this::xAccessor);
		final Supplier<double[]> dataY = getDataSupplier(testData, this::yAccessor);
		final CorrectedValues correctedValues = getCorrectedValues(dataX.get(), dataY.get(), outputFeatureIndex,
				stackElement.getResult().getCorrectionModels().get(foldIndex),
				stackElement.getResult().getTransformers().get(foldIndex));
		scatterPlot.addScatterPlot(legendEntry, color, correctedValues::getDataX, correctedValues::getDataY,
				correctedValues::getDataZ);
	}

	default void addScatterPlotForSurface(final ScatterPlot3D scatterPlot, final String legendEntry, final Color color,
			final List<T> testData,
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final int outputFeatureIndex, final int foldIndex) {
		final int numberOfPoints = 20;
		final DoubleSummaryStatistics rangeXStats = getRange(testData, this::xAccessor);
		final DoubleSummaryStatistics rangeYStats = getRange(testData, this::yAccessor);
		final double[] rangeX = getRange(testData, this::xAccessor, rangeXStats, numberOfPoints);
		final double[] rangeY = getRange(testData, this::yAccessor, rangeYStats, numberOfPoints);
		final CorrectedValues correctedValues = getCorrectedValuesFromRanges(rangeX, rangeY, outputFeatureIndex,
				stackElement.getResult().getCorrectionModels().get(foldIndex),
				stackElement.getResult().getTransformers().get(foldIndex));
		scatterPlot.addScatterPlot(legendEntry, color, correctedValues::getDataX, correctedValues::getDataY,
				correctedValues::getDataZ);
	}

	/**
	 * @deprecated The folded data should be obtained from the original folding
	 *             procedure
	 */
	@Deprecated
	default List<T> getDataForFold(
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final int foldIndex) {
		return getValuesForFold(foldIndex, () -> stackElement.getConfiguration().getData());
	}

	double xAccessor(final T input);

	double yAccessor(final T input);

	double zAccessor(final T input, final int outputFeatureIndex);

	default Supplier<double[]> getDataSupplier(final List<T> data, final ToDoubleFunction<T> axisSelection) {
		return () -> data.stream().mapToDouble(axisSelection).toArray();
	}

	default Supplier<double[]> getDataSupplier(final List<T> data, final int outputFeatureIndex,
			final BiFunction<T, Integer, Double> axisSelection) {
		return () -> data.stream().map(datum -> axisSelection.apply(datum, outputFeatureIndex)).mapToDouble(dbl -> dbl)
				.toArray();
	}

	default DoubleSummaryStatistics getRange(final List<T> data, final ToDoubleFunction<T> axisSelection) {
		return data.stream().mapToDouble(axisSelection).summaryStatistics();
	}

	default double[] getRange(final List<T> data, final ToDoubleFunction<T> axisSelection,
			final DoubleSummaryStatistics range, final int numberOfPoints) {
		final double stepSize = (range.getMax() - range.getMin()) / (numberOfPoints - 1);
		final double[] generatedData = new double[numberOfPoints];
		for (int i = 0; i < numberOfPoints; i++) {
			generatedData[i] = range.getMin() + (stepSize * i);
		}
		return generatedData;
	}

	CorrectedValues getCorrectedValues(final double[] dataX, final double[] dataY, final int outputFeatureIndex,
			final CorrectionModel<libsvm.svm_node[], Double, T> correctionModel,
			final List<Transformer<libsvm.svm_node[], Double>> transformers);

	CorrectedValues getCorrectedValuesFromRanges(final double[] dataX, final double[] dataY,
			final int outputFeatureIndex, final CorrectionModel<libsvm.svm_node[], Double, T> correctionModel,
			final List<Transformer<libsvm.svm_node[], Double>> transformers);

	/**
	 * @deprecated The folded data should be obtained from the original folding
	 *             procedure
	 */
	@Deprecated
	default List<List<Double>> getTruthValuesForFold(
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, T> stackElement,
			final int foldIndex) {
		return getValuesForFold(foldIndex, () -> stackElement.getConfiguration().getTruthValues());
	}

	/**
	 * @deprecated The folded data should be obtained from the original folding
	 *             procedure
	 */
	@Deprecated
	default <S> List<S> getValuesForFold(final int foldIndex, final Supplier<List<S>> valueProvider) {
		if (foldIndex < 0) {
			throw new IllegalArgumentException("Fold index must be >= 0");
		}
		final List<S> values = valueProvider.get();
		final int foldSize = values.size() / 2;
		if (foldSize * 2 != values.size()) {
			throw new IllegalArgumentException("The folds cannot be divided by two equally.");
		}
		final int fromIndexInclusive = foldIndex * foldSize;
		final int toIndexExclusive = (foldIndex + 1) * foldSize;
		return values.subList(fromIndexInclusive, toIndexExclusive);
	}

	default Supplier<double[]> getTruthSupplier(final List<List<Double>> truthValues, final int outputFeatureIndex) {
		return () -> truthValues.stream().mapToDouble(listOfDoubles -> listOfDoubles.get(outputFeatureIndex)).toArray();
	}

	class CorrectedValues {

		final double[] dataX;
		final double[] dataY;
		final double[] dataZ;

		public CorrectedValues(double[] dataX, double[] dataY, double[] dataZ) {
			this.dataX = dataX;
			this.dataY = dataY;
			this.dataZ = dataZ;
		}

		public double[] getDataX() {
			return dataX;
		}

		public double[] getDataY() {
			return dataY;
		}

		public double[] getDataZ() {
			return dataZ;
		}

	}

}
