package de.monochromata.ml.libsvm;

import static de.monochromata.ml.lowlevel.training.validation.ValidationUtilities.requireSameSize;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.validation.Validation;
import libsvm.svm_node;

/**
 * An interface for validating a {@link LibsvmCorrectionModel}.
 * 
 * @param <T>
 *            the type of objects to be corrected
 */
public class LibsvmValidation<T> implements Validation<svm_node[], Double, T> {

	@Override
	public List<Double> validate(final CorrectionModel<svm_node[], Double, T> model,
			final Function<T, CorrectionDatum<svm_node[], Double>> reader,
			final List<Transformer<svm_node[], Double>> transformers, final Supplier<T> testDataSupplier,
			final Supplier<List<Double>> truthSupplier,
			final BiFunction<List<Double>, List<Double>, Double> fitnessFunction) {
		final List<List<Double>> truth = new ArrayList<>();
		final List<List<Double>> estimates = new ArrayList<>();
		T nextDatum = null;
		while ((nextDatum = testDataSupplier.get()) != null) {
			final List<Double> nextTruth = truthSupplier.get();
			if (nextTruth == null) {
				throw new IllegalStateException("No truth available for test datum " + nextDatum);
			}
			// TODO: Validation should be implemented for Libsvm so Ote can be
			// left as type
			// parameter in the interface. The implementation may use
			// List<Double>, thought, to
			// be able to compute with it.
			appendMultipleDimensions(truth, nextTruth);
			final CorrectionDatum<svm_node[], Double> correctedDatum = model.applyToDatum(reader.apply(nextDatum),
					transformers);
			final List<Double> nextEstimate = correctedDatum.getOutputFeatureValues();
			appendMultipleDimensions(estimates, nextEstimate);
		}
		return computeFitnessValues(truth, estimates, fitnessFunction);
	}

	static List<Double> computeFitnessValues(final List<List<Double>> truth, final List<List<Double>> estimates,
			final BiFunction<List<Double>, List<Double>, Double> fitnessFunction) {
		requireSameSize(truth, estimates, "truth and estimates must have the same size");
		final int numberOfDimensions = truth.size();
		final List<Double> fitnessValues = new ArrayList<>(numberOfDimensions);
		for (int i = 0; i < numberOfDimensions; i++) {
			fitnessValues.add(fitnessFunction.apply(truth.get(i), estimates.get(i)));
		}
		return fitnessValues;
	}

	protected static void appendMultipleDimensions(final List<List<Double>> listOfMultipleDimensions,
			final List<Double> multiDimensionalElement) {
		final int numberOfDimensions = multiDimensionalElement.size();
		ensureNumberOfDimensionsAvailable(listOfMultipleDimensions, numberOfDimensions);
		for (int i = 0; i < numberOfDimensions; i++) {
			final Double value = multiDimensionalElement.get(i);
			listOfMultipleDimensions.get(i).add(value);
		}
	}

	protected static void ensureNumberOfDimensionsAvailable(final List<List<Double>> listOfMultipleDimensions,
			final int numberOfDimensions) {
		if (listOfMultipleDimensions.size() == 0) {
			for (int i = 0; i < numberOfDimensions; i++) {
				listOfMultipleDimensions.add(new LinkedList<Double>());
			}
		}
	}

}
