package de.monochromata.ml.libsvm;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.TrainingDataCollection;
import de.monochromata.ml.lowlevel.training.TrainingDataPopulator;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessing;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

/**
 * Training data for LIBSVM
 * 
 * @param <T>
 *            the type of objects to be corrected by the ML
 */
public class LibsvmTrainingDataCollection<T>
		implements TrainingDataCollection<svm_parameter, svm_node[], Double, List<svm_model>, T> {

	private final TrainingData<svm_node[], Double> trainingData;
	private State<T> state = new ToBePopulated();

	/**
	 * Creates a new empty training data container.
	 * 
	 * @param inputFeatures
	 *            the input features that will be used if
	 *            {@link #populate(Consumer)} is invoked.
	 * @param outputFeatures
	 *            the output features that will be used if
	 *            {@link #populate(Consumer)} is invoked.
	 * @see #LibsvmTrainingDataCollection(TrainingData)
	 * @throws NullPointerException
	 *             If any of the arguments is {@code null}, or if
	 *             {@code outputFeatureValues} contains {@code null} values
	 */
	public LibsvmTrainingDataCollection(final Libsvm spi, final List<Feature> inputFeatures,
			final List<Feature> outputFeatures) {
		this(new LibsvmTrainingData(spi, inputFeatures, outputFeatures));
	}

	/**
	 * Create a new training data container using the given features and values.
	 * 
	 * @param trainingData
	 *            an empty training data instance that will be used if
	 *            {@link #populate(Consumer)} is invoked.
	 * @see #LibsvmTrainingDataCollection(Libsvm, List, List)
	 */
	public LibsvmTrainingDataCollection(final TrainingData<svm_node[], Double> trainingData) {
		this.trainingData = trainingData;
	}

	@Override
	public Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T> populate(
			TrainingData<svm_node[], Double> trainingData) {
		return state.populate(trainingData);
	}

	@Override
	public CompletionStage<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T>> populate(
			final Consumer<TrainingDataPopulator> populatorUser) {
		return state.populate(populatorUser);
	}

	private interface State<T> extends TrainingDataCollection<svm_parameter, svm_node[], Double, List<svm_model>, T> {
	}

	private class ToBePopulated implements State<T> {

		@Override
		public Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T> populate(
				TrainingData<svm_node[], Double> trainingData) {
			return new LibsvmPreprocessing<T>(trainingData);
		}

		@Override
		public CompletionStage<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T>> populate(
				final Consumer<TrainingDataPopulator> populatorUser) {
			state = new Populating();
			final CompletableFuture<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T>> future = new CompletableFuture<>();
			final LibsvmTrainingDataPopulator populator = new LibsvmTrainingDataPopulator(trainingData, p -> {
				future.complete(new LibsvmPreprocessing<T>(p.getTrainingData()));
			});
			populatorUser.accept(populator);
			return future;
		}
	}

	private class Populating implements State<T> {

		@Override
		public Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T> populate(
				TrainingData<svm_node[], Double> trainingData) {
			throw new IllegalStateException("TrainingDataCollection cannot be populated twice");
		}

		@Override
		public CompletionStage<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, T>> populate(
				final Consumer<TrainingDataPopulator> populatorUser) {
			throw new IllegalStateException("TrainingDataCollection cannot be populated twice");
		}

	}
}
