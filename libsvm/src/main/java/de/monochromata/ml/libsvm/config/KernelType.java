package de.monochromata.ml.libsvm.config;

import static de.monochromata.ml.libsvm.config.KernelTypeHyperParameter.Coefficient;
import static de.monochromata.ml.libsvm.config.KernelTypeHyperParameter.Degree;
import static de.monochromata.ml.libsvm.config.KernelTypeHyperParameter.Gamma;

import java.util.ArrayList;
import java.util.List;

import de.monochromata.ml.modelselection.HyperParameter;
import de.monochromata.ml.modelselection.RatioHyperParameter;
import libsvm.svm_parameter;

public enum KernelType {
	/** {@code u'*v} */
	Linear,
	/** {@code (gamma*u'*v + coef0)^degree} */
	Polynomial,
	/** {@code exp(-gamma*|u-v|^2)} */
	RBF,
	/** {@code tanh(gamma*u'*v + coef0)} */
	Sigmoid;

	public <T> void setIn(final svm_parameter parameters) {
		switch (this) {
		case Linear:
			parameters.kernel_type = svm_parameter.LINEAR;
			break;
		case Polynomial:
			parameters.kernel_type = svm_parameter.POLY;
			break;
		case RBF:
			parameters.kernel_type = svm_parameter.RBF;
			break;
		case Sigmoid:
			parameters.kernel_type = svm_parameter.SIGMOID;
			break;
		default:
			throw new IllegalStateException("Unknown kernel-type: " + name());
		}
	}

	/**
	 * Returns an empty list of hyper-parameters for linear kernels, which do
	 * not have hyper-parameters.
	 */
	public static List<HyperParameter> hyperParametersForLinearKernel() {
		return new ArrayList<>();
	}

	/**
	 * @see KernelTypeHyperParameter#Degree
	 * @see KernelTypeHyperParameter#Gamma
	 * @see KernelTypeHyperParameter#Coefficient
	 */
	public static List<HyperParameter> hyperParametersForPolynomialKernel(final int degreeMin, final int degreeMax,
			final double gammaMin, final double gammaMax, final double coefficientMin, final double coefficientMax) {
		final ArrayList<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.add(new RatioHyperParameter(Degree.name(), degreeMin, degreeMax));
		hyperParameters.add(new RatioHyperParameter(Gamma.name(), gammaMin, gammaMax));
		hyperParameters.add(new RatioHyperParameter(Coefficient.name(), coefficientMin, coefficientMax));
		return hyperParameters;
	}

	/**
	 * @see KernelTypeHyperParameter#Gamma
	 */
	public static List<HyperParameter> hyperParametersForRBFKernel(final double gammaMin, final double gammaMax) {
		final ArrayList<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.add(new RatioHyperParameter(Gamma.name(), gammaMin, gammaMax));
		return hyperParameters;
	}

	/**
	 * @see KernelTypeHyperParameter#Gamma
	 * @see KernelTypeHyperParameter#Coefficient
	 */
	public static List<HyperParameter> hyperParametersForSigmoidKernel(final double gammaMin, final double gammaMax,
			final double coefficientMin, final double coefficientMax) {
		final ArrayList<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.add(new RatioHyperParameter(Gamma.name(), gammaMin, gammaMax));
		hyperParameters.add(new RatioHyperParameter(Coefficient.name(), coefficientMin, coefficientMax));
		return hyperParameters;
	}
}