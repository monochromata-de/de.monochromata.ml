package de.monochromata.ml.libsvm;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.Test;

import de.monochromata.ml.crossvalidation.CrossValidation.Result;
import de.monochromata.ml.crossvalidation.NFoldCrossValidation;
import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.lowlevel.DefaultMLConfiguration;
import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.ML;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;
import de.monochromata.ml.lowlevel.training.validation.ErrorFunction;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

public class LibsvmNFoldCrossValidationTest implements LibsvmTesting {

	@Test
	public void perform2FoldCrossValidation() {
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final PrintStream ps = new PrintStream(baos);

			svm.svm_set_print_string_function(ps::print);

			final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
			final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
			final svm_parameter hyperParameters = createHyperParameters(1e-3, 1);
			final Supplier<List<Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>>>> preprocessorsSupplier = () -> singletonList(
					LibsvmScaler.fromMinus1ToPlus1());
			final Function<List<Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>>>, List<Transformer<svm_node[], Double>>> transformersSupplier = (
					preprocessors) -> singletonList(new LibsvmTransformer((LibsvmScaler) preprocessors.get(0)));

			final List<TestDatum> data = new ArrayList<>();
			data.add(new TestDatum(1, 0, 8));
			data.add(new TestDatum(2, 2, 4));
			data.add(new TestDatum(3, 4, 2));
			data.add(new TestDatum(4, 6, 1));
			data.add(new TestDatum(3, 4, 2));
			data.add(new TestDatum(2, 2, 4));
			data.add(new TestDatum(1, 0, 8));
			final List<List<Double>> truthValues = new ArrayList<>();
			truthValues.add(singletonList(8d));
			truthValues.add(singletonList(4d));
			truthValues.add(singletonList(2d));
			truthValues.add(singletonList(1d));
			truthValues.add(singletonList(2d));
			truthValues.add(singletonList(4d));
			truthValues.add(singletonList(8d));

			final Libsvm<TestDatum> spi = new Libsvm<TestDatum>(this::testDatumReader, this::testDatumWriter);
			final ML<ModelType, svm_parameter, svm_node[], Double, List<svm_model>, TestDatum> ml = ML.getInstance(spi);
			final int numberOfFolds = 2;
			final NFoldCrossValidation<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> cv = new NFoldCrossValidation<>(
					numberOfFolds);
			final DefaultMLConfiguration<libsvm.svm_node[], Double, List<svm_model>, TestDatum> configuration = new DefaultMLConfiguration<>(
					ForkJoinPool.commonPool(), inputFeatures, outputFeatures, preprocessorsSupplier,
					transformersSupplier);
			final Result<libsvm.svm_node[], Double, TestDatum> result = cv.validate(ml, configuration, hyperParameters,
					ErrorFunction::rmse, data, truthValues);
			final List<Double> errorValues = result.getAverageErrorByDimension();

			final String expectedLogOutput = "*\n" + "optimization finished, #iter = 3\n" + "nu = 0.6666666666666666\n"
					+ "obj = -0.5833333333333333, rho = -7.401486830834377E-17\n" + "nSV = 3, nBSV = 1\n" + "*\n"
					+ "optimization finished, #iter = 3\n" + "nu = 0.6666666666666666\n"
					+ "obj = -0.5833333333333333, rho = -7.401486830834377E-17\n" + "nSV = 3, nBSV = 1\n";

			assertThat(errorValues).hasSize(1);
			assertThat(errorValues).containsExactly(1.38147729656189d);
			assertThat(baos.toString()).isEqualTo(expectedLogOutput);
		} finally {
			svm.svm_set_print_string_function(null);
		}
	}

}
