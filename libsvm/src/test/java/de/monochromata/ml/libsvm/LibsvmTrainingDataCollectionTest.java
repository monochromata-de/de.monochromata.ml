package de.monochromata.ml.libsvm;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.training.TrainingDataPopulator;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessing;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

public class LibsvmTrainingDataCollectionTest {

	@Test
	public void attemptingToPopulateTheTrainingDataTwiceRaisesAnException() {
		assertThatThrownBy(() -> {
			final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
			final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
			final LibsvmTrainingDataCollection<?> trainingDataCollection = new LibsvmTrainingDataCollection<>(
					new Libsvm(null, null), inputFeatures, outputFeatures);

			trainingDataCollection.populate(populator -> {
			});
			trainingDataCollection.populate(populator -> {
			});
		}).isInstanceOf(IllegalStateException.class).hasMessage("TrainingDataCollection cannot be populated twice");
	}

	@Test
	public void thePopulateMethodReturnsACompletionStage() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final LibsvmTrainingDataCollection<Object> trainingData = new LibsvmTrainingDataCollection<>(
				new Libsvm(null, null), inputFeatures, outputFeatures);

		final CompletionStage<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, Object>> completionStage = trainingData
				.populate(populator -> {
				});

		assertThat(completionStage, notNullValue());
	}

	@Test
	public void theCompletionStageIsCompletedAfterThePopulatorIsFinished() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final LibsvmTrainingDataCollection<Object> trainingDataCollection = new LibsvmTrainingDataCollection<>(
				new Libsvm(null, null), inputFeatures, outputFeatures);
		final PopulatorConsumer populatorConsumer = new PopulatorConsumer();
		final CompletionStage<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, Object>> completionStage = trainingDataCollection
				.populate(populatorConsumer);

		assertThat(completionStage.toCompletableFuture().isDone(), equalTo(false));

		populatorConsumer.populator.finish();
		assertThat(completionStage.toCompletableFuture().isDone(), equalTo(true));
	}

	@Test
	public void thePopulatorAddsData() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final LinkedList<svm_node[]> inputFeatureValues = new LinkedList<>();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(new LinkedList<>());
		final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null), inputFeatures,
				outputFeatures, inputFeatureValues, outputFeatureValues);

		final LibsvmTrainingDataCollection<Object> trainingDataCollection = new LibsvmTrainingDataCollection<>(
				trainingData);

		final CompletionStage<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, Object>> completionStage = trainingDataCollection
				.populate(populator -> {
					populator.addDatum(new double[] { 1d }, new double[] { 2d });
					populator.addDatum(new double[] { 3d }, new double[] { 4d });
					populator.finish();
				});
		completionStage.toCompletableFuture().join();

		assertThat(inputFeatureValues.size(), equalTo(2));

		final svm_node[] datum0InputValues = inputFeatureValues.get(0);
		assertThat(datum0InputValues[0].index, equalTo(1));
		assertThat(datum0InputValues[0].value, equalTo(1d));

		final svm_node[] datum1InputValues = inputFeatureValues.get(1);
		assertThat(datum1InputValues[0].index, equalTo(1));
		assertThat(datum1InputValues[0].value, equalTo(3d));

		assertThat(outputFeatureValues.size(), equalTo(1));
		final List<Double> outputFeature0Values = outputFeatureValues.get(0);

		assertThat(outputFeature0Values.size(), equalTo(2));
		assertThat(outputFeature0Values.get(0), equalTo(2d));
		assertThat(outputFeature0Values.get(1), equalTo(4d));
	}

	@Test
	public void thePopulatorReturnsACompletionStageContainingATrainingModel() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final LibsvmTrainingDataCollection<Object> trainingDataCollection = new LibsvmTrainingDataCollection<>(
				new Libsvm(null, null), inputFeatures, outputFeatures);

		final CompletionStage<Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, Object>> completionStage = trainingDataCollection
				.populate(populator -> {
					populator.addDatum(new double[] { 1d }, new double[] { 2d });
					populator.addDatum(new double[] { 3d }, new double[] { 4d });
					populator.finish();
				});
		final Preprocessing<svm_parameter, svm_node[], Double, List<svm_model>, Object> trainingModel = completionStage
				.toCompletableFuture().join();

		assertThat(trainingModel, notNullValue());
	}

	private static class PopulatorConsumer implements Consumer<TrainingDataPopulator> {

		private TrainingDataPopulator populator;

		@Override
		public void accept(final TrainingDataPopulator populator) {
			this.populator = populator;
		}

	}
}
