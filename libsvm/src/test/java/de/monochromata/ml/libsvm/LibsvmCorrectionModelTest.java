package de.monochromata.ml.libsvm;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.Transformer;
import libsvm.svm_model;
import libsvm.svm_node;

public class LibsvmCorrectionModelTest implements LibsvmTesting {

	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void correctInvokesTheCorrector() {
		final List<svm_model> models = emptyList();
		final TestableCorrector corrector = new TestableCorrector();
		new LibsvmCorrectionModel<>(models, this::intReader, corrector, this::intWriter).apply(new Integer(1));

		assertThat(corrector.numberOfCorrections, equalTo(1));
	}

	@Test
	public void correctInvokesTheTransformer() {
		final List<svm_model> models = Collections.emptyList();
		final TestableTransformer transformer = new TestableTransformer();
		new LibsvmCorrectionModel<Integer>(models, this::intReader, (input, modelz) -> input, this::intWriter)
				.apply(new Integer(1), transformer);

		assertThat(transformer.numberTransformInvocations, equalTo(1));
		assertThat(transformer.numberOfInvertInvocations, equalTo(1));
	}

	private class TestableCorrector implements
			BiFunction<CorrectionDatum<svm_node[], Double>, List<svm_model>, CorrectionDatum<svm_node[], Double>> {

		private int numberOfCorrections = 0;

		@Override
		public CorrectionDatum<svm_node[], Double> apply(final CorrectionDatum<svm_node[], Double> input,
				final List<svm_model> models) {
			numberOfCorrections++;
			return input;
		}

	}

	private class TestableTransformer implements Transformer<svm_node[], Double> {

		private int numberTransformInvocations = 0;
		private int numberOfInvertInvocations = 0;

		@Override
		public CorrectionDatum<svm_node[], Double> transformInput(final CorrectionDatum<svm_node[], Double> datum) {
			numberTransformInvocations++;
			return datum;
		}

		@Override
		public CorrectionDatum<svm_node[], Double> invertPreprocessing(
				final CorrectionDatum<svm_node[], Double> datum) {
			numberOfInvertInvocations++;
			return datum;
		}

	}
}
