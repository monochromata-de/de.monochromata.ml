package de.monochromata.ml.libsvm;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.training.Training;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

public class LibsvmPreprocessingTest implements LibsvmTesting {

	@Test
	public void preprocessorsAreInvoked() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = new ArrayList<>();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(new ArrayList<>());
		final LibsvmTrainingData<Object, Object> trainingData = new LibsvmTrainingData<>(new Libsvm(null, null),
				inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);
		final TestablePreprocessor preprocessor = new TestablePreprocessor();

		new LibsvmPreprocessing<>(trainingData).preprocess(Collections.singletonList(preprocessor));

		assertThat(preprocessor.invocationCount, equalTo(1));
	}

	@Test
	public void preprocessMethodLeavesOriginalDataUnaltered() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = Collections.singletonList(svm_nodes(-10, 0, 10, 20, 30, 40, 50));
		final List<List<Double>> outputFeatureValues = Collections
				.singletonList(Arrays.asList(-100d, 0d, 100d, 200d, 300d, 400d, 500d));
		final LibsvmTrainingData<Object, Object> trainingData = new LibsvmTrainingData<>(new Libsvm(null, null),
				inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);

		new LibsvmPreprocessing<>(trainingData).preprocess(Collections.singletonList(new IncrementingPreprocessor()));

		final libsvm.svm_node[] scaledInputValues = trainingData.getInputFeatureValues().get(0);
		assertInputValuesWithFixedPrecision(scaledInputValues, -10d, 0d, 10d, 20d, 30d, 40d, 50d);

		assertThat(trainingData.getOutputFeatureValues().get(0),
				equalTo(Arrays.asList(-100d, 0d, 100d, 200d, 300d, 400d, 500d)));
	}

	@Test
	public void preprocessingResultsAreUsedByTrainingInstanceObtainedAfterPreprocessing() throws Exception {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = Collections.singletonList(svm_nodes(-10, 0, 10, 20, 30, 40, 50));
		final List<List<Double>> outputFeatureValues = Collections
				.singletonList(Arrays.asList(-100d, 0d, 100d, 200d, 300d, 400d, 500d));
		final LibsvmTrainingData<Object, Object> trainingData = new LibsvmTrainingData(new Libsvm(null, null),
				inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);

		final Training<svm_parameter, svm_node[], Double, List<svm_model>, Object> training = new LibsvmPreprocessing<>(
				trainingData).preprocess(Collections.singletonList(new IncrementingPreprocessor()))
						.toCompletableFuture().join();

		final Field containedTrainingData = LibsvmTraining.class.getDeclaredField("trainingData");
		containedTrainingData.setAccessible(true);
		final TrainingData<svm_node[], Object> preprocessedData = (TrainingData) containedTrainingData.get(training);

		final libsvm.svm_node[] scaledInputValues = preprocessedData.getInputFeatureValues().get(0);
		assertInputValuesWithFixedPrecision(scaledInputValues, -9d, 1d, 11d, 21d, 31d, 41d, 51d);

		assertThat(preprocessedData.getOutputFeatureValues().get(0),
				equalTo(Arrays.asList(-99d, 1d, 101d, 201d, 301d, 401d, 501d)));
	}

	// TODO: Implement outlier elimination and include a test for it

	private static class TestablePreprocessor
			implements Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>> {

		private int invocationCount = 0;

		@Override
		public TrainingData<svm_node[], Double> apply(final TrainingData<svm_node[], Double> trainingData) {
			invocationCount++;
			return trainingData;
		}

	}

	private static class IncrementingPreprocessor
			implements Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>> {

		@Override
		public TrainingData<svm_node[], Double> apply(final TrainingData<svm_node[], Double> trainingData) {
			incrementInputValues(trainingData);
			incrementOutputValues(trainingData);
			return trainingData;
		}

		private void incrementInputValues(final TrainingData<svm_node[], Double> trainingData) {
			trainingData.getInputFeatureValues().forEach(nodes -> {
				Arrays.stream(nodes).forEach(node -> node.value++);
			});
		}

		private void incrementOutputValues(final TrainingData<svm_node[], Double> trainingData) {
			trainingData.getOutputFeatureValues().forEach(values -> {
				final List<Double> newValues = values.stream().map(value -> ++value).collect(toList());
				values.clear();
				values.addAll(newValues);
			});
		}

	}

}
