package de.monochromata.ml.libsvm;

import static java.lang.Math.sqrt;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import org.junit.Test;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.validation.ErrorFunction;
import libsvm.svm_node;

public class LibsvmValidationTest implements LibsvmTesting {

	@Test
	public void mseIsCorrectlyComputedDuringValidation() {
		final List<Double> errorValues = computeErrorForSampleData(ErrorFunction::mse);
		assertThat(errorValues).isEqualTo(singletonList(5 / 2d));
	}

	@Test
	public void rmseIsCorrectlyComputedDuringValidation() {
		final List<Double> errorValues = computeErrorForSampleData(ErrorFunction::rmse);
		assertThat(errorValues).isEqualTo(singletonList(sqrt(5 / 2d)));
	}

	private List<Double> computeErrorForSampleData(BiFunction<List<Double>, List<Double>, Double> errorFunction) {
		final TestableCorrectionModel model = new TestableCorrectionModel();
		final ListSupplier<TestDatum> testDataSupplier = new ListSupplier<>(
				asList(new TestDatum(1, 2, 0), new TestDatum(2, 3, 0)));
		final ListSupplier<List<Double>> truthSupplier = new ListSupplier<>(
				asList(singletonList(2d), singletonList(3d)));
		final List<Double> errorValue = new LibsvmValidation<TestDatum>().validate(model, this::testDatumReader, null,
				testDataSupplier, truthSupplier, errorFunction);
		return errorValue;
	}

	private class TestableCorrectionModel implements CorrectionModel<svm_node[], Double, TestDatum> {

		@Override
		public Serializable getMemento() {
			return null;
		}

		@Override
		public TestDatum apply(TestDatum input, List<Transformer<svm_node[], Double>> transformers) {
			final CorrectionDatum<libsvm.svm_node[], Double> datum = LibsvmValidationTest.this.testDatumReader(input);
			return LibsvmValidationTest.this.testDatumWriter(applyToDatum(datum, transformers), input);
		}

		@Override
		public CorrectionDatum<svm_node[], Double> applyToDatum(CorrectionDatum<svm_node[], Double> correctionDatum,
				List<Transformer<svm_node[], Double>> transformers) {
			if (transformers != null) {
				throw new IllegalArgumentException("Non-null transformers are ignored");
			}
			final libsvm.svm_node[] inputFeatureValues = correctionDatum.getInputFeatureValues();
			final double outputValue = inputFeatureValues[0].value + inputFeatureValues[1].value;
			correctionDatum.getOutputFeatureValues().set(0, outputValue);
			return correctionDatum;
		}

	}

	private static class ListSupplier<T> implements Supplier<T> {

		private final Iterator<T> values;

		public ListSupplier(final List<T> values) {
			this.values = values.iterator();
		}

		@Override
		public T get() {
			if (values.hasNext()) {
				return values.next();
			}
			return null;
		}

	}
}
