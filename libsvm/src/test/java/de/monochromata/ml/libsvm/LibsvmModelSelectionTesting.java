package de.monochromata.ml.libsvm;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;
import java.util.function.Supplier;

import de.monochromata.ml.crossvalidation.NFoldCrossValidation;
import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.lowlevel.DefaultMLConfiguration;
import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.ML;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.lowlevel.training.TrainingData;
import de.monochromata.ml.lowlevel.training.preprocessing.Preprocessor;
import de.monochromata.ml.lowlevel.training.validation.ErrorFunction;
import de.monochromata.ml.modelselection.Configuration;
import de.monochromata.ml.modelselection.HyperParameter;
import de.monochromata.ml.modelselection.HyperParameterValue;
import de.monochromata.ml.modelselection.ModelSelection.Result;
import de.monochromata.ml.modelselection.NonIterativeModelSelection;
import de.monochromata.ml.modelselection.RatioHyperParameter;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_print_interface;

public interface LibsvmModelSelectionTesting extends LibsvmTesting {

	default Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> chooseHyperParameters(
			final List<TestDatum> data, final List<List<Double>> truthValues,
			final Map<ModelType, List<HyperParameter>> hyperParametersByModelType, final String expectedLogOutput) {

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final PrintStream ps = new PrintStream(baos);

		final svm_print_interface printInterface = ps::print;

		final Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> result = chooseHyperParameters(
				data, truthValues, hyperParametersByModelType, printInterface);

		assertThat(baos.toString()).isEqualTo(expectedLogOutput);

		return result;
	}

	default Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> chooseHyperParameters(
			final List<TestDatum> data, final List<List<Double>> truthValues,
			final Map<ModelType, List<HyperParameter>> hyperParametersByModelType) {
		return chooseHyperParameters(data, truthValues, hyperParametersByModelType, string -> {
		});
	}

	default Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> chooseHyperParameters(
			final List<TestDatum> data, final List<List<Double>> truthValues,
			final Map<ModelType, List<HyperParameter>> hyperParametersByModelType,
			final svm_print_interface printInterface) {
		try {
			svm.svm_set_print_string_function(printInterface);
			final Configuration<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> msConfiguration = createConfiguration(
					data, truthValues, hyperParametersByModelType);
			final NonIterativeModelSelection<ModelType, svm_parameter, svm_node[], Double, List<svm_model>, TestDatum> choice = createModelSelection();

			return choice.chooseValues(msConfiguration);
		} finally {
			svm.svm_set_print_string_function(null);
		}
	}

	default Configuration<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> createConfiguration(
			final List<TestDatum> data, final List<List<Double>> truthValues,
			final Map<ModelType, List<HyperParameter>> hyperParametersByModelType) {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final Supplier<List<Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>>>> preprocessorsSupplier = () -> singletonList(
				LibsvmScaler.fromMinus1ToPlus1());
		final Function<List<Preprocessor<svm_node[], Double, TrainingData<svm_node[], Double>>>, List<Transformer<svm_node[], Double>>> transformersSupplier = (
				preprocessors) -> singletonList(new LibsvmTransformer((LibsvmScaler) preprocessors.get(0)));

		final Libsvm<TestDatum> spi = new Libsvm<TestDatum>(this::testDatumReader, this::testDatumWriter);
		final ML<ModelType, svm_parameter, svm_node[], Double, List<svm_model>, TestDatum> ml = ML.getInstance(spi);
		final DefaultMLConfiguration<libsvm.svm_node[], Double, List<svm_model>, TestDatum> mlConfiguration = new DefaultMLConfiguration<>(
				ForkJoinPool.commonPool(), inputFeatures, outputFeatures, preprocessorsSupplier, transformersSupplier);

		final int numberOfFolds = 2;
		final NFoldCrossValidation<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> cv = new NFoldCrossValidation<>(
				numberOfFolds);

		final Configuration<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> msConfiguration = new Configuration<>(
				ml, mlConfiguration, cv, hyperParametersByModelType, ErrorFunction::rmse, TestDatum::copy, data,
				truthValues);
		return msConfiguration;
	}

	default NonIterativeModelSelection<ModelType, svm_parameter, svm_node[], Double, List<svm_model>, TestDatum> createModelSelection() {
		final Function<RatioHyperParameter, List<HyperParameterValue<? extends Object>>> gridGenerator = RatioHyperParameter
				.gridGenerator(3);
		final NonIterativeModelSelection<ModelType, svm_parameter, svm_node[], Double, List<svm_model>, TestDatum> choice = new NonIterativeModelSelection<>(
				gridGenerator);
		return choice;
	}

}
