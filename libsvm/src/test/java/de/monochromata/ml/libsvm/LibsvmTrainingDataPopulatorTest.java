package de.monochromata.ml.libsvm;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import libsvm.svm_node;

public class LibsvmTrainingDataPopulatorTest {

	@Test(expected = IllegalStateException.class)
	public void finishMustNotBeInvokedTwice() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = new LinkedList<>();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(new LinkedList<>());
		final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null), inputFeatures,
				outputFeatures, inputFeatureValues, outputFeatureValues);

		final LibsvmTrainingDataPopulator populator = new LibsvmTrainingDataPopulator(trainingData);

		populator.finish();
		populator.finish();
	}

	@Test(expected = IllegalStateException.class)
	public void addDatumMustNotBeInvokedAfterFinish() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = new LinkedList<>();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(new LinkedList<>());
		final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null), inputFeatures,
				outputFeatures, inputFeatureValues, outputFeatureValues);

		final LibsvmTrainingDataPopulator populator = new LibsvmTrainingDataPopulator(trainingData);

		populator.finish();
		populator.addDatum(new double[] { 1d, 2d }, new double[] { 3d });
	}

	@Test(expected = IllegalArgumentException.class)
	public void fillWithTooFewValuesThrowsException() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = new LinkedList<>();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(new LinkedList<>());
		final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null), inputFeatures,
				outputFeatures, inputFeatureValues, outputFeatureValues);

		new LibsvmTrainingDataPopulator(trainingData).addDatum(new double[] { 1 }, new double[] { 2 });
	}

	@Test(expected = IllegalArgumentException.class)
	public void fillWithTooManyValuesThrowsException() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = new LinkedList<>();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(new LinkedList<>());
		final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null), inputFeatures,
				outputFeatures, inputFeatureValues, outputFeatureValues);

		new LibsvmTrainingDataPopulator(trainingData).addDatum(new double[] { 1 }, new double[] { 2, 3 });
	}

	// TODO: Remove libsvm-3.21.jar from Repository and use a MANIFEST.MF entry
	// instead.
	@Test
	public void obtainSvmNodesFromDatum() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = new LinkedList<>();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(new LinkedList<>());
		final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null), inputFeatures,
				outputFeatures, inputFeatureValues, outputFeatureValues);

		new LibsvmTrainingDataPopulator(trainingData).addDatum(new double[] { 1, 2 }, new double[] { 3 });
		assertThat(inputFeatureValues.size(), equalTo(1));
		assertThat(outputFeatureValues.size(), equalTo(1));

		final svm_node[] features = inputFeatureValues.get(0);
		assertSvmNode(features, 1);
		assertSvmNode(features, 2);
		assertThat(outputFeatureValues.get(0).size(), equalTo(1));
		assertThat(outputFeatureValues.get(0).get(0), equalTo(3.0));
	}

	private void assertSvmNode(final svm_node[] featureValues, final int index) {
		assertThat(featureValues[index - 1].index, equalTo(index));
		assertThat(featureValues[index - 1].value, equalTo((double) index));
	}
}
