package de.monochromata.ml.libsvm;

import static de.monochromata.ml.lowlevel.training.validation.ValidationUtilities.requireSameSize;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.monochromata.ml.libsvm.LibsvmTesting.TestDatum;
import de.monochromata.ml.libsvm.config.KernelType;
import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.libsvm.config.SvmType;
import de.monochromata.ml.libsvm.config.TechnicalHyperParameter;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.correction.Transformer;
import de.monochromata.ml.modelselection.Configuration;
import de.monochromata.ml.modelselection.HyperParameter;
import de.monochromata.ml.modelselection.NonIterativeModelSelection;
import de.monochromata.ml.stack.StackElement;
import de.monochromata.ml.stack.Stacking;
import de.monochromata.ml.stack.StackingStrategy;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_parameter;

public interface LibsvmStackTesting extends LibsvmModelSelectionTesting, StackPlotting<TestDatum> {

	default void assertStackElement(
			final StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> stackElement,
			final double expectedErrorLowerBound, final double expectedErrorUpperBound, final SvmType expectedSvmType,
			final KernelType expectedKernelType) {
		assertThat(stackElement.getResult().getAverageErrorValue()).isBetween(expectedErrorLowerBound,
				expectedErrorUpperBound);
		assertThat(stackElement.getResult().getModelType().getSvmType()).isEqualTo(expectedSvmType);
		assertThat(stackElement.getResult().getModelType().getKernelType()).isEqualTo(expectedKernelType);
	}

	default List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>> createAStackForATiltedParabola(
			final SvmType svmType, final StackingStrategy stackingStrategy) {

		try {
			svm.svm_set_print_string_function(message -> {
			});

			final List<TestDatum> data = getData();
			final List<List<Double>> truthValues = getTruthValues();

			final Map<ModelType, List<HyperParameter>> hyperParametersByModelType = createHyperParametersByModelType(
					svmType);
			final NonIterativeModelSelection<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> modelSelection = createModelSelection();
			final Configuration<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum> msConfiguration = createConfiguration(
					data, truthValues, hyperParametersByModelType);
			return new Stacking<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>()
					.assembleStack(stackingStrategy, modelSelection, msConfiguration,
							(result, configuration) -> new StackElement<>(result, configuration));
		} finally {
			svm.svm_set_print_string_function(null);
		}
	}

	default List<TestDatum> getData() {
		final List<TestDatum> data = new ArrayList<>();
		data.addAll(getDataForFold1());
		data.addAll(getDataForFold2());
		return data;
	}

	default List<TestDatum> getDataForFold1() {
		final List<TestDatum> data = new ArrayList<>();
		/* element 1 */ data.add(new TestDatum(-3, -1, 9 - 3));
		/* element 2 */ data.add(new TestDatum(-2, 0, 4 - 2));
		/* element 5 */ data.add(new TestDatum(1, 3, 1 + 1));
		/* element 6 */ data.add(new TestDatum(2, 4, 4 + 2));
		return data;
	}

	default List<TestDatum> getDataForFold2() {
		final List<TestDatum> data = new ArrayList<>();
		/* element 3 */ data.add(new TestDatum(-1, 1, 1 - 1));
		/* element 4 */ data.add(new TestDatum(0, 2, 0 + 0));
		/* element 7 */ data.add(new TestDatum(3, 5, 9 + 3));
		/* element 8 */ data.add(new TestDatum(4, 6, 16 + 4));
		return data;
	}

	default List<List<Double>> getTruthValues() {
		final List<List<Double>> truthValues = new ArrayList<>();
		truthValues.addAll(getTruthValuesForFold1());
		truthValues.addAll(getTruthValuesForFold2());
		return truthValues;
	}

	default List<List<Double>> getTruthValuesForFold1() {
		final List<List<Double>> truthValues = new ArrayList<>();
		/* element 1 */ truthValues.add(singletonList(9d - 3d));
		/* element 2 */ truthValues.add(singletonList(4d - 2d));
		/* element 5 */ truthValues.add(singletonList(1d + 1d));
		/* element 6 */ truthValues.add(singletonList(4d + 2d));
		return truthValues;
	}

	default List<List<Double>> getTruthValuesForFold2() {
		final List<List<Double>> truthValues = new ArrayList<>();
		/* element 3 */ truthValues.add(singletonList(1d - 1d));
		/* element 4 */ truthValues.add(singletonList(0d + 0d));
		/* element 7 */ truthValues.add(singletonList(9d + 3d));
		/* element 8 */ truthValues.add(singletonList(16d + 4d));
		return truthValues;
	}

	default Map<ModelType, List<HyperParameter>> createHyperParametersByModelType(final SvmType svmType) {
		final List<HyperParameter> hyperParameters0 = new ArrayList<>();
		hyperParameters0.addAll(KernelType.hyperParametersForLinearKernel());
		hyperParameters0.addAll(SvmType.hyperParametersForNuSVR(0.1d, 1d, 0.1d, 1d));
		hyperParameters0.addAll(TechnicalHyperParameter.getDefaults());

		final List<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.addAll(KernelType.hyperParametersForPolynomialKernel(1, 3, 0.5, 1, 0, 1));
		hyperParameters.addAll(SvmType.hyperParametersForNuSVR(0.1d, 1d, 0.1d, 1d));
		hyperParameters.addAll(TechnicalHyperParameter.getDefaults());

		final Map<ModelType, List<HyperParameter>> hyperParametersByModelType = new HashMap<>();
		hyperParametersByModelType.put(new ModelType(svmType, KernelType.Linear), hyperParameters0);
		hyperParametersByModelType.put(new ModelType(svmType, KernelType.Polynomial), hyperParameters);
		return hyperParametersByModelType;
	}

	@Override
	default double xAccessor(final TestDatum input) {
		return input.getIn1();
	}

	@Override
	default double yAccessor(final TestDatum input) {
		return input.getIn2();
	}

	@Override
	default double zAccessor(final TestDatum input, final int outputFeatureIndex) {
		requireOutputFeatureIndex0(outputFeatureIndex);
		return input.getOut1();
	}

	@Override
	default CorrectedValues getCorrectedValues(final double[] inputX, final double[] inputY,
			final int outputFeatureIndex, final CorrectionModel<libsvm.svm_node[], Double, TestDatum> correctionModel,
			final List<Transformer<libsvm.svm_node[], Double>> transformers) {
		requireSameSize(inputX, inputY, "inputX and inputY differ in length");
		final double[] dataZ = new double[inputX.length];
		for (int i = 0; i < inputX.length; i++) {
			final TestDatum input = new TestDatum(inputX[i], inputY[i], 0d);
			final TestDatum correctedDatum = correctionModel.apply(input, transformers);
			dataZ[i] = zAccessor(correctedDatum, outputFeatureIndex);
		}
		return new CorrectedValues(inputX, inputY, dataZ);
	}

	@Override
	default CorrectedValues getCorrectedValuesFromRanges(final double[] inputX, final double[] inputY,
			final int outputFeatureIndex, final CorrectionModel<libsvm.svm_node[], Double, TestDatum> correctionModel,
			final List<Transformer<libsvm.svm_node[], Double>> transformers) {
		requireSameSize(inputX, inputY, "inputX and inputY differ in length");
		final int numberOfPoints = inputX.length * inputY.length;
		final double[] dataX = new double[numberOfPoints];
		final double[] dataY = new double[numberOfPoints];
		final double[] dataZ = new double[numberOfPoints];
		int k = 0;
		for (int i = 0; i < inputX.length; i++) {
			for (int j = 0; j < inputY.length; j++) {
				dataX[k] = inputX[i];
				dataY[k] = inputY[j];
				final TestDatum input = new TestDatum(inputX[i], inputY[j], 0d);
				final TestDatum correctedDatum = correctionModel.apply(input, transformers);
				dataZ[k++] = zAccessor(correctedDatum, outputFeatureIndex);
			}
		}
		return new CorrectedValues(dataX, dataY, dataZ);
	}

	default void requireOutputFeatureIndex0(final int outputFeatureIndex) {
		if (outputFeatureIndex != 0) {
			throw new IllegalArgumentException("outputFeatureIndex must be 0");
		}
	}

}
