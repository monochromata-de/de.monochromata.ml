package de.monochromata.ml.libsvm;

import static de.monochromata.ml.libsvm.config.KernelType.Polynomial;
import static de.monochromata.ml.libsvm.config.SvmType.EpsilonSVR;
import static de.monochromata.ml.libsvm.config.SvmType.NuSVR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.stack.MaximumNumberOfLayers;
import de.monochromata.ml.stack.MinimumErrorReduction;
import de.monochromata.ml.stack.StackElement;
import libsvm.svm_model;
import libsvm.svm_parameter;

/**
 * A test that uses a stack to learn a 1-dimensional output function. The output
 * does not overwrite the input values.
 */
public class LibsvmStackingTest implements LibsvmStackTesting {

	@Test
	public void createAStackForATiltedParabolaWithEpsilonSVRWithMaximumNumberOf2Layers() {
		final List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>> stack = createAStackForATiltedParabola(
				EpsilonSVR, new MaximumNumberOfLayers(2));

		assertThat(stack).hasSize(2);
		assertStackElement(stack.get(0), .002595d, 0.002596d, EpsilonSVR, Polynomial);
		assertStackElement(stack.get(1), .0000002047d, .0000002048d, EpsilonSVR, Polynomial);
	}

	@Test
	public void createAStackForATiltedParabolaWithEpsilonSVRWithMaximumNumberOf3Layers() {
		final List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>> stack = createAStackForATiltedParabola(
				EpsilonSVR, new MaximumNumberOfLayers(3));

		assertThat(stack).hasSize(3);
		assertStackElement(stack.get(0), .002595d, .002596d, EpsilonSVR, Polynomial);
		assertStackElement(stack.get(1), .0000002047d, .0000002048d, EpsilonSVR, Polynomial);
		assertStackElement(stack.get(2), .0000002047d, .0000002048d, EpsilonSVR, Polynomial);
	}

	@Test
	public void createAStackForATiltedParabolaWithEpsilonSVRWithMinimumErrorReduction() {
		final List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>> stack = createAStackForATiltedParabola(
				EpsilonSVR, new MinimumErrorReduction(0.01));

		assertThat(stack).hasSize(1);
		assertStackElement(stack.get(0), 0.002595d, 0.002596d, EpsilonSVR, Polynomial);
	}

	@Test
	public void createAStackForATiltedParabolaWithNuSVRWithMaximumNumberOf2Layers() {
		final List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>> stack = createAStackForATiltedParabola(
				NuSVR, new MaximumNumberOfLayers(2));

		assertThat(stack).hasSize(2);
		assertStackElement(stack.get(0), .006805d, .006806d, NuSVR, Polynomial);
		assertStackElement(stack.get(1), .00000121d, .00000122d, NuSVR, Polynomial);
	}

	@Test
	public void createAStackForATiltedParabolaWithNuSVRWithMaximumNumberOf3Layers() {
		final List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>> stack = createAStackForATiltedParabola(
				NuSVR, new MaximumNumberOfLayers(3));

		assertThat(stack).hasSize(3);
		assertStackElement(stack.get(0), .006805d, .006806d, NuSVR, Polynomial);
		assertStackElement(stack.get(1), .00000121d, .00000122d, NuSVR, Polynomial);
		assertStackElement(stack.get(2), .00000030d, .00000031d, NuSVR, Polynomial);
	}

	@Test
	public void createAStackForATiltedParabolaWithNuSVRWithMinimumErrorReduction() {
		final List<StackElement<ModelType, svm_parameter, libsvm.svm_node[], Double, List<svm_model>, TestDatum>> stack = createAStackForATiltedParabola(
				NuSVR, new MinimumErrorReduction(0.01));

		assertThat(stack).hasSize(1);
		assertStackElement(stack.get(0), 0.006805d, 0.006806d, NuSVR, Polynomial);
	}

	@Ignore
	@Test
	public void useStackToCorrectData() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void addATestForAMultiDimensionalProblem() {
		fail("Not yet implemented");
	}

}
