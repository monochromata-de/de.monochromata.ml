package de.monochromata.ml.libsvm;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

public interface LibsvmTesting extends LibsvmSupport {

	/**
	 * The format string used to format double values using a fixed precision.
	 */
	public static final String DOUBLE_FORMAT = "%1$.5f";

	default void assertInputValuesWithFixedPrecision(final libsvm.svm_node[] actualValues,
			final double... expectedValues) {
		assertThat(actualValues.length, equalTo(expectedValues.length));
		final Stream<Double> streamOfActualValues = Arrays.stream(actualValues).map(node -> node.value);
		final Stream<Double> streamOfExpectedValues = Arrays.stream(expectedValues)
				.mapToObj(doubleValue -> doubleValue);
		assertStreamsOfDoublesWithFixedPrecision(streamOfActualValues, streamOfExpectedValues);
	}

	default void assertListsOfDoublesWithFixedPrecision(final List<Double> actualValues,
			final List<Double> expectedValues) {
		assertStreamsOfDoublesWithFixedPrecision(actualValues.stream(), expectedValues.stream());
	}

	default void assertStreamsOfDoublesWithFixedPrecision(final Stream<Double> streamOfActualValues,
			final Stream<Double> streamOfExpectedValues) {
		final List<String> listOfActualStrings = streamOfActualValues.map(LibsvmTesting::formatDouble)
				.collect(toList());
		final List<String> listOfExpectedStrings = streamOfExpectedValues.map(LibsvmTesting::formatDouble)
				.collect(toList());
		assertThat(listOfActualStrings, equalTo(listOfExpectedStrings));
	}

	default void assertDoubleEqualsWithFixedPrecision(final double actualValue, final double expectedValue) {
		assertThat(formatDouble(actualValue), equalTo(formatDouble(expectedValue)));
	}

	default svm_parameter createValidDefaultHyperParameters() {
		return createHyperParameters(1e-3, 1);
	}

	default svm_parameter createHyperParameters(final double eps, final int C) {
		final svm_parameter hyperParameters = new svm_parameter();
		hyperParameters.svm_type = svm_parameter.EPSILON_SVR;
		hyperParameters.cache_size = 100;
		hyperParameters.eps = eps;
		hyperParameters.C = C;
		return hyperParameters;
	}

	/**
	 * Format the given double using {@link #DOUBLE_FORMAT}.
	 * 
	 * @param doubleValue
	 *            the double to format
	 * @return the formatted double value
	 */
	static String formatDouble(final double doubleValue) {
		return format(DOUBLE_FORMAT, doubleValue);
	}

	default CorrectionDatum<svm_node[], Double> predictingCorrector(final CorrectionDatum<svm_node[], Double> input,
			final List<svm_model> models) {
		final double predictedOut = svm.svm_predict(models.get(0), input.getInputFeatureValues());
		return LibsvmCorrectionDatum.of(input.getInputFeatureValues(), singletonList(predictedOut));
	}

	default CorrectionDatum<svm_node[], Double> intReader(final Integer input) {
		return LibsvmCorrectionDatum.of(svm_nodes(input), emptyList());
	}

	default Integer intWriter(final CorrectionDatum<svm_node[], Double> data, final Integer input) {
		return input;
	}

	default CorrectionDatum<svm_node[], Double> testDatumReader(final TestDatum input) {
		return LibsvmCorrectionDatum.of(svm_nodes(input.getIn1(), input.getIn2()), Arrays.asList(input.out1));
	}

	default TestDatum testDatumWriter(final CorrectionDatum<svm_node[], Double> data, final TestDatum input) {
		input.setOut1(data.getOutputFeatureValues().get(0));
		return input;
	}

	static class TestDatum {

		private double in1;
		private double in2;
		private double out1;

		public TestDatum(final double in1, final double in2, final double out1) {
			this.in1 = in1;
			this.in2 = in2;
			this.out1 = out1;
		}

		protected double getIn1() {
			return in1;
		}

		protected void setIn1(final double in1) {
			this.in1 = in1;
		}

		protected double getIn2() {
			return in2;
		}

		protected void setIn2(final double in2) {
			this.in2 = in2;
		}

		protected double getOut1() {
			return out1;
		}

		protected void setOut1(final double out1) {
			this.out1 = out1;
		}

		@Override
		public String toString() {
			return format("TestDatum [in1=%1$.5f, in2=%2$.5f, out1=%3$.5f]", in1, in2, out1);
		}

		public static List<TestDatum> copy(final List<TestDatum> input) {
			final List<TestDatum> copies = new ArrayList<>(input.size());
			for (final TestDatum datum : input) {
				copies.add(new TestDatum(datum.getIn1(), datum.getIn2(), datum.getOut1()));
			}
			return copies;
		}

	}
}
