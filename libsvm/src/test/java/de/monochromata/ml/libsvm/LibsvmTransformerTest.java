package de.monochromata.ml.libsvm;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import libsvm.svm_model;
import libsvm.svm_node;

public class LibsvmTransformerTest implements LibsvmTesting {

	@Test
	public void inputIsTransformedCorrectlyAndOutputIsUnaffected() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = Arrays.asList(svm_nodes(-10), svm_nodes(0), svm_nodes(10),
				svm_nodes(20), svm_nodes(30), svm_nodes(40), svm_nodes(50));
		final List<List<Double>> outputFeatureValues = Arrays
				.asList(new ArrayList<>(Arrays.asList(-100d, 0d, 100d, 200d, 300d, 400d, 500d)));
		final LibsvmTrainingData<svm_model, TestDatum> trainingData = new LibsvmTrainingData<>(new Libsvm(null, null),
				inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);

		final LibsvmScaler scaler = LibsvmScaler.fromMinus1ToPlus1();
		scaler.apply(trainingData);

		final CorrectionDatum<libsvm.svm_node[], Double> datum = LibsvmCorrectionDatum.of(svm_nodes(10),
				singletonList(100d));

		final LibsvmTransformer transformer = new LibsvmTransformer(scaler);
		final CorrectionDatum<libsvm.svm_node[], Double> correctedDatum = transformer.transformInput(datum);

		assertThat(correctedDatum.getInputFeatureValues().length, equalTo(1));
		assertDoubleEqualsWithFixedPrecision(correctedDatum.getInputFeatureValues()[0].value, -1d / 3);
		assertThat(correctedDatum.getOutputFeatureValues().size(), equalTo(1));
		assertDoubleEqualsWithFixedPrecision(correctedDatum.getOutputFeatureValues().get(0), 100d);
	}

	@Test
	public void preprocessingIsInvertedCorrectly() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = Arrays.asList(svm_nodes(-10), svm_nodes(0), svm_nodes(10),
				svm_nodes(20), svm_nodes(30), svm_nodes(40), svm_nodes(50));
		final List<List<Double>> outputFeatureValues = Arrays
				.asList(new ArrayList<>(Arrays.asList(-100d, 0d, 100d, 200d, 300d, 400d, 500d)));
		final LibsvmTrainingData<svm_model, TestDatum> trainingData = new LibsvmTrainingData<>(new Libsvm(null, null),
				inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);

		final LibsvmScaler scaler = LibsvmScaler.fromMinus1ToPlus1();
		scaler.apply(trainingData);

		final CorrectionDatum<libsvm.svm_node[], Double> datum = LibsvmCorrectionDatum.of(svm_nodes(-1d / 3),
				singletonList(2d / 3));

		final LibsvmTransformer transformer = new LibsvmTransformer(scaler);
		final CorrectionDatum<libsvm.svm_node[], Double> correctedDatum = transformer.invertPreprocessing(datum);

		assertThat(correctedDatum.getInputFeatureValues().length, equalTo(1));
		assertDoubleEqualsWithFixedPrecision(correctedDatum.getInputFeatureValues()[0].value, 10);
		assertThat(correctedDatum.getOutputFeatureValues().size(), equalTo(1));
		assertDoubleEqualsWithFixedPrecision(correctedDatum.getOutputFeatureValues().get(0), 400d);
	}

}
