package de.monochromata.ml.libsvm;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.ML;
import libsvm.svm;
import libsvm.svm_parameter;

public class LibsvmCompleteTest implements LibsvmTesting {

	@Test
	public void aCompletePipelineTest() {
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final PrintStream ps = new PrintStream(baos);

			svm.svm_set_print_string_function(ps::print);

			final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
			final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
			final svm_parameter hyperParameters = createHyperParameters(1e-3, 1);
			final LibsvmScaler scaler = LibsvmScaler.fromMinus1ToPlus1();
			final LibsvmTransformer transformer = new LibsvmTransformer(scaler);
			final TestDatum datumToCorrect = new TestDatum(2.5d, 3d, 0d);

			final Libsvm<TestDatum> spi = new Libsvm<TestDatum>(this::testDatumReader, this::testDatumWriter);
			final TestDatum correctedValue = ML.getInstance(spi).trainingData(inputFeatures, outputFeatures)
					.populate(populator -> {
						populator.addDatum(new double[] { 1, 0 }, new double[] { 8 });
						populator.addDatum(new double[] { 2, 2 }, new double[] { 4 });
						populator.addDatum(new double[] { 3, 4 }, new double[] { 2 });
						populator.addDatum(new double[] { 4, 6 }, new double[] { 1 });
						populator.addDatum(new double[] { 3, 4 }, new double[] { 2 });
						populator.addDatum(new double[] { 2, 2 }, new double[] { 4 });
						populator.addDatum(new double[] { 1, 0 }, new double[] { 8 });
						populator.finish();
					}).thenCompose(trainingData -> trainingData.preprocess(Collections.singletonList(scaler)))
					.thenCompose(training -> training.train(hyperParameters, this::testDatumReader,
							this::predictingCorrector, this::testDatumWriter))
					.thenApply(correctionModel -> correctionModel.apply(datumToCorrect, transformer))
					.toCompletableFuture().join();

			final String expectedLogOutput = "*\n" + "optimization finished, #iter = 6\n" + "nu = 0.8520408165213481\n"
					+ "obj = -1.556122467983799, rho = 0.28571426729479454\n" + "nSV = 6, nBSV = 4\n";

			assertDoubleEqualsWithFixedPrecision(correctedValue.getIn1(), 2.5d);
			assertDoubleEqualsWithFixedPrecision(correctedValue.getIn2(), 3d);
			assertDoubleEqualsWithFixedPrecision(correctedValue.getOut1(), 3.5d);
			assertThat(baos.toString(), equalTo(expectedLogOutput));
		} finally {
			svm.svm_set_print_string_function(null);
		}
	}

}
