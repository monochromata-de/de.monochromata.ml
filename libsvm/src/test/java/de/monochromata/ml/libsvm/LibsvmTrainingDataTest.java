package de.monochromata.ml.libsvm;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;

public class LibsvmTrainingDataTest {

	@Test(expected = IllegalArgumentException.class)
	public void missingKeysInOutputFeatureValuesYieldsException() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<List<Double>> outputFeatureValues = Collections.emptyList();

		new LibsvmTrainingData(new Libsvm(null, null), inputFeatures, outputFeatures, new LinkedList<>(),
				outputFeatureValues);
	}

	@Test(expected = NullPointerException.class)
	public void providedOutputFeatureValuesNeedsToHaveASubListForEachOutputFeature() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<List<Double>> outputFeatureValues = Collections.singletonList(null);

		new LibsvmTrainingData(new Libsvm(null, null), inputFeatures, outputFeatures, new LinkedList<>(),
				outputFeatureValues);
	}

}
