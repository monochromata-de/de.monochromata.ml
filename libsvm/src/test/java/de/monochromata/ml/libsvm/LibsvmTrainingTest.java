package de.monochromata.ml.libsvm;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.MLConfigurationException;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import libsvm.svm;
import libsvm.svm_node;
import libsvm.svm_parameter;

public class LibsvmTrainingTest implements LibsvmTesting {

	@Test
	public void multipleInvocationsOfTrainMethodArePermitted() {
		final LibsvmTraining<Integer> trainingModel = new LibsvmTraining<>(new Libsvm(null, null),
				Collections.emptyList(), Collections.emptyList());
		trainingModel.train(createValidDefaultHyperParameters(), this::intReader, (input, modelz) -> input,
				this::intWriter);
		trainingModel.train(createValidDefaultHyperParameters(), this::intReader, (input, modelz) -> input,
				this::intWriter);
	}

	@Test
	public void trainMethodsCompletableFutureContainsACorrectionModel() {
		final CorrectionModel<svm_node[], Double, Integer> correctionModel = new LibsvmTraining<Integer>(
				new Libsvm(null, null), Collections.emptyList(), Collections.emptyList())
						.train(createValidDefaultHyperParameters(), this::intReader, (input, modelz) -> input,
								this::intWriter)
						.toCompletableFuture().join();
		assertThat(correctionModel, notNullValue());
	}

	@Test(expected = MLConfigurationException.class)
	public void misconfigurationRaisesSVMConfigurationException() {
		final List<svm_node[]> inputFeatureValues = Collections.emptyList();
		final List<List<Double>> outputFeatureValues = Collections.singletonList(Collections.emptyList());
		final svm_parameter invalidHyperParameters = new svm_parameter();
		final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null),
				Arrays.asList(new Feature("in1")), Arrays.asList(new Feature("out1")), inputFeatureValues,
				outputFeatureValues);

		new LibsvmTraining<Integer>(trainingData).train(invalidHyperParameters, this::intReader,
				(input, modelz) -> input, this::intWriter);
	}

	@Test
	public void trainMethodPerformsTrainingInProvidedExecutor() {
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final PrintStream ps = new PrintStream(baos);

			svm.svm_set_print_string_function(ps::print);

			final List<svm_node[]> inputFeatureValues = Collections.emptyList();
			final List<List<Double>> outputFeatureValues = Collections.singletonList(Collections.emptyList());
			final TestableExecutor executor = new TestableExecutor();
			final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null),
					Arrays.asList(new Feature("in1")), Arrays.asList(new Feature("out1")), inputFeatureValues,
					outputFeatureValues);

			final String expectedLogOutput = ".*\n" + "optimization finished, #iter = 0\n" + "nu = NaN\n"
					+ "obj = 0.0, rho = NaN\n" + "nSV = 0, nBSV = 0\n";

			new LibsvmTraining<Integer>(trainingData).train(createValidDefaultHyperParameters(), this::intReader,
					(input, modelz) -> input, this::intWriter, executor);

			assertThat(executor.runnablesExecuted, equalTo(1));
			assertThat(baos.toString(), equalTo(expectedLogOutput));
		} finally {
			svm.svm_set_print_string_function(null);
		}
	}

	@Test
	public void trainForAOneDimensionalProblem() {
		handleAOneDimensionalProblem((training, baos) -> {
			trainForAOneDimentionalProblem(training, baos, createValidDefaultHyperParameters(),
					new TestDatum(10d, 0d, 0d),
					"*\n" + "optimization finished, #iter = 1\n" + "nu = 0.05555555555555555\n"
							+ "obj = -0.1111111111111111, rho = -4.222222222222222\n" + "nSV = 2, nBSV = 0\n",
					4.77778d);
		});
	}

	@Test
	public void trainMethodTrainsTwiceWithDifferentHyperparameters() {
		handleAOneDimensionalProblem((training, baos) -> {
			trainForAOneDimentionalProblem(training, baos, createHyperParameters(1e-3, 1), new TestDatum(10d, 0d, 0d),
					"*\n" + "optimization finished, #iter = 1\n" + "nu = 0.05555555555555555\n"
							+ "obj = -0.1111111111111111, rho = -4.222222222222222\n" + "nSV = 2, nBSV = 0\n",
					4.77778d);
			trainForAOneDimentionalProblem(training, baos, createHyperParameters(1e-7, 3), new TestDatum(10d, 0d, 0d),
					"*\n" + "optimization finished, #iter = 1\n" + "nu = 0.018518518518518517\n"
							+ "obj = -0.1111111111111111, rho = -4.222222222222222\n" + "nSV = 2, nBSV = 0\n",
					4.77778d);
		});
	}

	private void trainForAOneDimentionalProblem(final LibsvmTraining<TestDatum> training,
			final ByteArrayOutputStream baos, final svm_parameter hyperParameters, final TestDatum datumToCorrect,
			final String expectedLogOutput, final double expectedCorrectedValue) {
		try {
			final CorrectionModel<svm_node[], Double, TestDatum> correctionModel = training
					.train(hyperParameters, this::testDatumReader, this::predictingCorrector, this::testDatumWriter)
					.toCompletableFuture().join();
			final TestDatum correctedDatum = correctionModel.apply(datumToCorrect);

			assertDoubleEqualsWithFixedPrecision(correctedDatum.getOut1(), expectedCorrectedValue);
			assertThat(baos.toString(), equalTo(expectedLogOutput));
		} finally {
			baos.reset();
		}
	}

	private void handleAOneDimensionalProblem(
			final BiConsumer<LibsvmTraining<TestDatum>, ByteArrayOutputStream> trainer) {
		try {
			final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"), new Feature("in2"));
			final List<svm_node[]> inputFeatureValues = new LinkedList<>();
			inputFeatureValues.add(svm_nodes(1, 2, 3, 4, 5, 6, 7));
			inputFeatureValues.add(svm_nodes(0, 2, 4, 6, 4, 2, 0));

			final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
			final List<List<Double>> outputFeatureValues = Collections
					.singletonList(Arrays.asList(8d, 4d, 2d, 1d, 2d, 4d, 8d));

			final LibsvmTrainingData trainingData = new LibsvmTrainingData(new Libsvm(null, null), inputFeatures,
					outputFeatures, inputFeatureValues, outputFeatureValues);

			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final PrintStream ps = new PrintStream(baos);

			svm.svm_set_print_string_function(ps::print);

			final LibsvmTraining<TestDatum> training = new LibsvmTraining<TestDatum>(trainingData);
			trainer.accept(training, baos);
		} finally {
			svm.svm_set_print_string_function(null);
		}
	}

	private static class TestableExecutor implements Executor {

		private int runnablesExecuted = 0;

		@Override
		public void execute(final Runnable command) {
			command.run();
			runnablesExecuted++;
		}

	}
}
