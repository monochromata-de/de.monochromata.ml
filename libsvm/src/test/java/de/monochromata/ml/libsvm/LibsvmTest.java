package de.monochromata.ml.libsvm;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.ML;

public class LibsvmTest {

	@Test(expected = IllegalArgumentException.class)
	public void nullInputFeaturesRaiseException() {
		ML.getInstance(new Libsvm<>(null, null)).trainingData(null, singletonList(new Feature("output")));
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyListOfInputFeaturesRaisesException() {
		ML.getInstance(new Libsvm<>(null, null)).trainingData(emptyList(), singletonList(new Feature("output")));
	}

	@Test(expected = IllegalArgumentException.class)
	public void nullOutputFeaturesRaiseException() {
		ML.getInstance(new Libsvm<>(null, null)).trainingData(singletonList(new Feature("input")), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyListOfOutputFeaturesRaisesException() {
		ML.getInstance(new Libsvm<>(null, null)).trainingData(singletonList(new Feature("input")), emptyList());
	}
}
