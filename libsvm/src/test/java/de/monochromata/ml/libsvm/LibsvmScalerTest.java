package de.monochromata.ml.libsvm;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.monochromata.ml.lowlevel.Feature;
import libsvm.svm_node;

public class LibsvmScalerTest implements LibsvmTesting {

	@Test
	public void invokingApplyTwiceYieldsAnException() {
		final LibsvmScaler scaler = LibsvmScaler.from0to1();
		final LibsvmTrainingData<Object, Object> trainingData = createTrainingData();
		scaler.apply(trainingData);

		assertThatThrownBy(() -> {
			scaler.apply(trainingData);
		}).isInstanceOf(IllegalStateException.class)
				.hasMessage("LibsvmScaler must not be re-trained. Use a new instance.");
	}

	@Test
	public void scalingIsRejectedIfDataRangeIs0AndAllDataValuesAreIdenticalToDesiredMin() {
		final LibsvmScaler scaler = LibsvmScaler.fromMintoMax(2, 3);
		final LibsvmTrainingData<Object, Object> trainingData = createSingleValueTrainingData(2, 3);

		assertThatThrownBy(() -> {
			scaler.apply(trainingData);
		}).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("TrainingData range is empty and identical to target minimum: min=2.0 max=2.0");
	}

	@Test
	public void scalingIsPerformedIfDataRangeIs0ButAllDataValuesDifferFromDesignedMin() {
		final LibsvmScaler scaler = LibsvmScaler.fromMintoMax(2, 3);
		final LibsvmTrainingData<Object, Object> trainingData = createSingleValueTrainingData(100, 1000);
		scaler.apply(trainingData);

		final List<libsvm.svm_node[]> scaledInputValues = trainingData.getInputFeatureValues();
		assertInputValuesWithFixedPrecision(scaledInputValues.get(0), 3d);
		assertListsOfDoublesWithFixedPrecision(trainingData.getOutputFeatureValues().get(0), singletonList(3d));
	}

	@Test
	public strictfp void scalingFrom0To1WorksCorrectly() {
		final LibsvmTrainingData<Object, Object> trainingData = createTrainingData();

		LibsvmScaler.from0to1().apply(trainingData);

		final List<svm_node[]> scaledInputValues = trainingData.getInputFeatureValues();
		assertInputValuesWithFixedPrecision(scaledInputValues.get(0), 0d);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(1), 1d / 6);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(2), 2d / 6);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(3), 3d / 6);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(4), 4d / 6);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(5), 5d / 6);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(6), 1d);

		assertListsOfDoublesWithFixedPrecision(trainingData.getOutputFeatureValues().get(0),
				asList(0d, 1d / 6, 2d / 6, 3d / 6, 4d / 6, 5d / 6, 1d));
	}

	@Test
	public strictfp void scalingFromMinus1To1WorksCorrectly() {
		final LibsvmTrainingData<Object, Object> trainingData = createTrainingData();

		LibsvmScaler.fromMinus1ToPlus1().apply(trainingData);

		final List<svm_node[]> scaledInputValues = trainingData.getInputFeatureValues();
		assertInputValuesWithFixedPrecision(scaledInputValues.get(0), -1d);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(1), -2d / 3);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(2), -1d / 3);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(3), -0d);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(4), 1d / 3);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(5), 2d / 3);
		assertInputValuesWithFixedPrecision(scaledInputValues.get(6), 1d);

		assertListsOfDoublesWithFixedPrecision(trainingData.getOutputFeatureValues().get(0),
				Arrays.asList(-1d, -2d / 3, -1d / 3, 0d, 1d / 3, 2d / 3, 1d));
	}

	protected LibsvmTrainingData<Object, Object> createSingleValueTrainingData(final int inputValue,
			final int outputValue) {
		final List<Feature> inputFeatures = singletonList(new Feature("in1"));
		final List<Feature> outputFeatures = singletonList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = singletonList(svm_nodes(inputValue));
		final List<List<Double>> outputFeatureValues = singletonList(
				new ArrayList<>(singletonList((double) outputValue)));
		final LibsvmTrainingData<Object, Object> trainingData = new LibsvmTrainingData<>(new Libsvm(null, null),
				inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);
		return trainingData;
	}

	protected LibsvmTrainingData<Object, Object> createTrainingData() {
		final List<Feature> inputFeatures = Arrays.asList(new Feature("in1"));
		final List<Feature> outputFeatures = Arrays.asList(new Feature("out1"));
		final List<svm_node[]> inputFeatureValues = asList(svm_nodes(-10), svm_nodes(0), svm_nodes(10), svm_nodes(20),
				svm_nodes(30), svm_nodes(40), svm_nodes(50));
		final List<List<Double>> outputFeatureValues = Arrays
				.asList(new ArrayList<>(asList(-100d, 0d, 100d, 200d, 300d, 400d, 500d)));
		final LibsvmTrainingData<Object, Object> trainingData = new LibsvmTrainingData<>(new Libsvm(null, null),
				inputFeatures, outputFeatures, inputFeatureValues, outputFeatureValues);
		return trainingData;
	}

}
