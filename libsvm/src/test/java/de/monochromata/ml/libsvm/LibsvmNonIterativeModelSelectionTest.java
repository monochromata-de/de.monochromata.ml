package de.monochromata.ml.libsvm;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import de.monochromata.ml.libsvm.config.KernelType;
import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.libsvm.config.SvmType;
import de.monochromata.ml.libsvm.config.TechnicalHyperParameter;
import de.monochromata.ml.modelselection.HyperParameter;
import de.monochromata.ml.modelselection.ModelSelection.Result;
import libsvm.svm_parameter;

public class LibsvmNonIterativeModelSelectionTest implements LibsvmModelSelectionTesting {

	@Test
	public void chooseHyperParametersForALinearKernelWithNuSVR() {
		final List<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.addAll(KernelType.hyperParametersForLinearKernel());
		hyperParameters.addAll(SvmType.hyperParametersForNuSVR(0.1d, 1d, 0.1d, 1d));
		hyperParameters.addAll(TechnicalHyperParameter.getDefaults());
		final Map<ModelType, List<HyperParameter>> hyperParametersByModelType = new HashMap<>();
		hyperParametersByModelType.put(new ModelType(SvmType.NuSVR, KernelType.Linear), hyperParameters);

		final String expectedLogOutput = "*\n" + "optimization finished, #iter = 1\n" + "epsilon = 0.94\n"
				+ "obj = -0.029100000000000004, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "epsilon = 0.94\n" + "obj = -0.029100000000000004, rho = 0.0\n"
				+ "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n"
				+ "epsilon = 0.6699999999999999\n" + "obj = -0.13777500000000004, rho = 0.0\n" + "nSV = 2, nBSV = 0\n"
				+ "*\n" + "optimization finished, #iter = 1\n" + "epsilon = 0.6699999999999999\n"
				+ "obj = -0.13777500000000004, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 3\n" + "epsilon = -0.0\n" + "obj = -0.16, rho = 0.0\n"
				+ "nSV = 2, nBSV = 2\n" + "*\n" + "optimization finished, #iter = 3\n" + "epsilon = -0.0\n"
				+ "obj = -0.16, rho = 0.0\n" + "nSV = 2, nBSV = 2\n" + "*\n" + "optimization finished, #iter = 1\n"
				+ "epsilon = 0.6699999999999999\n" + "obj = -0.13777500000000004, rho = 0.0\n" + "nSV = 2, nBSV = 0\n"
				+ "*\n" + "optimization finished, #iter = 1\n" + "epsilon = 0.6699999999999999\n"
				+ "obj = -0.13777500000000004, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "epsilon = -0.0\n" + "obj = -0.25, rho = 0.0\n"
				+ "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n" + "epsilon = -0.0\n"
				+ "obj = -0.25, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 2\n"
				+ "epsilon = -0.0\n" + "obj = -0.25, rho = 0.0\n" + "nSV = 3, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 2\n" + "epsilon = -0.0\n" + "obj = -0.25, rho = 0.0\n"
				+ "nSV = 3, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n"
				+ "epsilon = 0.3999999999999999\n" + "obj = -0.21000000000000002, rho = 0.0\n" + "nSV = 2, nBSV = 0\n"
				+ "*\n" + "optimization finished, #iter = 1\n" + "epsilon = 0.3999999999999999\n"
				+ "obj = -0.21000000000000002, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "epsilon = -1.1102230246251565E-16\n"
				+ "obj = -0.25, rho = -1.1102230246251565E-16\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "epsilon = -1.1102230246251565E-16\n"
				+ "obj = -0.25, rho = -1.1102230246251565E-16\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "epsilon = -0.0\n" + "obj = -0.25, rho = 0.0\n"
				+ "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n" + "epsilon = -0.0\n"
				+ "obj = -0.25, rho = 0.0\n" + "nSV = 2, nBSV = 0\n";

		final Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> result = chooseHyperParametersForALinearKernel(
				hyperParametersByModelType, expectedLogOutput);

		assertThat(result.getModelConfiguration().C).isEqualTo(0.55d);
		assertThat(result.getModelConfiguration().nu).isEqualTo(0.55d);
		assertThat(result.getAverageErrorValue()).isEqualTo(0.0d);
	}

	@Test
	public void chooseHyperParametersForALinearKernelWithEpsilonSVR() {
		final List<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.addAll(KernelType.hyperParametersForLinearKernel());
		hyperParameters.addAll(SvmType.hyperParametersForEpsilonSVR(0.1d, 1d, 0.001d, 0.1d));
		hyperParameters.addAll(TechnicalHyperParameter.getDefaults());
		final Map<ModelType, List<HyperParameter>> hyperParametersByModelType = new HashMap<>();
		hyperParametersByModelType.put(new ModelType(SvmType.EpsilonSVR, KernelType.Linear), hyperParameters);

		final String expectedLogOutput = "*\n" + "optimization finished, #iter = 1\n" + "nu = 0.6666666666666666\n"
				+ "obj = -0.1598, rho = 0.0\n" + "nSV = 2, nBSV = 2\n" + "*\n" + "optimization finished, #iter = 1\n"
				+ "nu = 0.6666666666666666\n" + "obj = -0.1598, rho = 0.0\n" + "nSV = 2, nBSV = 2\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "nu = 0.6666666666666666\n" + "obj = -0.1499, rho = 0.0\n"
				+ "nSV = 2, nBSV = 2\n" + "*\n" + "optimization finished, #iter = 1\n" + "nu = 0.6666666666666666\n"
				+ "obj = -0.1499, rho = 0.0\n" + "nSV = 2, nBSV = 2\n" + "*\n" + "optimization finished, #iter = 1\n"
				+ "nu = 0.6666666666666666\n" + "obj = -0.13999999999999999, rho = 0.0\n" + "nSV = 2, nBSV = 2\n"
				+ "*\n" + "optimization finished, #iter = 1\n" + "nu = 0.6666666666666666\n"
				+ "obj = -0.13999999999999999, rho = 0.0\n" + "nSV = 2, nBSV = 2\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "nu = 0.3027272727272727\n" + "obj = -0.24950025, rho = 0.0\n"
				+ "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n" + "nu = 0.3027272727272727\n"
				+ "obj = -0.24950025, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "nu = 0.2877272727272727\n"
				+ "obj = -0.2253875625, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "nu = 0.2877272727272727\n"
				+ "obj = -0.2253875625, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "nu = 0.2727272727272727\n" + "obj = -0.2025, rho = 0.0\n"
				+ "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n" + "nu = 0.2727272727272727\n"
				+ "obj = -0.2025, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n"
				+ "nu = 0.1665\n" + "obj = -0.24950025, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "nu = 0.1665\n" + "obj = -0.24950025, rho = 0.0\n"
				+ "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n" + "nu = 0.15825\n"
				+ "obj = -0.2253875625, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n"
				+ "optimization finished, #iter = 1\n" + "nu = 0.15825\n" + "obj = -0.2253875625, rho = 0.0\n"
				+ "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n" + "nu = 0.15\n"
				+ "obj = -0.2025, rho = 0.0\n" + "nSV = 2, nBSV = 0\n" + "*\n" + "optimization finished, #iter = 1\n"
				+ "nu = 0.15\n" + "obj = -0.2025, rho = 0.0\n" + "nSV = 2, nBSV = 0\n";

		final Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> result = chooseHyperParametersForALinearKernel(
				hyperParametersByModelType, expectedLogOutput);

		assertThat(result.getModelConfiguration().C).isEqualTo(0.55d);
		assertThat(result.getModelConfiguration().eps).isEqualTo(0.001d);
		assertThat(result.getAverageErrorValue()).isBetween(0.00155456, 0.00155457);
	}

	private Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> chooseHyperParametersForALinearKernel(
			final Map<ModelType, List<HyperParameter>> hyperParametersByModelType, final String expectedLogOutput) {

		final List<TestDatum> data = new ArrayList<>();
		data.add(new TestDatum(1, 7, -3));
		data.add(new TestDatum(2, 6, -2.5));
		data.add(new TestDatum(3, 5, -2));
		data.add(new TestDatum(4, 4, -1.5));
		data.add(new TestDatum(5, 3, -1));
		data.add(new TestDatum(6, 2, -0.5));
		data.add(new TestDatum(7, 1, 0));

		final List<List<Double>> truthValues = new ArrayList<>();
		truthValues.add(singletonList(-3d));
		truthValues.add(singletonList(-2.5d));
		truthValues.add(singletonList(-2d));
		truthValues.add(singletonList(-1.5d));
		truthValues.add(singletonList(-1d));
		truthValues.add(singletonList(-0.5d));
		truthValues.add(singletonList(0d));

		return chooseHyperParameters(data, truthValues, hyperParametersByModelType, expectedLogOutput);
	}

	@Test
	public void chooseHyperParametersForAPolynomialKernelWithNuSVR() {
		final List<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.addAll(KernelType.hyperParametersForPolynomialKernel(1, 3, 0.5, 1, 0, 1));
		hyperParameters.addAll(SvmType.hyperParametersForNuSVR(0.1d, 1d, 0.1d, 1d));
		hyperParameters.addAll(TechnicalHyperParameter.getDefaults());
		final Map<ModelType, List<HyperParameter>> hyperParametersByModelType = new HashMap<>();
		hyperParametersByModelType.put(new ModelType(SvmType.NuSVR, KernelType.Polynomial), hyperParameters);

		final Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> result = chooseHyperParametersForAPolynomialKernel(
				hyperParametersByModelType);

		assertThat(result.getModelConfiguration().C).isEqualTo(1.0d);
		assertThat(result.getModelConfiguration().nu).isEqualTo(1.0d);
		assertThat(result.getAverageErrorValue()).isEqualTo(0.0d);
	}

	@Test
	public void chooseHyperParametersForAPolynomialKernelWithEpsilonSVR() {
		final List<HyperParameter> hyperParameters = new ArrayList<>();
		hyperParameters.addAll(KernelType.hyperParametersForPolynomialKernel(1, 3, 0.5, 1, 0, 1));
		hyperParameters.addAll(SvmType.hyperParametersForEpsilonSVR(0.1d, 1d, 0.001d, 0.1d));
		hyperParameters.addAll(TechnicalHyperParameter.getDefaults());
		final Map<ModelType, List<HyperParameter>> hyperParametersByModelType = new HashMap<>();
		hyperParametersByModelType.put(new ModelType(SvmType.EpsilonSVR, KernelType.Polynomial), hyperParameters);

		final Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> result = chooseHyperParametersForAPolynomialKernel(
				hyperParametersByModelType);

		assertThat(result.getModelConfiguration().C).isEqualTo(0.55d);
		assertThat(result.getModelConfiguration().eps).isEqualTo(0.001d);
		assertThat(result.getAverageErrorValue()).isBetween(0.012684d, 0.012685d);
	}

	private Result<ModelType, svm_parameter, libsvm.svm_node[], Double, TestDatum> chooseHyperParametersForAPolynomialKernel(
			final Map<ModelType, List<HyperParameter>> hyperParametersByModelType) {

		final List<TestDatum> data = new ArrayList<>();
		data.add(new TestDatum(-3, -1, 9));
		data.add(new TestDatum(-2, 0, 4));
		data.add(new TestDatum(-1, 1, 1));
		data.add(new TestDatum(0, 2, 0));
		data.add(new TestDatum(1, 3, 1));
		data.add(new TestDatum(2, 4, 4));
		data.add(new TestDatum(3, 5, 9));

		final List<List<Double>> truthValues = new ArrayList<>();
		truthValues.add(singletonList(9d));
		truthValues.add(singletonList(4d));
		truthValues.add(singletonList(1d));
		truthValues.add(singletonList(0d));
		truthValues.add(singletonList(1d));
		truthValues.add(singletonList(4d));
		truthValues.add(singletonList(9d));

		return chooseHyperParameters(data, truthValues, hyperParametersByModelType);
	}

}
